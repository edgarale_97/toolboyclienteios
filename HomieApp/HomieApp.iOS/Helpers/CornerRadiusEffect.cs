﻿using System;
using System.ComponentModel;
using HomieApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(CornerRadiusEffect), "CornerRadiusEffect")]
namespace HomieApp.iOS
{
    public class CornerRadiusEffect : PlatformEffect
    {
        private nfloat _originalRadius;

        protected override void OnAttached()
        {
            if (Container == null)
                return;
            _originalRadius = Container.Layer.CornerRadius;
            Container.ClipsToBounds = (true);
            UpdateCorner();
        }

        protected override void OnDetached()
        {
            if (Container == null)
                return;
            Container.Layer.CornerRadius = (_originalRadius);
            Container.ClipsToBounds = (false);
        }

        protected virtual void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            OnElementPropertyChanged(args);
            if (!(args.PropertyName == HomieApp.Helpers.Effects.CornerRadiusProperty.PropertyName))
                return;
            UpdateCorner();
        }

        private void UpdateCorner()
        {
            Container.Layer.CornerRadius =
                (nfloat)HomieApp.Helpers.Effects.GetCornerRadius(Element);
        }
    }
}