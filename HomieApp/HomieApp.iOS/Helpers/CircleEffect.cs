﻿using System;
using System.ComponentModel;
using CoreAnimation;
using CoreGraphics;
using HomieApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(CircleEffect), "CircleEffect")]
namespace HomieApp.iOS
{
    public class CircleEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            UpdateCircle();
        }

        protected override void OnDetached()
        {
            Container.Layer.Mask = null;
        }

        protected virtual void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            OnElementPropertyChanged(args);
            if (!(args.PropertyName == VisualElement.WidthProperty.PropertyName) && !(args.PropertyName == VisualElement.HeightProperty.PropertyName))
                return;
            UpdateCircle();
        }

        private void UpdateCircle()
        {
            double width = ((VisualElement)Element).Width;
            double height = ((VisualElement)Element).Height;
            if (width <= 0.0 || height <= 0.0)
                return;
            double num1 = Math.Min(width, height);
            double num2 = width <= num1 ? 0.0 : (width - num1) / 2.0;
            double num3 = height <= num1 ? 0.0 : (height - num1) / 2.0;
            CAShapeLayer caShapeLayer = new CAShapeLayer();
            caShapeLayer.Path = (CGPath.EllipseFromRect(new CGRect(num2, num3, num1, num1)));
            Container.Layer.Mask = caShapeLayer;
        }
    }
}