﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using HomieApp.iOS.Helpers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(HomieApp.Helpers.EntryProperties), typeof(ArtinaEntryRenderer))]
namespace HomieApp.iOS.Helpers
{
    public class ArtinaEntryRenderer : EntryRenderer
    {
        private CALayer _borderLayer;

        public ArtinaEntryRenderer()
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement == null || this.Control == null)
                return;
            this.UpdatePlaceholderColor(e.NewElement);
            this.SetupBorderProperties(e.NewElement);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (string.Equals(e.PropertyName, "BorderColor"))
            {

                HomieApp.Helpers.BorderStyle borderStyle = HomieApp.Helpers.EntryProperties.GetBorderStyle(this.Element);
                Color borderColor = HomieApp.Helpers.EntryProperties.GetBorderColor(this.Element);
                if (borderStyle == HomieApp.Helpers.BorderStyle.BottomLine)
                {
                    if (this._borderLayer == null)
                        return;
                    this._borderLayer.BackgroundColor = borderColor.ToCGColor();
                }
                else
                {
                    if (borderStyle != HomieApp.Helpers.BorderStyle.Rect && borderStyle != HomieApp.Helpers.BorderStyle.RoundRect && borderStyle != HomieApp.Helpers.BorderStyle.Default)
                        return;
                    this.Control.Layer.BorderColor = borderColor.ToCGColor();
                }
            }
            else if (string.Equals(e.PropertyName, "BorderWidth"))
            {
                switch (HomieApp.Helpers.EntryProperties.GetBorderStyle(this.Element))
                {
                    case HomieApp.Helpers.BorderStyle.Rect:
                    case HomieApp.Helpers.BorderStyle.RoundRect:
                        this.Control.Layer.BorderWidth = HomieApp.Helpers.EntryProperties.GetBorderWidth(this.Element);
                        break;
                }
            }
            else if (string.Equals(e.PropertyName, "BorderCornerRadius"))
            {
                if (HomieApp.Helpers.EntryProperties.GetBorderStyle(this.Element) != HomieApp.Helpers.BorderStyle.RoundRect)
                    return;
                this.Control.Layer.CornerRadius= HomieApp.Helpers.EntryProperties.GetBorderCornerRadius(this.Element);
            }
            else
            {
                if (!string.Equals(e.PropertyName, "PlaceholderColor"))
                    return;
                this.UpdatePlaceholderColor(this.Element);
            }
        }

        private void UpdatePlaceholderColor(Entry entry)
        {
            Color placeholderColor = HomieApp.Helpers.EntryProperties.GetPlaceholderColor(entry);
            if (!(placeholderColor != Color.Default))
                return;
            UIStringAttributes stringAttributes = new UIStringAttributes();
            UIColor uiColor = placeholderColor.ToUIColor();
            stringAttributes.ForegroundColor = uiColor;
            UIColor clear = UIColor.Clear;
            stringAttributes.BackgroundColor = clear;
            this.Control.AttributedPlaceholder = new NSAttributedString(this.Control.Placeholder, stringAttributes);
        }

        private void SetupBorderProperties(Entry entry)
        {
            switch (HomieApp.Helpers.EntryProperties.GetBorderStyle(entry))
            {
                case HomieApp.Helpers.BorderStyle.BottomLine:
                    this.Control.BorderStyle = 0L;
                    Color borderColor1 = HomieApp.Helpers.EntryProperties.GetBorderColor(entry);
                    CALayer caLayer = new CALayer();
                    caLayer.BackgroundColor = borderColor1.ToCGColor();
                    this.Control.Layer.AddSublayer(caLayer);
                    this._borderLayer = caLayer;
                    break;
                case HomieApp.Helpers.BorderStyle.Rect:
                    this.Control.BorderStyle = (UITextBorderStyle)1L;
                    this.Control.Layer.BorderWidth = HomieApp.Helpers.EntryProperties.GetBorderWidth(entry);
                    this.Control.Layer.CornerRadius = 0.0f;
                    Color borderColor2 = HomieApp.Helpers.EntryProperties.GetBorderColor(entry);
                    if (!(borderColor2 != Color.Default))
                        break;
                    this.Control.Layer.BorderColor = borderColor2.ToCGColor();
                    break;
                case HomieApp.Helpers.BorderStyle.RoundRect:
                    this.Control.BorderStyle = (UITextBorderStyle)3L;
                    this.Control.Layer.BorderWidth = HomieApp.Helpers.EntryProperties.GetBorderWidth(entry);
                    this.Control.Layer.CornerRadius = HomieApp.Helpers.EntryProperties.GetBorderCornerRadius(entry);
                    Color borderColor3 = HomieApp.Helpers.EntryProperties.GetBorderColor(entry);
                    if (!(borderColor3 != Color.Default))
                        break;
                    this.Control.Layer.BorderColor = borderColor3.ToCGColor();
                    break;
                case HomieApp.Helpers.BorderStyle.None:
                    this.Control.BorderStyle = 0L;
                    this.Control.Layer.BorderWidth = 0;
                    this.Control.Layer.CornerRadius = 0;
                    this.Control.Layer.BorderColor = Color.Default.ToCGColor();
                    break;
                case HomieApp.Helpers.BorderStyle.Default:
                    Color borderColor4 = HomieApp.Helpers.EntryProperties.GetBorderColor(entry);
                    if (!(borderColor4 != Color.Default))
                        break;
                    this.Control.Layer.BorderColor = borderColor4.ToCGColor();
                    break;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (this._borderLayer == null)
                return;
            ArtinaEntryRenderer.UpdateFrame(this._borderLayer, HomieApp.Helpers.EntryProperties.GetBorderWidth(this.Element), this.Control);
        }

        private static void UpdateFrame(CALayer layer, float borderWidth, UIView view)
        {
            CALayer caLayer = layer;
            nfloat nfloat1 = 0;
            CGRect frame = view.Frame;
            // ISSUE: explicit reference operation
            //nfloat nfloat2 = nfloat.op_Subtraction(((CGRect)@frame).Height, (nfloat)(borderWidth));
            nfloat nfloat2 = frame.Height - borderWidth;
            frame = view.Frame;
            // ISSUE: explicit reference operation
            nfloat width = frame.Width;
            nfloat nfloat3 = borderWidth;
            CGRect cgRect = new CGRect(nfloat1, nfloat2, width, nfloat3);
            caLayer.Frame = cgRect;
        }
    }
}