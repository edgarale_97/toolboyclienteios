﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Card.IO;
using Foundation;
using HomieApp.Helpers;
using HomieApp.iOS.DependencyServices;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(CardService))]
namespace HomieApp.iOS.DependencyServices
{
    public class CardService : CardIOPaymentViewControllerDelegate, HomieApp.DependencyServices.ICardService
    {
        private UIViewController rootViewController;
        private CreditCardInfo cardInfo;

        public void StartCapture()
        {
            InitCardService();
            var paymentViewController = new CardIOPaymentViewController(this);
            rootViewController.PresentViewController(paymentViewController, true, null);
        }

        public Entities.CreditCard GetCreditCard()
        {
            Console.WriteLine($"GetCreditCard --->");
            if (cardInfo != null)
            {
                Entities.CreditCardType type;

                if (cardInfo.CardType == CreditCardType.Amex)
                {
                    type = Entities.CreditCardType.Amex;
                }
                else if (cardInfo.CardType == CreditCardType.Visa)
                {
                    type = Entities.CreditCardType.Visa;
                }
                else if (cardInfo.CardType == CreditCardType.Mastercard)
                {
                    type = Entities.CreditCardType.Mastercard;
                }
                else
                {
                    type = Entities.CreditCardType.Unrecognized;
                }

                Console.WriteLine($"Credit Card ---> {cardInfo.CardNumber}");

                return new Entities.CreditCard
                {
                    ExpiryYear = (int)cardInfo.ExpiryYear,
                    ExpiryMonth = (int)cardInfo.ExpiryMonth,
                    Cvv = cardInfo.Cvv,
                    CardholderName = cardInfo.CardholderName,
                    CardNumber = cardInfo.CardNumber,
                    RedactedCardNumber = cardInfo.RedactedCardNumber,
                    CardType = type
                };
            }
            else
            {
                Console.WriteLine("Informacion card nulo!");
            }

            return null;
        }

        public string GetCardNumber()
        {
            return (cardInfo != null) ? cardInfo.CardNumber : null;
        }

        public string GetCardholderName()
        {
            return (cardInfo != null) ? cardInfo.CardholderName : null;
        }

        private void InitCardService()
        {
            // Init rootViewController
            var window = UIApplication.SharedApplication.KeyWindow;
            rootViewController = window.RootViewController;
            while (rootViewController.PresentedViewController != null)
            {
                rootViewController = rootViewController.PresentedViewController;
            }
            //cardInfo = null;
        }

        public void CloseCardService()
        {
            rootViewController = null;
            cardInfo = null;
        }

        public override void UserDidCancelPaymentViewController(CardIOPaymentViewController paymentViewController)
        {
            Console.WriteLine("Scanning Canceled! (UserDidCancelPaymentViewControlle)");

            this.cardInfo = null;
            paymentViewController.DismissViewController(true, null);
        }

        public override void UserDidProvideCreditCardInfo(CreditCardInfo cardInfo, CardIOPaymentViewController paymentViewController)
        {
            paymentViewController.DismissViewController(true, null);
            if(cardInfo == null)
            {
                Console.WriteLine($"Scanning Canceled...!!!");
            }
            else
            {
                this.cardInfo = cardInfo;
                //Console.WriteLine($"CardInfo ---> {cardInfo.CardNumber}");
            }
            /*if (cardInfo == null)
            {
                Console.WriteLine("Scanning Canceled!");
            }
            else
            {
                this.cardInfo = cardInfo;
            }

            paymentViewController.DismissViewController(true, null);*/
        }
    }
}