﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using FFImageLoading.Forms.Touch;
using Foundation;
using HomieApp.Helpers;
using ImageCircle.Forms.Plugin.iOS;
using KeyboardOverlap.Forms.Plugin.iOSUnified;
using Lottie.Forms.iOS.Renderers;
using Plugin.Badge;
using Plugin.VersionTracking;
using System.Collections.Generic;
using UIKit;
using Xamarin.Forms;

[assembly: ResolutionGroupName("HomieApp")]
namespace HomieApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //private SBNotificationHub Hub { get; set; }

        //public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        //{
        //    try
        //    {
        //        Hub = new SBNotificationHub(ApiKeys.ConnectionString, ApiKeys.NotificationHub);

        //        // Update registration with Azure Notification Hub
        //        Hub.UnregisterAll(deviceToken, (error) =>
        //        {
        //            if (error != null)
        //            {
        //                System.Diagnostics.Debug.WriteLine("Error calling Unregister: {0}", error.ToString());
        //                return;
        //            }

        //            string[] list = { HomieApp.Helpers.Settings.DeviceId };
        //            NSSet tags = new NSSet(list);

        //            Hub.RegisterNative(deviceToken, tags, (errorCallback) =>
        //            {
        //                if (errorCallback != null)
        //                    Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
        //            });
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            //Initializing FFImageLoading
            CachedImageRenderer.Init();

            // Color Negro para el Switch
            UISwitch.Appearance.OnTintColor = UIColor.FromRGB(0, 169, 157);

            global::Xamarin.Forms.Forms.Init();

            //var settings = UIUserNotificationSettings.GetSettingsForTypes(
            //  UIUserNotificationType.Alert
            //| UIUserNotificationType.Badge
            //| UIUserNotificationType.Sound,
            //new NSSet());

            //UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            //UIApplication.SharedApplication.RegisterForRemoteNotifications();

            //// Initializaing OneSignal
            //OneSignal.Current.StartInit("676f4277-f432-4aa1-a3c8-c1a027dd6ccf").EndInit();

            // ONE SIGNAL
            OneSignal.Current.StartInit("5ee4f6bf-f2f4-40f1-8671-a5e2c392c017").Settings(new Dictionary<string, bool>() {
                { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } }).HandleNotificationOpened(onHandleNotificationOpened).HandleNotificationReceived(onHandleNotificationReceived)
                .InFocusDisplaying(OSInFocusDisplayOption.Notification).EndInit();

            OneSignal.Current.RegisterForPushNotifications();

            OneSignal.Current.IdsAvailable(IdsAvailable);

            OneSignal.Current.SendTag("deviceId", Settings.DeviceId);

            Xamarin.FormsGoogleMaps.Init(HomieApp.Helpers.ApiKeys.iOSGoogleMapsKey);
            ImageCircleRenderer.Init();
            CrossVersionTracking.Current.Track();
            CachedImageRenderer.Init(); //Initializing FFImageLoading
            AnimationViewRenderer.Init(); //Initializing Lottie

            HomieApp.Helpers.Settings.Platform = (int)Entities.Platform.iOS;

            // Para hacerlo funcionar tuve que habilitar Google Places API Web Service.
            HomieApp.Helpers.Settings.GooglePlacesApiKey = HomieApp.Helpers.ApiKeys.iOSGooglePlacesApiKey;

            if (string.IsNullOrWhiteSpace(HomieApp.Helpers.Settings.DeviceId))
            {
                //HomieApp.Helpers.Settings.DeviceId = CrossDeviceInfo.Current.GenerateAppId(true);
                HomieApp.Helpers.Settings.Confirmado = false;
            }

            LoadApplication(new App());

            KeyboardOverlapRenderer.Init();

            //NotificationSettings();

            //UIApplication.SharedApplication.RegisterForRemoteNotifications();

            return base.FinishedLaunching(app, options);
        }

        private static void onHandleNotificationReceived(OSNotification notification)
        {
            OSNotificationPayload payload = notification.payload;
            Dictionary<string, object> additionalData = payload.additionalData;
            string message = payload.body;
            if (additionalData != null)
            {
                if (additionalData.ContainsKey("pageToOpen"))
                {

                    var notificacion = new HomieApp.Entities.Notificaciones();
                    notificacion.PageToOpen = additionalData["pageToOpen"].ToString();
                    notificacion.Parameters = additionalData["parameters"].ToString();
                    notificacion.Titulo = payload.title;
                    notificacion.Descripcion = message;

                    
                    //if (additionalData["pageToOpen"] != HomieApp.Entities.NotificacionPage.Chat)
                    //{
                    //    HomieApp.Helpers.Settings.Notificaciones++;
                    //    CrossBadge.Current.SetBadge(HomieApp.Helpers.Settings.Notificaciones);

                    //    //UIAlertView avAlert = new UIAlertView(titleText, alert, null, "OK", null);
                    //    //avAlert.Show();
                    //}

                    // Enviamos un message para abrir las pantallas.
                    MessagingCenter.Send<App, HomieApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "HubNotification", notificacion);
                    // Take user to your store.
                }
            }
            //if (actionID != null)
            //{
            //    // actionSelected equals the id on the button the user pressed.
            //    // actionSelected will equal "__DEFAULT__" when the notification itself was tapped when buttons were present.

            //}
        }

        private static void onHandleNotificationOpened(OSNotificationOpenedResult result)
        {
            OSNotificationPayload payload = result.notification.payload;
            Dictionary<string, object> additionalData = payload.additionalData;
            string message = payload.body;
            if (additionalData != null)
                {
                    if (additionalData.ContainsKey("pageToOpen"))
                    {

                        var notificacion = new HomieApp.Entities.Notificaciones();
                        notificacion.PageToOpen = additionalData["pageToOpen"].ToString();
                        notificacion.Parameters = additionalData["parameters"].ToString();
                        notificacion.Titulo = payload.title;
                        notificacion.Descripcion = message;


                        //if (additionalData["pageToOpen"] != HomieApp.Entities.NotificacionPage.Chat)
                        //{
                        //    HomieApp.Helpers.Settings.Notificaciones++;
                        //    CrossBadge.Current.SetBadge(HomieApp.Helpers.Settings.Notificaciones);

                        //    //UIAlertView avAlert = new UIAlertView(titleText, alert, null, "OK", null);
                        //    //avAlert.Show();
                        //}

                        // Enviamos un message para abrir las pantallas.
                        MessagingCenter.Send<App, HomieApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "OpenFromNotificationBar", notificacion);
                        // Take user to your store.
                    }
                }
            //if (actionID != null)
            //{
            //    // actionSelected equals the id on the button the user pressed.
            //    // actionSelected will equal "__DEFAULT__" when the notification itself was tapped when buttons were present.

            //}
        }

        private void IdsAvailable(string userID, string pushToken)
        {
            Settings.DeviceId = userID;
        }
        /*public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Hub = new SBNotificationHub(ApiKeys.ConnectionString, ApiKeys.NotificationHub);
            // Update registration with Azure Notification Hub
            Hub.UnregisterAllAsync(deviceToken);
            string[] list = { Settings.DeviceId };
            var tags = new NSSet(list);
            Hub.RegisterNativeAsync(deviceToken, tags);
        }*/

        /*public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            byte[] bytes = deviceToken.ToArray<byte>();
            string[] hexArray = bytes.Select(b => b.ToString("x2")).ToArray();
            string pnsDeviceToken = string.Join(string.Empty, hexArray);

            HomieApp.Helpers.Settings.RegistrationId = pnsDeviceToken;
            Hub = new SBNotificationHub(ApiKeys.ConnectionString, ApiKeys.NotificationHub);
            
            string[] list = { HomieApp.Helpers.Settings.DeviceId };
            NSSet tags = new NSSet(list);

            Hub.RegisterNative(pnsDeviceToken, tags, err =>
            {
                if (err != null)
                {
                    Console.WriteLine("Error: " + err.Description);
                }
                else
                {
                    Console.WriteLine("Success: " + pnsDeviceToken + " " + tags.ToString());
                }
            });
        }*/

        //private void NotificationSettings()
        //{
        //    if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
        //    {
        //        UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
        //                                                                (granted, error) =>
        //                                                                {
        //                                                                    if (granted)
        //                                                                        InvokeOnMainThread(UIApplication.SharedApplication.RegisterForRemoteNotifications);
        //                                                                });
        //    }
        //    else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
        //    {
        //        var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
        //                UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
        //                new NSSet());

        //        UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
        //        UIApplication.SharedApplication.RegisterForRemoteNotifications();
        //    }
        //    else
        //    {
        //        UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
        //        UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
        //    }

        //}

        /*public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            HomieApp.Helpers.Settings.RegistrationId = deviceToken.GetBase64EncodedString(NSDataBase64EncodingOptions.None);

            Hub = new SBNotificationHub(HomieApp.Helpers.ApiKeys.ListenConnectionString, HomieApp.Helpers.ApiKeys.NotificationHubName);
            Hub.UnregisterAllAsync(deviceToken, (error) =>
            {
                if (error != null)
                {
                    Console.WriteLine("Error calling Unregister: {0}", error.ToString());
                    return;
                }

                string[] list = { HomieApp.Helpers.Settings.DeviceId };
                NSSet tags = new NSSet(list);

                Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) =>
                {
                    if (errorCallback != null)
                        Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                });
            });
        }*/

        //public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        //{
        //    if (application.ApplicationState == UIApplicationState.Active)
        //    {
        //        ProcessNotification(userInfo, false);
        //    }
        //    else if (application.ApplicationState == UIApplicationState.Background)
        //    {
        //        ProcessNotification(userInfo, true);
        //    }
        //    else if (application.ApplicationState == UIApplicationState.Inactive)
        //    {
        //        ProcessNotification(userInfo, true);
        //    }            
        //}

        //void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        //{
        //    // Solamente puede recibir notificaciones si está confirmado (login).
        //    if (HomieApp.Helpers.Settings.Confirmado)
        //    {
        //        // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
        //        if (null != options && options.ContainsKey(new NSString("aps")))
        //        {
        //            //Get the aps dictionary
        //            NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

        //            var alert = string.Empty;
        //            var titleText = string.Empty;
        //            var appName = string.Empty;
        //            var pageToOpen = string.Empty;
        //            var parameters = string.Empty;

        //            if (aps.ContainsKey(new NSString("alert")))
        //            {
        //                alert = (aps[new NSString("alert")] as NSString).ToString();
        //            }
        //            if (aps.ContainsKey(new NSString("title")))
        //            {
        //                titleText = (aps[new NSString("title")] as NSString).ToString();
        //            }
        //            if (aps.ContainsKey(new NSString("appName")))
        //            {
        //                appName = (aps[new NSString("appName")] as NSString).ToString();
        //            }

        //            if (appName != "homie_cliente")
        //            {
        //                return;
        //            }

        //            if (aps.ContainsKey(new NSString("pageToOpen")))
        //            {
        //                pageToOpen = (aps[new NSString("pageToOpen")] as NSString).ToString();
        //                parameters = (aps[new NSString("parameters")] as NSString).ToString();
        //            }

        //            var notificacion = new HomieApp.Entities.Notificaciones();
        //            notificacion.PageToOpen = pageToOpen;
        //            notificacion.Parameters = parameters;
        //            notificacion.Titulo = titleText;
        //            notificacion.Descripcion = alert;

        //            //If this came from the ReceivedRemoteNotification while the app was running,
        //            // we of course need to manually process things like the sound, badge, and alert.
        //            if (!fromFinishedLaunching)
        //            {
        //                //Manually show an alert
        //                if (!string.IsNullOrEmpty(alert))
        //                {
        //                    if (pageToOpen != HomieApp.Entities.NotificacionPage.Chat)
        //                    {
        //                        HomieApp.Helpers.Settings.Notificaciones++;
        //                        CrossBadge.Current.SetBadge(HomieApp.Helpers.Settings.Notificaciones);

        //                        //UIAlertView avAlert = new UIAlertView(titleText, alert, null, "OK", null);
        //                        //avAlert.Show();
        //                    }

        //                    // Enviamos un message para abrir las pantallas.
        //                    MessagingCenter.Send<App, HomieApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "HubNotification", notificacion);
        //                }
        //            }
        //            else
        //            {
        //                HomieApp.Helpers.Settings.Notificaciones = 0;
        //                CrossBadge.Current.SetBadge(HomieApp.Helpers.Settings.Notificaciones);
        //                MessagingCenter.Send<App, HomieApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "OpenFromNotificationBar", notificacion);
        //            }
        //        }
        //    }
        //}


        public override void OnActivated(UIApplication application)
        {
            MessagingCenter.Send<App>((App)Xamarin.Forms.Application.Current, "OnActivated");
        }

        public override void DidEnterBackground(UIApplication application)
        {
            MessagingCenter.Send<App>((App)Xamarin.Forms.Application.Current, "DidEnterBackground");
        }

        //public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        //{
        //    new UIAlertView("Registration failed", error.LocalizedDescription, null, "OK", null).Show();
        //}

        //public class NotificationDelegate : UNUserNotificationCenterDelegate
        //{
        //    public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, [BlockProxy(typeof(NIDActionArity1V203))] Action<UNNotificationPresentationOptions> completionHandler)
        //    {
        //        completionHandler(UNNotificationPresentationOptions.Alert);
        //    }
        //}
    }
}
