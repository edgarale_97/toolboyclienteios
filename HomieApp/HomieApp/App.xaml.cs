﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using DLToolkit.Forms.Controls;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Views.Servicio;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HomieApp
{
    public partial class App : Application
	{
	    public static MasterDetailPage MasterDetailPage;
	    public static Messages Messages;
		public App ()
		{
			InitializeComponent();
		    FlowListView.Init();

            Messages = new Messages();

		    Settings.MetodoPago = true;

		    SetMainPage();

            MessagingCenter.Subscribe<HomePage>(this, "Login", (sender) =>
             {
                 Device.BeginInvokeOnMainThread(() =>
                 {
                     var login = new LoginPage();
                     login.LoginConfirmado += Login_LoginConfirmado;

                     MainPage = login;
                 });
             });

            MessagingCenter.Subscribe<HomePage>(this, "SignUp", (sender) =>
             {
                 Device.BeginInvokeOnMainThread(() =>
                 {
                     var signUp = new SignUpPage();
                     signUp.RegistroCompletado += Signup_RegistroCompletado;

                     MainPage = signUp;
                 });
             });

            MessagingCenter.Subscribe<HomeLabelNavigation>(this, "Home", (sender) =>
             {
                 Device.BeginInvokeOnMainThread(() =>
                 {
                     var home = new HomePage();
                     MainPage = home;
                 });
             });

            MessagingCenter.Subscribe<HomePage>(this, "Navigation", (sender) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var navigation = new NavigationTipoPage();
                    MainPage = navigation;
                });
            });

        }

        

        private void SetMainPage()
	    {
            if (!Settings.Registrado)
            {
                var home = new HomePage();
                MainPage = home;
            }
            else
            {
                if (Settings.Confirmado)
                {
                    var rootPage = new RootPage();
                    rootPage.CerrarSesion += Rootpage_CerrarSesion;
                    MainPage = rootPage;
                }
                else
                {
                    var login = new LoginPage();
                    login.LoginConfirmado += Login_LoginConfirmado;
                    MainPage = login;
                }
            }
            MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

	    private void Signup_RegistroCompletado(object sender, EventArgs e)
	    {
	        ((SignUpPage)sender).RegistroCompletado -= Signup_RegistroCompletado;
	        SetMainPage();
	    }

	    private void Rootpage_CerrarSesion(object sender, EventArgs e)
	    {
	        ((RootPage)sender).CerrarSesion -= Rootpage_CerrarSesion;
	        SetMainPage();
	    }

	    private void Login_LoginConfirmado(object sender, EventArgs e)
	    {
	        ((LoginPage)sender).LoginConfirmado -= Login_LoginConfirmado;
	        SetMainPage();
	    }
    }
}
