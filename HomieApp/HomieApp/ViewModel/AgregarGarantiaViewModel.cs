﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    class AgregarGarantiaViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private Garantias garatias;
        private ImageSource _fotoEquipo;
        private ImageSource _fotoFactura;
        private bool _isEquipo;
        private bool _isFactura;

        private Direcciones _direccion;
        private ObservableCollection<Categorias> _categorias;
        private List<string> _pickerCategorias;

        private DateTime _fechaCompra;
        private DateTime _fechaVigencia;

        public DateTime FechaCompra
        {
            get { return _fechaCompra; }
            set
            {
                _fechaCompra = value;
                RaisePropertyChanged();
            }
        }

        public DateTime FechaVigencia
        {
            get { return _fechaVigencia; }
            set
            {
                _fechaVigencia = value;
                RaisePropertyChanged();
            }
        }

        public Direcciones Direccion
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Categorias> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public Garantias Garantias
        {
            get { return garatias; }
            set
            {
                garatias = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource FotoEquipo
        {
            get { return _fotoEquipo; }
            set
            {
                _fotoEquipo = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource FotoFactura
        {
            get { return _fotoFactura; }
            set
            {
                _fotoFactura = value;
                RaisePropertyChanged();
            }
        }

        public bool IsEquipo
        {
            get { return _isEquipo; }
            set
            {
                _isEquipo = value;
                RaisePropertyChanged();
            }
        }

        public bool IsFactura
        {
            get { return _isFactura; }
            set
            {
                _isFactura = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public AgregarGarantiaViewModel()
        {
            Categorias = new ObservableCollection<Categorias>();
            IsEquipo = false;
            IsFactura = false;
            Garantias = new Garantias();
            //BuscarDireccion();
        }

        #endregion

        #region Methods

        public async void BuscarDireccion()
        {
            Direccion = new Direcciones();
            IsLoading = true;
            var model = new DireccionesModel();
            var result = await model.GetListAsync();
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Direccion = result.Result.FirstOrDefault(x => x.Principal);
                }

                IsLoading = true;
                var modelCat = new CategoriasModel();
                var resultCat = await modelCat.GetListAsync(0);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (resultCat.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", resultCat.ErrorDescription, "OK");
                    }
                    else
                    {
                        foreach(var categoria in resultCat.Result)
                        {
                            Categorias.Add(categoria);
                        }
                    }
                    IsLoading = false;
                });
            });
        }

        public async Task TakePicture(int foto)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontró una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 30)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            switch (foto)
            {
                case 1:
                    FotoEquipo = image;
                    Garantias.FotoEquipo = array;
                    IsEquipo = true;
                    break;
                case 2:
                    FotoFactura = image;
                    Garantias.FotoFactura = array;
                    IsFactura = true;
                    break;
            }
        }

        public async Task PickPicture(int foto)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "No se otorgaron permisos para accesar a las fotos", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 30
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            switch (foto)
            {
                case 1:
                    FotoEquipo = image;
                    Garantias.FotoEquipo = array;
                    IsEquipo = true;
                    break;
                case 2:
                    FotoFactura = image;
                    Garantias.FotoFactura = array;
                    IsFactura = true;
                    break;
            }
        }

        public async Task Guardar()
        {
            IsLoading = true;
            var model = new ActivosModel();
            var result = await model.PostAsync(Garantias);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    // Cuando el registro se hizo correctamente
                    await Application.Current.MainPage.DisplayAlert("Registro correcto", "Su equipo se registró correctamente", "OK");
                    RaisePropertyChanged("Close");
                }
                IsLoading = false;
            });
            
        }

        #endregion

        #region Commands

        public RelayCommand AgregarCommand => new RelayCommand(async () =>
        {
            if (string.IsNullOrEmpty(Garantias.Marca))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La Marca no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(Garantias.Modelo))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El Modelo no puede quedar vacio", "OK");
                return;
            }
            //if (string.IsNullOrEmpty(Garantias.Factura))
            //{
            //    await Application.Current.MainPage.DisplayAlert("Error",
            //        "La Factura no puede quedar vacio", "OK");
            //    return;
            //}
            if (string.IsNullOrEmpty(Garantias.NumeroSerie))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El Número de serie no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(Garantias.Tienda))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El nombre de la Tienda no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(Garantias.NombreVendedor))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El Nombre del Vendedor no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(Garantias.NombreCategoria))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La categoría no puede quedar vacia", "OK");
                return;
            }
            if (!IsEquipo)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Debe agregar una imagen del equipo", "OK");
                return;
            }
            if (!IsFactura)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Debe agregar una imagen de su factura", "OK");
                return;
            }
            if (string.IsNullOrWhiteSpace(Garantias.FechaVigencia.ToString()))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Debe agregar la fecha de su factura", "OK");
                return;
            }

            await Guardar();

        });

        #endregion
    }
}

