﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    class SignUpViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private bool _close;

        private SignUpUser _user;
        private ImageSource _image;
        private bool _checkPicture;
        public bool AceptoTerminos { get; set; }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public SignUpUser User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged();
            }
        }

        public bool CheckPicture
        {
            get { return _checkPicture; }
            set
            {
                _checkPicture = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SignUpViewModel()
        {
            IsLoading = false;
            CheckPicture = false;
            User = new SignUpUser();
        }

        #endregion

        #region Methods

        public async Task TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontró una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Front, 
                //AllowCropping = true,
                 CompressionQuality = 30,
                 PhotoSize = PhotoSize.Small
            });

            if (file == null)
                return;

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();

                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();

                        await Task.Delay(1000);
                    }

                    count++;
                }
            }

            User.Foto = array;

            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;

            CheckPicture = true;
        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se otorgaron los permisos para accesar a las fotos", "OK");
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Small,
                CompressionQuality = 30
            });

            if (file == null)
                return;

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();

                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();

                        await Task.Delay(1000);
                    }

                    count++;
                }
            }

            User.Foto = array;

            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;

            CheckPicture = true;
        }

        public async Task Registrarse()
        {
            if (string.IsNullOrWhiteSpace(User.Nombres))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El nombre es obligatorio", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(User.ApellidoPaterno))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El apellido es obligatorio", "OK");
                return;
            }

            if (string.IsNullOrEmpty(User.Correo))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El E-mail es obligatorio", "OK");
                return;
            }

            if (string.IsNullOrEmpty(User.TelefonoMovil))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El teléfono móvil es obligatorio", "OK");
                return;
            }
            if (User.TelefonoMovil.Length < 10)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El teléfono móvil debe tener al menos 10 dígitos", "OK");
                return;
            }
            /* if (!AceptoTerminos)
             {
                 await Application.Current.MainPage.DisplayAlert("Información", "Debe aceptar los términos y condiciones.", "OK");
                 return;
             } */

            IsLoading = true;
            var model = new SignUpModel();
            var result = await model.PostAsync(User);


            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
            }
            else
            {
                // Cuando el registro se realizó con exito:
                await Application.Current.MainPage.DisplayAlert("Registro correcto", "En breve, recibirás un código de acceso en tu correo electrónico para poder accesar a Toolboy", "OK");

                Settings.Registrado = true;
                Settings.User = User.Correo;
                Settings.Password = User.Password;

                Close = true;
            }
            IsLoading = false;
        }

        #endregion

        #region Commands

        public RelayCommand RegistrarseCommand => new RelayCommand(async () =>
        {
            await Registrarse();
        });

        public RelayCommand TakePictureCommand => new RelayCommand(async () =>
        {
            await TakePicture();
        });

        public RelayCommand PickPictureCommand => new RelayCommand(async () =>
        {
            await PickPicture();
        });

        public RelayCommand IngresarCommand => new RelayCommand(() =>
        {
            Settings.Registrado = true;
            Settings.User = User.Correo;
            Settings.Password = User.Password;

            Close = true;
        });

        #endregion
    }
}
