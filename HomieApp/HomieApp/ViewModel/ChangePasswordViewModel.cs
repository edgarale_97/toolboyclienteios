﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using GalaSoft.MvvmLight.Command;
using HomieApp.Helpers;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    class ChangePasswordViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private bool _close;
        private string _password;
        private string _confirmPassword;

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value; 
                RaisePropertyChanged();
            }
        }

        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set
            {
                _confirmPassword = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value; 
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public ChangePasswordViewModel() { }

        #endregion

        #region Methods



        #endregion

        #region Commands

        public RelayCommand IngresarCommand => new RelayCommand(async () =>
        {
            if (string.IsNullOrWhiteSpace(Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar la contraseña", "OK");
                return;
            }
            if (string.IsNullOrWhiteSpace(ConfirmPassword))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Debe ingresar la confirmación de la contraseña", "OK");
                return;
            }

            if (Password != ConfirmPassword)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La contraseña y la confirmación de la contraseña son diferentes", "OK");
                return;
            }
            IsLoading = true;
            var rest = new ChangePasswordModel();
            var result = await rest.PostAsync(Password);
            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");

            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Correcto", "La contraseña se actualizó correctamente",
                    "OK");
                Settings.Password = Password;
                Close = true;
            }
            IsLoading = false;
        });

        #endregion
    }
}
