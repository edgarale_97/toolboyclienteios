﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    public class TarjetasViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private ObservableCollection<Tarjeta> _tarjeta;
        private Tarjeta _tarjetaSeleccionada;

        public ObservableCollection<Tarjeta> Tarjetas
        {
            get { return _tarjeta; }
            set
            {
                _tarjeta = value;
                RaisePropertyChanged();
            }
        }

        public Func<string, Task<string>> EvaluateJavascript { get; set; }

        #endregion

        #region Constructor

        public TarjetasViewModel()
        {
            Tarjetas = new ObservableCollection<Tarjeta>();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            Tarjetas = new ObservableCollection<Tarjeta>();

            IsLoading = true;
            var model = new TarjetasModel();
            var result = await model.GetListAsync();

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    foreach (var tarjeta in result.Result.OrderByDescending(x => x.Principal))
                    {
                        Tarjetas.Add(tarjeta);
                    }
                }

                IsLoading = false;
            });

        }

        public async Task Eliminar(long idTarjeta)
        {
            if (!await Application.Current.MainPage.DisplayAlert("Confirmar", "Está seguro que desea eliminar la tarjeta seleccionada?", "Continuar", "Cancelar"))
            {
                return;
            }

            IsLoading = true;

            var model = new TarjetasModel();
            var result = await model.DeleteAsync(idTarjeta);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Buscar();
                }

                IsLoading = false;
            });
        }

        public async Task Principal(long idTarjeta)
        {
            if (!await Application.Current.MainPage.DisplayAlert("Confirmar", "Está seguro que desea usar la tarjeta seleccionada como principal?", "Continuar", "Cancelar"))
            {
                return;
            }

            IsLoading = true;

            var model = new TarjetasModel();
            var result = await model.PutAsync(idTarjeta, true);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Buscar();
                }

                IsLoading = false;
            });
        }

        public async void NuevaTarjeta(CreditCard tarjeta)
        {
            IsLoading = true;

            // Evaluamos el deviceSessionId
            var res = await EvaluateJavascript("$('#deviceSessionId').val();");
            var deviceSessionId = res.Replace("\"", "");

            var t = new Tarjeta
            {
                CardNumber = tarjeta.CardNumber,
                Csc = tarjeta.Cvv,
                ExpiresMonth = tarjeta.ExpiryMonth,
                ExpireYear = tarjeta.ExpiryYear,
                PaymentType = (int)tarjeta.CardType
            };

            var model = new TarjetasModel();
            var result = await model.PostAsync(t, deviceSessionId);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Settings.MetodoPago = true;
                    RaisePropertyChanged("Completed");
                }

                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand BuscarCommand => new RelayCommand(async () =>
        {
            await Buscar();
        });

        public RelayCommand<long> EliminarCommand => new RelayCommand<long>(async (id) =>
        {
            await Eliminar(id);
        });

        public RelayCommand<long> PrincipalCommand => new RelayCommand<long>(async (id) =>
        {
            await Principal(id);
        });

        //public ICommand EvalJS
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            var result = await EvaluateJavascript("document.getElementById('html');");
        //        });
        //    }
        //}

        #endregion
    }
}
