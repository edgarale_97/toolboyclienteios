﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.ViewModel;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using HomieApp.Models;
using ImageSource = Xamarin.Forms.ImageSource;

namespace HomieApp.ViewModel
{
    public class SettingViewModel : HomieViewModelBase
    {
        #region Fields And Properties

        private ProfileUser _user;
        private ImageSource _image;

        public ProfileUser User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                _image = value; 
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SettingViewModel()
        {
            User = new ProfileUser();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            IsLoading = true;
            var model = new ProfileModel();
            var result = await model.GetAsync();

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");

                }
                else
                {
                    User = result.Result;
                }
                IsLoading = false;
            });
        }

        public async Task TakePicture()
        {

            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontró una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium
            });

            if (file == null)
                return;

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();

                while (count < 30)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();

                        await Task.Delay(1000);
                    }

                    count++;
                }
            }

            User.Foto = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se otorgaron los permisos para accesar a las fotos", "OK");
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Small,
                CompressionQuality = 30
            });

            if (file == null)
                return;

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();

                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();

                        await Task.Delay(1000);
                    }

                    count++;
                }
            }

            User.Foto = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
        }

        public async Task Guardar()
        {
            // TODO: Validar el formato de los telefonos.
            if (string.IsNullOrWhiteSpace(User.Nombres))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El nombre es obligatorio", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(User.ApellidoPaterno))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El apellido es obligatorio", "OK");
                return;
            }

            IsLoading = true;
            var model = new ProfileModel();
            var result = await model.PostAsync(User);

            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
            }
            else
            {
                // Cuando el registro se realizó con exito:
                await Application.Current.MainPage.DisplayAlert("", "Los datos se guardaron con éxito", "OK");
            }
            IsLoading = false;
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            await Guardar();
        });

        #endregion
    }
}
