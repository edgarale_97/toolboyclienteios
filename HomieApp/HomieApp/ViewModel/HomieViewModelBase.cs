﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight;

namespace HomieApp.ViewModel
{
    public class HomieViewModelBase : ViewModelBase
    {
        private bool _isLoading;
        private bool _isEnabled;

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                IsEnabled = !value;
                RaisePropertyChanged();
            }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                RaisePropertyChanged();
            }
        }

        public HomieViewModelBase()
        {
            IsLoading = false;
        }
    }
}
