﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    public class BusquedaDireccionViewModel : HomieViewModelBase
    {
        #region Properties

        public string Direccion { get; set; }

        public double Latitud { get; set; }
        public double Longitud { get; set; }

        private string _detalleDireccion;

        public string DetalleDireccion
        {
            get { return _detalleDireccion; }
            set
            {
                _detalleDireccion = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public BusquedaDireccionViewModel()
        {
            
        }

        #endregion

        #region Methods

        public async Task Guardar()
        {
            if (string.IsNullOrWhiteSpace(Direccion))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe seleccionar una dirección", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(DetalleDireccion))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El apartado del detalle no puede quedar vacío", "OK");
                return;
            }

            IsLoading = true;
            var model = new DireccionesModel();
            var result = await model.PostAsync(new Direcciones
            {
                Direccion = Direccion,
                Latitud = Latitud,
                Longitud = Longitud,
                Nombre = DetalleDireccion,        
                Principal = true
            });

            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
            }
            else
            {
                // Cuando el registro se realizó con exito:
                await Application.Current.MainPage.DisplayAlert("Registro correcto", "La dirección se registró correctamente", "OK");
                RaisePropertyChanged("Close");
            }
            IsLoading = false;
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            await Guardar();
        });

        #endregion


    }
}
