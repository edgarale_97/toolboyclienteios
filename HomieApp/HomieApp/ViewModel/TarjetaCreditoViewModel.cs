﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    public class TarjetaCreditoViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private ObservableCollection<Tarjeta> _tarjeta;

        private string _userName;
        private string _cardNumber;
        private string _expireDate;
        private string _cvv;

        private int _type;

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                RaisePropertyChanged();
            }
        }

        public string CardNumber
        {
            get { return _cardNumber; }
            set
            {
                _cardNumber = value;
                RaisePropertyChanged();
            }
        }

        public string ExpirationDate
        {
            get { return _expireDate; }
            set
            {
                _expireDate = value;
                RaisePropertyChanged();
            }
        }

        public string Cvv
        {
            get { return _cvv; }
            set
            {
                _cvv = value;
                RaisePropertyChanged();
            }
        }

        public int Type
        {
            get { return _type; }
            set
            {
                _type = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Tarjeta> Tarjetas
        {
            get { return _tarjeta; }
            set
            {
                _tarjeta = value;
                RaisePropertyChanged();
            }
        }

        public Func<string, Task<string>> EvaluateJavascript { get; set; }

        #endregion

        #region Constructor

        public TarjetaCreditoViewModel()
        {
            Tarjetas = new ObservableCollection<Tarjeta>();
        }

        #endregion

        public async Task Buscar()
        {
            Tarjetas = new ObservableCollection<Tarjeta>();

            IsLoading = true;
            var model = new TarjetasModel();
            var result = await model.GetListAsync();

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    foreach (var tarjeta in result.Result.OrderByDescending(x => x.Principal))
                    {
                        Tarjetas.Add(tarjeta);
                    }
                }

                IsLoading = false;
            });

        }

        public async Task NuevaTarjeta()
        {
            IsLoading = true;
            // Evaluamos el deviceSesionId
            var res = await EvaluateJavascript("$('#deviceSessionId').val();");
            var deviceSessionId = res.Replace("\"", "");
            if (string.IsNullOrWhiteSpace(UserName))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El nombre es obligatorio", "OK");
                IsLoading = false;
                return;
            }
            if (string.IsNullOrWhiteSpace(CardNumber))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El número de la tarjeta es obligatorio", "OK");
                IsLoading = false;
                return;
            }
            if (string.IsNullOrWhiteSpace(ExpirationDate))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El fecha de expiración es obligatoria", "OK");
                IsLoading = false;
                return;
            }
            if (string.IsNullOrWhiteSpace(Cvv))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "El código de seguridad es obligatorio", "OK");
                IsLoading = false;
                return;
            }

            string cadena = ExpirationDate;
            char delimitador = '/';
            string[] valores = cadena.Split(delimitador);
            int month = Int32.Parse(valores[0]);
            int year = Int32.Parse(valores[1]);
            var t = new Tarjeta
            {
                CardNumber = CardNumber,
                Csc = Cvv,
                FirstName = UserName,
                ExpiresMonth = month,
                ExpireYear = year,
            };

            var model = new TarjetasModel();
            var result = await model.PostTarjeta(t, deviceSessionId);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Settings.MetodoPago = true;
                    RaisePropertyChanged("Completed");
                }

                IsLoading = false;
            });
        }

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            await NuevaTarjeta();
        });

    }
}

