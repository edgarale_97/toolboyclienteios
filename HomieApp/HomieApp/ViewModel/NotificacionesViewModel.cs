﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Plugin.Badge;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    class NotificacionesViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private int _pagina;
        private ObservableCollection<Notificaciones> _notificaciones;
        private bool _notificacion;
        

        public bool Notificacion
        {
            get { return _notificacion; }
            set
            {
                _notificacion = value;               
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Notificaciones> Notificaciones
        {
            get { return _notificaciones; }
            set
            {
                _notificaciones = value;
                RaisePropertyChanged();
            }
        }

        public int Pagina
        {
            get { return _pagina; }
            set
            {
                _pagina = value; 
                RaisePropertyChanged();
            }
        }

        public int TotalPages { get; set; }

        #endregion

        #region Constructor

        public NotificacionesViewModel()
        {
            Pagina = 0;
            TotalPages = 0;
            Notificaciones = new ObservableCollection<Notificaciones>();
            //Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            if (Pagina == 0)
            {
                TotalPages = 0;
                Notificaciones = new ObservableCollection<Notificaciones>();
            }

            //if (Pagina < TotalPages || Pagina == 0)
            //{
            if (IsLoading == false)
            {
                IsLoading = true;
                var model = new NotificacionesModel();
                var result = await model.GetListAsync(Pagina);

                if( result.Result.Count == 0)
                {
                    Notificacion = true;
                    IsLoading = false;
                }
                else
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (result.Error)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                        }
                        else
                        {
                            CrossBadge.Current.ClearBadge();
                            Settings.Notificaciones = 0;
                            MessagingCenter.Send((App)Application.Current, "Notificaciones");
                            foreach (var item in result.Result)
                            {
                                Notificaciones.Add(item);
                            }
                            TotalPages = result.TotalPages;
                            Pagina++;
                        }
                        Notificacion = false;
                        IsLoading = false;
                    });
                }
               
               
            }
            //}
        }

        public async Task Eliminar(long idNotificacion)
        {
            IsLoading = true;
            var model = new NotificacionesModel();
            var result = await model.DeleteAsync(idNotificacion);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Notificaciones.Remove(Notificaciones.FirstOrDefault(x => x.IdNotificacion == idNotificacion));

                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand BuscarCommand => new RelayCommand(async () =>
        {
            Notificaciones = new ObservableCollection<Notificaciones>();
            Pagina = 0;
            TotalPages = 0;

            await Buscar();     

        });

        public RelayCommand<long> EliminarCommand => new RelayCommand<long>(async (id) =>
        {
            await Eliminar(id);
        });

        #endregion
    }
}
