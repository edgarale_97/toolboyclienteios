﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class NavigationTipoViewModel : HomieViewModelBase
    {
        #region Properties

        private bool _tipoUrgente;
        private bool _tipoAgendado;
        private bool _isVisibleCheckUrgente;
        private bool _isVisibleCheckAgendado;
        private Direcciones _direccion;
        private ObservableCollection<Categorias> _categorias;
        private Categorias _categoriaSeleccionada;

        public bool TipoUrgente
        {
            get { return _tipoUrgente; }
            set
            {
                _tipoUrgente = value;
                RaisePropertyChanged();
            }
        }

        public bool TipoAgendado
        {
            get { return _tipoAgendado; }
            set
            {
                _tipoAgendado = value;
                RaisePropertyChanged();
            }
        }

        public bool IsVisibleCheckUrgente
        {
            get { return _isVisibleCheckUrgente; }
            set { _isVisibleCheckUrgente = value; RaisePropertyChanged(); }
        }

        public bool IsVisibleCheckAgendado
        {
            get { return _isVisibleCheckAgendado; }
            set { _isVisibleCheckAgendado = value; RaisePropertyChanged(); }
        }

        public Direcciones Direccion
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Categorias> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public Categorias CategoriaSeleccionada
        {
            get { return _categoriaSeleccionada; }
            set
            {
                _categoriaSeleccionada = value;

                if (Categorias != null && _categoriaSeleccionada != null)
                {
                    Categorias.ToList()
                        .ForEach(x =>
                        {
                            x.Seleccionado = false;
                        });

                    _categoriaSeleccionada.Seleccionado = true;
                }

                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public NavigationTipoViewModel()
        {
            Categorias = new ObservableCollection<Categorias>();
            TipoUrgente = false;
            TipoAgendado = true;
            UpdateChecks();
            BuscarDireccion();
        }

        #endregion

        #region Methods

        private async void BuscarDireccion()
        {
            IsLoading = true;
            var modelCat = new CategoriasModel();
            var resultCat = await modelCat.GetList(0);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (resultCat.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", resultCat.ErrorDescription, "OK");
                }
                else
                {
                    foreach (var categoria in resultCat.Result)
                    {
                        Categorias.Add(categoria);

                    }

                }

                IsLoading = false;
            });

        }

        private void UpdateChecks()
        {
            IsVisibleCheckUrgente = false;
            IsVisibleCheckAgendado = false;

            if (TipoUrgente)
            {
                IsVisibleCheckUrgente = true;
            }
            if (TipoAgendado)
            {
                IsVisibleCheckAgendado = true;
            }
        }

        #endregion

        #region Commands

        public RelayCommand UrgenteCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    TipoUrgente = true;
                    TipoAgendado = false;
                    UpdateChecks();
                });
            }
        }

        public RelayCommand AgendadoCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    TipoUrgente = false;
                    TipoAgendado = true;
                    UpdateChecks();
                });
            }
        }

        #endregion

    }
}


