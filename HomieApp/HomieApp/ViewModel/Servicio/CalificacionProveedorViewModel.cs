﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class CalificacionProveedorViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private CalificacionData _data;
        private Calificacion _calificacion;
        private bool _close;

        public CalificacionData Data
        {
            get { return _data; }
            set
            {
                _data = value;
                RaisePropertyChanged();
            }
        }

        public Calificacion Calificacion
        {
            get { return _calificacion; }
            set
            {
                _calificacion = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public CalificacionProveedorViewModel(CalificacionData data)
        {
            Data = data;
            Calificacion = new Calificacion();
        }

        #endregion

        #region Methods

        public async Task Calificar()
        {
            IsLoading = true;
            var model = new CalificacionesModel();
            Calificacion.IdServicio = Data.IdServicio;
            Calificacion.Tipo = TiposCalificacion.ClienteProveedor;
            var result = await model.PostAsync(Calificacion);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Close = true;
                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand CalificarCommand => new RelayCommand(() =>
        {
            Calificar();
        });

        #endregion
    }
}

