﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.Helpers;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class NavigationDireccionViewModel : HomieViewModelBase
    {
        #region Fileds and Properties

        private ObservableCollection<Direcciones> _direccion;
        private Tarjeta _direccionSeleccionada;

        public ObservableCollection<Direcciones> Direcciones
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public NavigationDireccionViewModel()
        {
            Direcciones = new ObservableCollection<Direcciones>();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {

            Direcciones = new ObservableCollection<Direcciones>();

            if (Settings.Principal == true)
            {
                Direcciones.Add(new Entities.Direcciones
                {
                    //IdDireccion = Settings.IdDireccion,
                    Direccion = Settings.Direccion,
                    Latitud = Settings.Latitud,
                    Longitud = Settings.Longitud
                });
            }
            else
            {
                //Application.Current.MainPage.DisplayAlert("False", "De nulo", "OK");
            }

        }

        #endregion
    }
}

