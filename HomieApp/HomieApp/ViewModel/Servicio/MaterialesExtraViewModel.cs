﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    class MaterialesExtraViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private Tarifas _tarifa;
        private ObservableCollection<ServicioMateriales> _materialesExtra;
        private string _cotizacionInicialString;
        private string _totalMaterialesExtraString;
        private string _totalString;
        private bool _close;
        private bool _permitidoModificar;

        public Tarifas Tarifa
        {
            get { return _tarifa; }
            set
            {
                _tarifa = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<ServicioMateriales> MaterialesExtra
        {
            get { return _materialesExtra; }
            set
            {
                _materialesExtra = value; 
                RaisePropertyChanged();
            }
        }

        public string CotizacionInicialString
        {
            get { return _cotizacionInicialString; }
            set
            {
                _cotizacionInicialString = value;
                RaisePropertyChanged();
            }
        }

        public string TotalMaterialesExtraString
        {
            get { return _totalMaterialesExtraString; }
            set
            {
                _totalMaterialesExtraString = value;
                RaisePropertyChanged();
            }
        }

        public string TotalString
        {
            get { return _totalString; }
            set
            {
                _totalString = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        public long IdProveedor { get; set; }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value; 
                RaisePropertyChanged();
            }
        }

        public bool PermitidoModificar
        {
            get { return _permitidoModificar; }
            set
            {
                _permitidoModificar = value;
                RaisePropertyChanged();
            }
        }


        #endregion

        #region Constructor

        public MaterialesExtraViewModel(long idServicio, long idProveedor)
        {
            PermitidoModificar = false;
            Tarifa = new Tarifas();
            MaterialesExtra = new ObservableCollection<ServicioMateriales>();
            IdServicio = idServicio;
            IdProveedor = idProveedor;
            
            BuscarMateriales();
        }

        #endregion

        #region Methods

        public async Task BuscarTarifa()
        {
            IsLoading = true;
            var model = new TarifasModel();
            var result = await model.GetAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Tarifa = result.Result;
                }
                IsLoading = false;
            });
        }

        public async Task BuscarMateriales()
        {
            IsLoading = true;
            await BuscarTarifa();
            MaterialesExtra = new ObservableCollection<ServicioMateriales>();
            var model = new MaterialesModel();
            var result = await model.GetAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    decimal totalMateriales = 0;
                    decimal totalMaterialesExtra = 0;
                    foreach (var material in result.Result)
                    {
                        if (material.MaterialExtra && !(material.AceptadoCliente ?? false))
                        {
                            MaterialesExtra.Add(material);
                            totalMaterialesExtra += material.Importe;
                            if (material.AceptadoCliente == null || !material.AceptadoCliente.Value)
                            {
                                PermitidoModificar = true;
                            }
                        }
                        else
                        {
                            totalMateriales += material.Importe;
                        }
                        
                    }
                    var totalInicial = totalMateriales + Tarifa.Monto;
                    CotizacionInicialString = totalInicial.ToString("C");
                    TotalMaterialesExtraString = totalMaterialesExtra.ToString("C");
                    var total = totalInicial + totalMaterialesExtra;
                    TotalString = total.ToString("C");
                }
                IsLoading = false;
            });
        }

        public async Task AceptarCotizacion(bool aceptar)
        {
            IsLoading = true;

            var model = new CotizacionClienteModel();
            var result = await model.PostAsync(IdServicio, aceptar, true);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    var chatModel = new ChatModel();
                    if (aceptar)
                    {
                        await chatModel.PostAsync(IdServicio, "El cliente aceptó los materiales extra", IdProveedor,
                            null, null, false);
                        MessagingCenter.Send<MaterialesExtraViewModel>(this,"MaterialesExtraAceptados");
                    }
                    else
                    {
                        await chatModel.PostAsync(IdServicio, "El cliente rechazó los materiales extra", IdProveedor,
                            null, null);
                    }
                    Close = true;
                }

                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand AceptarCommand => new RelayCommand(async () =>
        {
            if (await Application.Current.MainPage.DisplayAlert("Pregunta", "¿Está seguro que acepta la cotización?", "Aceptar", "Cancelar")) 
                await AceptarCotizacion(true);
        });

        public RelayCommand CancelarCommand => new RelayCommand(async () =>
        {
            if (await Application.Current.MainPage.DisplayAlert("Pregunta", "¿Está seguro de rechazar la cotización?", "Aceptar", "Cancelar"))
                await AceptarCotizacion(false);
        });

        #endregion
    }
}
