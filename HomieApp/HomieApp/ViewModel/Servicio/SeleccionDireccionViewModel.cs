﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class SeleccionDireccionViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private ObservableCollection<Direcciones> _direccion;
        private Tarjeta _direccionSeleccionada;

        public ObservableCollection<Direcciones> Direcciones
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SeleccionDireccionViewModel()
        {
            Direcciones = new ObservableCollection<Direcciones>();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            Direcciones = new ObservableCollection<Direcciones>();
            IsLoading = true;
            var model = new DireccionesModel();
            var result = await model.GetListAsync();
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    foreach (var tarjeta in result.Result.OrderByDescending(x => x.Principal))
                    {
                        Console.WriteLine($"Direcciones notas ----> {tarjeta.Nombre}");
                        Direcciones.Add(tarjeta);
                    }
                }
                IsLoading = false;
            });

        }

        public async Task UpdateDireccion(Direcciones direcciones)
        {
            Direcciones = new ObservableCollection<Direcciones>();
            IsLoading = true;
            var model = new DireccionesModel();
            var result = await model.UpdateAsync(direcciones);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    foreach(var dir in result.Result.OrderByDescending(x => x.Principal))
                    {
                        Direcciones.Add(dir);
                    }
                }
                IsLoading = false;
            });
        }

        public async Task Eliminar(long idDireccion)
        {
            if (!await Application.Current.MainPage.DisplayAlert("Confirmar", "Está seguro que desea eliminar la dirección seleccionada?", "Continuar", "Cancelar"))
            {
                return;
            }

            IsLoading = true;

            var model = new DireccionesModel();
            var result = await model.DeleteAsync(idDireccion);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Buscar();
                }

                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand BuscarCommand => new RelayCommand(async () =>
        {
            await Buscar();
        });

        public RelayCommand<long> EliminarCommand => new RelayCommand<long>(async (id) =>
        {
            await Eliminar(id);
        });

        #endregion
    }
}
