﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class DetalleProveedorViewModel : HomieViewModelBase
    {
        private Entities.DetalleProveedor _detalleProveedor;
        private List<Catalogo> _trabajos;

        #region Properties

        public Entities.DetalleProveedor DetalleProveedor
        {
            get { return _detalleProveedor; }
            set
            {
                _detalleProveedor = value;
                RaisePropertyChanged();
            }
        }

        public List<Catalogo> Trabajos
        {
            get
            {
                return _trabajos;
            }
            set
            {
                _trabajos = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public DetalleProveedorViewModel(long idProveedor)
        {
            Buscar(idProveedor);
        }

        #endregion

        #region Metodos

        private async Task Buscar(long idProveedor)
        {
            IsLoading = true;

            var model = new DetalleProveedorModel();
            var result = await model.GetByIdAsync(idProveedor);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    var trabajos = new List<Catalogo>();
                    DetalleProveedor = result.Result;
                    //Se convierten en lista las imagenes
                    if (!string.IsNullOrEmpty(DetalleProveedor.TrabajoFoto1))
                    {
                        trabajos.Add(new Catalogo
                        {
                            Id = (int)DetalleProveedor.IdProveedor,
                            Nombre = DetalleProveedor.TrabajoFoto1
                        });
                    }

                    if (!string.IsNullOrEmpty(DetalleProveedor.TrabajoFoto2))
                    {
                        trabajos.Add(new Catalogo
                        {
                            Id = (int)DetalleProveedor.IdProveedor,
                            Nombre = DetalleProveedor.TrabajoFoto2
                        });
                    }

                    if (!string.IsNullOrEmpty(DetalleProveedor.TrabajoFoto3))
                    {
                        trabajos.Add(new Catalogo
                        {
                            Id = (int)DetalleProveedor.IdProveedor,
                            Nombre = DetalleProveedor.TrabajoFoto3
                        });
                    }

                    Trabajos = trabajos;

                }

                IsLoading = false;
            });
        }

        #endregion

        #region Commandos




        #endregion
    }
}
