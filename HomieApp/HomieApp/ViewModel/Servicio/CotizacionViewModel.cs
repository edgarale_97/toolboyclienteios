﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class CotizacionViewModel : HomieViewModelBase
    {
        private Entities.Servicio _servicio;
        private bool _close;

        private bool isAceptado;

        #region Properties

        public Entities.Servicio Servicio
        {
            get { return _servicio; }
            set
            {
                _servicio = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public bool IsAceptado
        {
            get { return isAceptado; }
            set
            {
                isAceptado = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public CotizacionViewModel(long idServicio)
        {
            IsAceptado = false;
            Buscar(idServicio);
        }

        #endregion

        #region Methods

        private async Task Buscar(long idServicio)
        {
            IsLoading = true;

            var model = new ServiciosModel();
            var result = await model.GetByIdAsync(idServicio);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Servicio = result.Result;
                    foreach(var material in Servicio.Materiales)
                    {
                        Console.WriteLine($"Resultado de materiales ---> {material.AceptadoCliente}");
                        if((material.AceptadoCliente == true) && (material.IdServicioMaterial != 0))
                        {
                            IsAceptado = true;
                        }
                    }
                    //Console.WriteLine($"Material Aceptado ---> {IsAceptado}");
                }

                IsLoading = false;
            });

        }

        public async Task AceptarCotizacion(bool aceptar)
        {
            IsLoading = true;
            var todosAceptados = true;

            //Material Aceptado
            if (IsAceptado)
            {
                foreach (var material in Servicio.Materiales)
                {
                    if (!(material.AceptadoCliente ?? false) && !material.MaterialExtra)
                    {
                        todosAceptados = false;
                    }
                }

                if (todosAceptados)
                {
                    await Application.Current.MainPage.DisplayAlert("Error",
                        "La cotización fue aceptada anteriormente, no es posible aceptarla o rechazarla", "OK");
                    IsLoading = false;
                    return;
                }


                var model = new CotizacionClienteModel();
                var result = await model.PostAsync(Servicio.IdServicio, aceptar);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        // En esta parte se envia la notificacion de si fue aceptada o no la cotizacion.
                        var chatModel = new ChatModel();
                        if (aceptar)
                        {
                            // Manda notificacion que fue aceptada.
                            await chatModel.PostAsync(Servicio.IdServicio, "El cliente aceptó la cotización", Servicio.IdProveedor, null, null, false);
                            // Enviamos un message para abrir la pantalla del servicio.
                            MessagingCenter.Send<CotizacionViewModel, long>(this, "AceptaCotizacion", Servicio.IdServicio);
                            //Close = true;
                        }
                        else
                        {
                            // Manda notificacion que fue rechazada.
                            await chatModel.PostAsync(Servicio.IdServicio, "El cliente rechazó la cotización",
                                Servicio.IdProveedor, null, null);
                            // Enviamos un message para abrir la pantalla del servicio.
                            Close = true;
                        }

                    }

                    IsLoading = false;
                });
            }
            else
            {
                foreach (var material in Servicio.Materiales)
                {
                    if (!(material.AceptadoCliente ?? false) && !material.MaterialExtra)
                    {
                        todosAceptados = false;
                    }
                }

                if (todosAceptados)
                {
                    await Application.Current.MainPage.DisplayAlert("Error",
                        "La cotización fue rechazada anteriormente, no es posible aceptarla o rechazarla", "OK");
                    IsLoading = false;
                    return;
                }


                var model = new CotizacionClienteModel();
                var result = await model.PostAsync(Servicio.IdServicio, aceptar);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        // En esta parte se envia la notificacion de si fue aceptada o no la cotizacion.
                        var chatModel = new ChatModel();
                        if (aceptar)
                        {
                            // Manda notificacion que fue aceptada.
                            await chatModel.PostAsync(Servicio.IdServicio, "El cliente aceptó la cotización", Servicio.IdProveedor, null, null, false);
                            // Enviamos un message para abrir la pantalla del servicio.
                            MessagingCenter.Send<CotizacionViewModel, long>(this, "AceptaCotizacion", Servicio.IdServicio);
                            //Close = true;
                        }
                        else
                        {
                            // Manda notificacion que fue rechazada.
                            await chatModel.PostAsync(Servicio.IdServicio, "El cliente rechazó la cotización",
                                Servicio.IdProveedor, null, null);
                            // Enviamos un message para abrir la pantalla del servicio.
                            Close = true;
                        }

                    }

                    IsLoading = false;
                });
            }
            
        }

        #endregion

        #region Commands

        public RelayCommand AceptarCommand => new RelayCommand(async () =>
        {
            if (await Application.Current.MainPage.DisplayAlert("Pregunta", "¿Está seguro que acepta la cotización?", "Aceptar", "Cancelar"))
                await AceptarCotizacion(true);
        });

        public RelayCommand CancelarCommand => new RelayCommand(async () =>
        {
            if (await Application.Current.MainPage.DisplayAlert("Pregunta", "¿Está seguro de rechazar la cotización?", "Aceptar", "Cancelar"))
                await AceptarCotizacion(false);
        });

        #endregion
    }
}
