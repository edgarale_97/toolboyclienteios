﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Helpers;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class NavigationBusquedaViewModel : HomieViewModelBase
    {
        #region Properties

        public string Direccion { get; set; }

        public double Latitud { get; set; }
        public double Longitud { get; set; }

        public int IdDireccion { get; set; }
        public bool Principal { get; set; }

        #endregion

        #region Constructor

        public NavigationBusquedaViewModel()
        {

        }

        #endregion

        #region Methods

        public async Task Guardar()
        {
            if (string.IsNullOrWhiteSpace(Direccion))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe seleccionar una dirección", "OK");
                return;
            }

            Settings.IdDireccion = IdDireccion;
            Settings.Direccion = Direccion;
            Settings.Latitud = Latitud;
            Settings.Longitud = Longitud;
            Settings.Principal = true;

            RaisePropertyChanged("Close");

        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            await Guardar();
        });

        #endregion

    }
}

