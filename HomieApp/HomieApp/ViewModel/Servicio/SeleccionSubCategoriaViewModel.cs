﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class SeleccionSubCategoriaViewModel : HomieViewModelBase
    {
        #region Properties

        private ObservableCollection<Categorias> _categorias;
        private Categorias _categoriasSeleccionada;
        private int _idCategoriaPadre;
        private Direcciones _direccion;
        private Categorias _categoria;
        private Categorias _chat;

        public ObservableCollection<Categorias> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public Categorias Chat
        {
            get { return _chat; }
            set
            {
                _chat = value;
                RaisePropertyChanged();
            }
        }

        public Categorias CategoriaSeleccionada
        {
            get { return _categoriasSeleccionada; }
            set
            {
                _categoriasSeleccionada = value;

                if (Categorias != null && _categoriasSeleccionada != null && _categoriasSeleccionada.IdCategorias != 0)
                {
                    _categoriasSeleccionada.Seleccionado = !_categoriasSeleccionada.Seleccionado;
                    CategoriaSeleccionada = null;
                }
                RaisePropertyChanged();
            }
        }

        public Direcciones Direcciones
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        public Categorias Categoria
        {
            get { return _categoria; }
            set
            {
                _categoria = value;
                if (Categoria != null && _categoria != null)
                {
                    Categorias.ToList()
                        .ForEach(x =>
                        {
                            x.Seleccionado = false;
                        });

                    _categoria.Seleccionado = true;

                }
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SeleccionSubCategoriaViewModel(int idCategoria, Direcciones direccion)
        {
            _idCategoriaPadre = idCategoria;
            _direccion = direccion;

            Categorias = new ObservableCollection<Categorias>();
            Buscar();
        }

        #endregion

        #region Methods

        private async void Buscar()
        {
            Direcciones = new Direcciones();

            IsLoading = true;
            var model = new DireccionesModel();
            var result = await model.GetListAsync();

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Direcciones = result.Result.FirstOrDefault(x => x.Principal);
                }

                IsLoading = true;
                var modelSubCat = new CategoriasTarifaModel();
                var resultSubCat = await modelSubCat.GetSubList(_idCategoriaPadre, _direccion.IdDireccion);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (resultSubCat.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        foreach (var subCategoria in resultSubCat.Result)
                        {
                            if(subCategoria.IdCategorias != 0)
                            {
                                Categorias.Add(subCategoria);
                            }
                            else
                            {
                                _chat = subCategoria;
                                _chat.Seleccionado = true;
                            }
                        }

                        RaisePropertyChanged("FinalizarBuscar");
                    }

                    IsLoading = false;
                });
            });
        }

        #endregion
    }
}

