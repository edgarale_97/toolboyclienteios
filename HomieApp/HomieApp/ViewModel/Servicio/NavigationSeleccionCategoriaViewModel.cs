﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class NavigationSeleccionCategoriaViewModel : HomieViewModelBase
    {

        #region Fields and Properties

        private ObservableCollection<Categorias> _categorias;
        private Categorias _categoriaSeleccionada;
        private int _idCategoriaPadre;

        public ObservableCollection<Categorias> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public Categorias CategoriaSeleccionada
        {
            get { return _categoriaSeleccionada; }
            set
            {
                _categoriaSeleccionada = value;

                if (Categorias != null && _categoriaSeleccionada != null)
                {
                    _categoriaSeleccionada.Seleccionado = !_categoriaSeleccionada.Seleccionado;
                    CategoriaSeleccionada = null;
                }

                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public NavigationSeleccionCategoriaViewModel(int idCategoria)
        {
            _idCategoriaPadre = idCategoria;

            Categorias = new ObservableCollection<Categorias>();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            IsLoading = true;
            var model = new CategoriasTarifaModel();
            var result = await model.GetList(_idCategoriaPadre, 179);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    foreach (var categoria in result.Result)
                    {
                        Categorias.Add(categoria);
                    }

                    RaisePropertyChanged("FinalizarBuscar");
                }

                IsLoading = false;

            });
        }

        #endregion

        /*public RelayCommand AceptarCommand => new RelayCommand(async () => 
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Confirmar", "Para seguir navengando debe registrarse.", "OK", "NO");
            if (answer)
            {
                Console.WriteLine("To Register ------> ");
                await Navigation.PushModalAsync(new SignUpPage());
            }
        });*/

    }
}

