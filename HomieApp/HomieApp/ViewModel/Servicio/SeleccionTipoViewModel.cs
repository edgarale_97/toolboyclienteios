﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;
using XLabs;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace HomieApp.ViewModel.Servicio
{
    public class SeleccionTipoViewModel : HomieViewModelBase
    {
        #region Properties

        private bool _tipoUrgente;
        private bool _tipoAgendado;
        private bool _isVisibleCheckUrgente;
        private bool _isVisibleCheckAgendado;
        private Direcciones _direccion;
        private ObservableCollection<Categorias> _categorias;
        private Categorias _categoriaSeleccionada;

        private ObservableCollection<string> _listIcono;

        public bool TipoUrgente
        {
            get { return _tipoUrgente; }
            set
            {
                _tipoUrgente = value;
                RaisePropertyChanged();
            }
        }

        public bool TipoAgendado
        {
            get { return _tipoAgendado; }
            set
            {
                _tipoAgendado = value;
                RaisePropertyChanged();
            }
        }

        public bool IsVisibleCheckUrgente
        {
            get { return _isVisibleCheckUrgente; }
            set { _isVisibleCheckUrgente = value; RaisePropertyChanged();}
        }

        public bool IsVisibleCheckAgendado
        {
            get { return _isVisibleCheckAgendado; }
            set { _isVisibleCheckAgendado = value; RaisePropertyChanged();}
        }

        public Direcciones Direccion
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Categorias> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<string> ListaIcono
        {
            get { return _listIcono; }
            set
            {
                _listIcono = value;
                RaisePropertyChanged();
            }
        }

        public Categorias CategoriaSeleccionada
        {
            get { return _categoriaSeleccionada; }
            set
            {
                _categoriaSeleccionada = value;

                if (Categorias != null && _categoriaSeleccionada != null)
                {
                    Categorias.ToList()
                        .ForEach(x =>
                        {
                            x.Seleccionado = false;
                        });

                    _categoriaSeleccionada.Seleccionado = true;
                }
                
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SeleccionTipoViewModel()
        {
            Categorias = new ObservableCollection<Categorias>();
            TipoUrgente = false;
            TipoAgendado = true;
            UpdateChecks();
            BuscarDireccion();
        }

        #endregion

        #region Methods

        private async void BuscarDireccion()
        {
            Direccion = new Direcciones();

             IsLoading = true;
             var model = new DireccionesModel();
             var result = await model.GetListAsync();

             Device.BeginInvokeOnMainThread(async () =>
             {
                 if (result.Error)
                 {
                     await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                 }
                 else
                 {
                     Direccion = result.Result.FirstOrDefault(x => x.Principal);
                 }

                 IsLoading = true;
                 var modelCat = new CategoriasModel();
                 var resultCat = await modelCat.GetListAsync(0);

                 Device.BeginInvokeOnMainThread(async () =>
                 {
                     if (result.Error)
                     {
                         await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                     }
                     else
                     {
                         foreach (var categoria in resultCat.Result)
                         {
                             categoria.Icono = Uri.EscapeUriString(categoria.Icono);
                             //ListaIcono.Add(categoria.Icono);
                             Categorias.Add(categoria);
                         }

                     }

                     IsLoading = false;
                 });
             });
        }

        private void UpdateChecks()
        {
            IsVisibleCheckUrgente = false;
            IsVisibleCheckAgendado = false;

            if (TipoUrgente)
            {
                IsVisibleCheckUrgente = true;
            }
            if (TipoAgendado)
            {
                IsVisibleCheckAgendado = true;
            }
        }

        #endregion

        #region Commands

        public RelayCommand UrgenteCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    TipoUrgente = true;
                    TipoAgendado = false;
                    UpdateChecks();
                });
            }
        }

        public RelayCommand AgendadoCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    TipoUrgente = false;
                    TipoAgendado = true;
                    UpdateChecks();
                });
            }
        }

        #endregion
    }
}
