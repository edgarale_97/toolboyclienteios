﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Plugin.Badge;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class ArchivosViewModel : HomieViewModelBase
    {
        #region Propiedades

        private ObservableCollection<Archivo> _listaArchivos;
        public ObservableCollection<Archivo> ListaArchivos
        {
            get
            {
                return _listaArchivos;
            }
            set
            {
                _listaArchivos = value;
                RaisePropertyChanged();
            }
        }

        private int _pagina;
        public int Pagina
        {
            get { return _pagina; }
            set
            {
                _pagina = value;
                RaisePropertyChanged();
            }
        }

        public int TotalPages { get; set; }

        public long IdServicio { get; set; }

        #endregion

        #region Constructor

        public ArchivosViewModel(long idServicio)
        {
            Pagina = 0;
            TotalPages = 0;
            ListaArchivos = new ObservableCollection<Archivo>();
            IdServicio = idServicio;
            Buscar(idServicio);
        }

        #endregion

        #region Metodos

        private async Task Buscar(long idServicio)
        {
            if (Pagina == 0)
            {
                TotalPages = 0;
                ListaArchivos = new ObservableCollection<Archivo>();
            }

            if (Pagina < TotalPages || Pagina == 0)
            {
                IsLoading = true;
                var model = new ArchivosModel();
                var result = await model.GetByIdAsync(idServicio);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");

                    }
                    else
                    {
                        foreach (var item in result.Result)
                        {
                            ListaArchivos.Add(item);
                        }
                        
                    }
                    IsLoading = false;
                });
            }
        }

        public async Task Descargar(string ruta)
        {
            IsLoading = true;
            Device.OnPlatform(
                iOS: () =>
                {
                    Device.OpenUri(new Uri(ruta));
                },
                Android:
                () =>
                {
                    Device.OpenUri(new Uri(ruta));
                });
            
                IsLoading = false;
          
        }

        #endregion

        #region Comandos

        public RelayCommand BuscarCommand => new RelayCommand(async () =>
        {
            ListaArchivos = new ObservableCollection<Archivo>();
            Pagina = 0;
            TotalPages = 0;

            await Buscar(IdServicio);

        });

        public RelayCommand<string> DescargarCommand => new RelayCommand<string>(async (ruta) =>
        {
            await Descargar(ruta);
        });

        #endregion
    }
}
