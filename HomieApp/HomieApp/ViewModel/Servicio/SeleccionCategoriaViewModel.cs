﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using HomieApp.Views.Servicio;
using Xamarin.Forms;

namespace HomieApp.ViewModel
{
    public class SeleccionCategoriaViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private ObservableCollection<Categorias> _categorias;
        private Categorias _categoriaSeleccionada;
        private int _idCategoriaPadre;
        private Direcciones _direccion;

        public ObservableCollection<Categorias> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public Categorias CategoriaSeleccionada
        {
            get { return _categoriaSeleccionada; }
            set
            {
                _categoriaSeleccionada = value;

                var servicios = Categorias.Where(x => x.Monto == 0);
                //if (servicios.Any())
                //{
                    if (Categorias != null && _categoriaSeleccionada != null)
                    {
                        Categorias.ToList()
                            .ForEach(x =>
                            {
                                x.Seleccionado = false;
                            });
                        _categoriaSeleccionada.Seleccionado = true;
                    }

                    RaisePropertyChanged();
                //}
                //else
                //{
                //    if (Categorias != null && _categoriaSeleccionada != null && _categoriaSeleccionada.IdCategorias != 0)
                //    {
                //        _categoriaSeleccionada.Seleccionado = !_categoriaSeleccionada.Seleccionado;
                //        CategoriaSeleccionada = null;
                //    }

                //    RaisePropertyChanged();
                //}
            }
        }

        /*public Categorias CategoriaSeleccionada
        {
            get { return _categoriaSeleccionada; }
            set
            {
                _categoriaSeleccionada = value;

                if (Categorias != null && _categoriaSeleccionada != null)
                {
                    //_categoriaSeleccionada.Seleccionado = !_categoriaSeleccionada.Seleccionado;
                    //CategoriaSeleccionada = null;
                    Categorias.ToList()
                        .ForEach(x =>
                        {
                            x.Seleccionado = false;
                        });
                    _categoriaSeleccionada.Seleccionado = true;
                }

                RaisePropertyChanged();
            }
        }*/

        #endregion

        #region Constructor

        public SeleccionCategoriaViewModel(int idCategoria, Direcciones direccion)
        {
            _idCategoriaPadre = idCategoria;
            _direccion = direccion;

            Categorias = new ObservableCollection<Categorias>();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            IsLoading = true;
            var model = new CategoriasTarifaModel();
            var result = await model.GetListAsync(_idCategoriaPadre, _direccion.IdDireccion);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    if(result.ErrorDescription.Equals("Lo sentimos, tu dirección no se encuentra dentro de la zona de cobertura"))
                    {
                        Settings.Aprobado = true;
                        var rootPage = new RootPage();
                        rootPage.CerrarSesion += Rootpage_CerrarSesion;
                        Application.Current.MainPage = rootPage;
                    }
                    return;
                }
                else
                {
                    Settings.Aprobado = false;
                    foreach (var categoria in result.Result)
                    {
                        Categorias.Add(categoria);
                    }

                    RaisePropertyChanged("FinalizarBuscar");
                }

                IsLoading = false;
            });
        }
        private void Rootpage_CerrarSesion(object sender, EventArgs e)
        {
            ((RootPage)sender).CerrarSesion -= Rootpage_CerrarSesion;
            //SetMainPage();
        }

        #endregion

        #region Commands

        #endregion
    }
}
