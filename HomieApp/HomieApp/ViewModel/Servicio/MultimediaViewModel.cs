﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class MultimediaViewModel : HomieViewModelBase
    {
        #region Properties

        private ServicioCreate _servicio;
        private ImageSource _image;

        private string _direccion;
        private string _nombre;

        private bool _isVigente;

        public ServicioCreate Servicio
        {
            get { return _servicio; }
            set { _servicio = value; RaisePropertyChanged(); }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged();
            }
        }

        public bool IsVigente
        {
            get { return _isVigente; }
            set
            {
                _isVigente = value;
                RaisePropertyChanged();
            }
        }

        public string Direccion
        {
            get { return _direccion; }
            set
            {
                _direccion = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public MultimediaViewModel(int idTipo, Categorias categoriaPadre, List<Categorias> categorias, Direcciones direccion, int activo, bool vigencia, int idRelacionCliente)
        {
            Servicio = new ServicioCreate();
            Servicio.Fecha = DateTime.Now.AddDays(1);
            Servicio.IdTipo = idTipo;
            Servicio.Activo = activo;
            Servicio.IsVigente = vigencia;
            Servicio.Categorias = categorias;
            Servicio.CategoriaPadre = categoriaPadre;
            Servicio.Direccion = direccion;
            Servicio.IdCategoriaPadre = categoriaPadre.IdCategorias;
            Servicio.IdsCategorias = categorias.Select(x => x.IdCategorias).ToList();
            Servicio.IdRelacionCliente = idRelacionCliente;
            Direccion = direccion.Direccion;
            if(direccion.Nombre != null) Servicio.NotaDireccion = direccion.Nombre;
            //IsVigente = vigencia;
        }

        #endregion

        #region Methods

        public async Task TakePicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 20)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(2000);
                    }
                    count++;
                }
            }

            Servicio.FotoBytes = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
            
        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "No se obtuvieron permisos para acceder a la galería", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                CompressionQuality = 30 

            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                var count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Servicio.FotoBytes = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
        }

        #endregion

        #region Commands

        public RelayCommand AceptarCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    if (Servicio.IdTipo == TipoServicio.Agendado)
                    {
                        var inicio = Servicio.Fecha + Servicio.HoraInicio;
                        if (inicio < DateTime.Now)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", "La hora seleccionada debe ser mayor a la hora actual", "OK");
                            return;
                        }
                    }

                    string mensaje;
                    var chat = Servicio.Categorias.FirstOrDefault(x => x.IdCategorias == 0);

                    if (string.IsNullOrEmpty(Servicio.CodigoDescuento) && chat != null && Servicio.IsVigente == false)
                    {
                        //mensaje = $"Se realizará un cargo inicial de ${chat.Monto:N2}. ¿Desea continuar?";
                        mensaje = "¿Esta seguro que desea crear el servicio?";
                    }
                    else
                    {
                        mensaje = "¿Esta seguro que desea crear el servicio?";
                    }
                    var answer = await Application.Current.MainPage.DisplayAlert("Confirmar", mensaje, "Si", "No");
                    if (answer)
                    {
                        IsLoading = true;

                        // Se guardan los cambios previos y se inicia la búsqueda de las proveedores.
                        var model = new ServiciosModel();
                        //Console.WriteLine($"IdDireccion: {Servicio.Direccion.IdDireccion}");
                        var result = await model.PostAsync(Servicio);

                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            if (result.Error)
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                            }
                            else
                            {
                                // Cerrar todas las views y abrir la ventana de // BuscandoProveedor
                                MessagingCenter.Send<MultimediaViewModel, long>(this, "BuscandoProveedor", result.Result);
                            }
                            IsLoading = false;
                        });
                    }

                    // SE COMENTA ESTE APARTADO PARA NO PREGUNTAR SOBRE EL MONTO QUE ES CERO (0).
                    // Verificamos si el servicio es normal o si es de vigencia

                    /*if (string.IsNullOrEmpty(Servicio.CodigoDescuento) && chat != null && Servicio.IsVigente == false)
                    {
                       mensaje = $"Se realizará un cargo inicial de ${chat.Monto:N2}. ¿Desea continuar?";
                    }
                    else
                    {
                        mensaje = "¿Esta seguro que desea crear el servicio?";
                    }
                    var answer = await Application.Current.MainPage.DisplayAlert("Confirmar", mensaje, "Si", "No");
                    if (answer)
                    {
                        IsLoading = true;

                        // Se guardan los cambios previos y se inicia la búsqueda de las proveedores.
                        var model = new ServiciosModel();
                        //Console.WriteLine($"IdDireccion: {Servicio.Direccion.IdDireccion}");
                        var result = await model.PostAsync(Servicio);

                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            if (result.Error)
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                            }
                            else
                            {
                                // Cerrar todas las views y abrir la ventana de // BuscandoProveedor
                                MessagingCenter.Send<MultimediaViewModel, long>(this, "BuscandoProveedor", result.Result);
                            }
                            IsLoading = false;
                        });
                    }*/
                });
            }
        }

        #endregion

    }
}
