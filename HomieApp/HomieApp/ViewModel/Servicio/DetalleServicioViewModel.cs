﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using HomieApp.Views.Servicio;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class DetalleServicioViewModel : HomieViewModelBase
    {
        private Entities.Servicio _servicio;
        private bool _closeCancelacion;
        private bool _isFinalizado;

        private bool _notaDireccion;

        private Entities.ServicioGarantia _servicioGarantia;

        #region Properties

        public Entities.ServicioGarantia ServiciosGarantias
        {
            get { return _servicioGarantia; }
            set
            {
                _servicioGarantia = value;
                RaisePropertyChanged();
            }
        }

        public Entities.Servicio Servicio
        {
            get { return _servicio; }
            set
            {
                _servicio = value;
                RaisePropertyChanged();
            }
        }

        public bool CloseCancelacion
        {
            get { return _closeCancelacion; }
            set
            {
                _closeCancelacion = value;
                RaisePropertyChanged();
            }
        }

        public bool IsFinalizado
        {
            get { return _isFinalizado; }
            set
            {
                _isFinalizado = value;
                RaisePropertyChanged();
            }
        }

        public bool NotaDireccion
        {
            get { return _notaDireccion; }
            set
            {
                _notaDireccion = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        #endregion

        #region Constructor

        public DetalleServicioViewModel(long idServicio)
        {
            IdServicio = idServicio;
            Buscar(idServicio);
        }

        #endregion

        #region Methods

        public async Task Buscar(long idServicio)
        {
            IsLoading = true;

            var model = new ServiciosModel();
            var result = await model.GetByIdAsync(idServicio);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    if(result.Result.DetalleDireccion != null)
                    {
                        NotaDireccion = true;
                    }
                    else
                    {
                        NotaDireccion = false;
                    }
                    var materiales = new List<ServicioMateriales>();
                    foreach (var material in result.Result.Materiales)
                    {
                        if ((material.AceptadoCliente ?? false))
                        {
                            materiales.Add(material);
                        }
                    }
                    Servicio = result.Result;
                    Servicio.Materiales = materiales;
                }

                IsLoading = false;
            });

        }

        public async Task CancelarServicio()
        {
            IsLoading = true;
            var model = new ServiciosModel();
            var result = await model.PostAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se cancelo el servicio", "OK");
                    CloseCancelacion = true;
                }
                IsLoading = false;
            });
        }

        public async Task ValidarGarantia()
        {
            // Cerrar todas las views y abrir la ventana de // BuscandoProveedor
            IsLoading = true;
            var cat = Servicio.Categorias;
            var idCategorias = cat.Select(x => x.IdCategorias).ToList();
            ServiciosGarantias = new Entities.ServicioGarantia();
            // Se guardan los cambios previos y se inicia la búsqueda de los proveedores.
            ServiciosGarantias.IdServicio = Servicio.IdServicio;
            ServiciosGarantias.IdsCategorias = idCategorias;
            var model = new Models.ServicioGarantia();
            var result = await model.PostAsync(ServiciosGarantias);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    // Cerrar todas las views y abrir la ventana de BuscandoProveedor
                    MessagingCenter.Send(this, "BuscandoProveedor", result.Result);
                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand CancelarCommand => new RelayCommand(async () =>
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Confirmar",
                "¿Esta seguro que desea cancelar el servicio?", "Si", "No");
            if (answer)
            {
                CancelarServicio();
            }
        });

        public RelayCommand GarantiaCommand => new RelayCommand(async () =>
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Confirmar", "¿Estas seguro de hacer valida la garantía?", "Sí", "No");
            if (answer)
            {
                await ValidarGarantia();
                /*IsLoading = true;
                var cat = Servicio.Categorias;
                var idCategorias = cat.Select(x => x.IdCategorias).ToList();
                ServiciosGarantias = new Entities.ServicioGarantia();
                // Se guardan los cambios previos y se inicia la búsqueda de los proveedores.
                ServiciosGarantias.IdServicio = Servicio.IdServicio;
                ServiciosGarantias.IdsCategorias = idCategorias;
                var model = new Models.ServicioGarantia();
                var result = await model.PostAsync(ServiciosGarantias);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        // Cerrar todas las views y abrir la ventana de BuscandoProveedor
                        MessagingCenter.Send<DetalleServicioViewModel, long>(this, "BuscandoProveedor", result.Result);
                        Console.WriteLine($"Resultado: {result.Result.ToString()}");
                    }
                    IsLoading = false;
                });*/
            }
        });

        //public RelayCommand TerminarServicioCommand => new RelayCommand(async () =>
        //{
        //    var answer = await Application.Current.MainPage.DisplayAlert("¿Terminar Servicio?",
        //        "¿Esta seguro que desea terminar el servicio?", "Si", "No");
        //    if (answer)
        //    {
        //        IsLoading = true;
        //        var model = new ServiciosModel();
        //        var result = await model.TerminarAsync(IdServicio);
        //        Device.BeginInvokeOnMainThread(async () =>
        //        {
        //            if (result.Error)
        //            {
        //                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
        //            }
        //            else
        //            {
        //                await Application.Current.MainPage.DisplayAlert("Correcto", "El servicio se finalizo correctamente","OK");
        //                MessagingCenter.Send<App>((App)Application.Current, "SeTerminoElServicio");
        //            }
        //            IsLoading = false;
        //        });
        //    }
        //});

        #endregion
    }
}
