﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Models;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    class SeguimientoProveedorViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private Ubicaciones _ubicaciones;

        public Ubicaciones Ubicaciones
        {
            get { return _ubicaciones; }
            set
            {
                _ubicaciones = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        #endregion

        #region Constructor

        public SeguimientoProveedorViewModel(long idServicio)
        {
            IdServicio = idServicio;
            Ubicaciones = new Ubicaciones();
            //BuscarUbicaciones();
        }

        #endregion

        #region Methods

        public async Task BuscarUbicaciones()
        {
            IsLoading = true;
            var model = new UbicacionesModel();
            var result = await model.GetAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Ubicaciones = result.Result;
                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand ActualizarCommand => new RelayCommand(() =>
        {
            BuscarUbicaciones();
        });

        #endregion
    }
}
