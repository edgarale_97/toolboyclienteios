﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieApp.ViewModel.Servicio
{
    public class ChatViewModel : HomieViewModelBase
    {
        #region Properties

        private ObservableCollection<ServicioChat> _mensajes;
        private string _mensaje;
        //private HubConnection _hubConnection;
        private long _idServicio;
        private Proveedor _proveedor;
        private bool _cotizacionRecibida;
        private string _chatPageId;
        private bool _cargandoArchivo;
        private int _idEstatus;

        public ObservableCollection<ServicioChat> Mensajes
        {
            get { return _mensajes; }
            set
            {
                _mensajes = value;
                RaisePropertyChanged();
            }
        }

        public string Mensaje
        {
            get { return _mensaje; }
            set
            {
                _mensaje = value;
                RaisePropertyChanged();
            }
        }

        public Proveedor Proveedor
        {
            get { return _proveedor; }
            set
            {
                _proveedor = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio
        {
            get { return _idServicio; }
            set
            {
                _idServicio = value;
                RaisePropertyChanged();
            }
        }

        public bool CotizacionRecibida
        {
            get { return _cotizacionRecibida; }
            set
            {
                _cotizacionRecibida = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public ChatViewModel(long idServicio)
        {
            _chatPageId = Guid.NewGuid().ToString();
            Settings.ChatPageId = _chatPageId;

            IdServicio = idServicio;
            Mensajes = new ObservableCollection<ServicioChat>();
            BuscarServicio();
            //Hub();
        }

        #endregion

        #region Commands

        public RelayCommand EnviarMensajeCommand => new RelayCommand(async () =>
        {

            if (string.IsNullOrWhiteSpace(Mensaje))
            {
                return;
            }

            await EnviarMensaje(Mensaje, null, null);
        });

        public RelayCommand EnviarImagenCommand => new RelayCommand(async () =>
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium,
                
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }

            if (array != null)
            {
                await EnviarMensaje(string.Empty, array, null);
            }
        });

        public RelayCommand EnviarVideoCommand => new RelayCommand(async () =>
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }

            try
            {
                var android = false;

                Device.OnPlatform(
                    iOS: () =>
                    {
                        android = false;
                    },
                    Android:
                    () =>
                    {
                        android = true;
                    });

                var file = await CrossMedia.Current.TakeVideoAsync(new StoreVideoOptions
                {
                    SaveToAlbum = false,
                    DefaultCamera = CameraDevice.Rear,
                    Quality = android ? VideoQuality.Low : VideoQuality.Medium,
                    // Tiempo maximo para la grabacion (30 segundos).
                    DesiredLength = new TimeSpan(0, 0, 0, 30)
                });

                if (file == null)
                {
                    return;
                }
                byte[] array;
                using (var memoryStream = new MemoryStream())
                {
                    int count = 0;
                    file.GetStream().CopyTo(memoryStream);
                    array = memoryStream.ToArray();
                    while (count < 10)
                    {
                        if (array.Count() == 0)
                        {
                            file.GetStream().CopyTo(memoryStream);
                            array = memoryStream.ToArray();
                            await Task.Delay(1000);
                        }
                        count++;
                    }
                }

                if (array != null)
                {
                    await EnviarMensaje(string.Empty, null, array);
                }
            }
            catch (Exception)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }
            
        });

        #endregion

        #region Methods

        private async Task EnviarMensaje(string mensaje, byte[] imagen, byte[] video)
        {
            IsLoading = true;

            if (imagen != null || video != null)
            {
                _cargandoArchivo = true;
            }

            var model = new ChatModel();
            var result = await model.PostAsync(IdServicio, mensaje, Proveedor.IdProveedor, imagen, video);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    if (imagen != null)
                    {
                        result.Result.Mensaje = "Enviaste una imágen";
                    }
                    else if (video != null)
                    {
                        result.Result.Mensaje = "Enviaste un video";
                        
                    }
                    else
                    {
                        result.Result.Mensaje = mensaje;
                    }

                    Mensajes.Add(result.Result);

                    Mensaje = string.Empty;
                    RaisePropertyChanged("Envia");
                }

                IsLoading = false;
                _cargandoArchivo = false;
            });
        }

        private async Task BuscarServicio()
        {
            IsLoading = true;

            var modelServicio = new ServiciosModel();
            var resultServicio = await modelServicio.GetByIdAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (resultServicio.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", resultServicio.ErrorDescription, "OK");
                }
                else
                {
                    if (!resultServicio.Result.ChatHabilitado)
                    {
                        //await Application.Current.MainPage.DisplayAlert("Información", "El chat no se encuentra habilitado para el servicio.", "OK");
                        MessagingCenter.Send((App)Application.Current, "DetalleServicio", IdServicio);
                        return;
                    }

                    Proveedor = new Proveedor
                    {
                        IdProveedor = resultServicio.Result.IdProveedor,
                        Nombre = resultServicio.Result.NombreProveedor
                    };

                    if (Proveedor != null && Proveedor.IdProveedor > 0)
                    {
                        IsLoading = true;
                        Mensajes = new ObservableCollection<ServicioChat>();

                        // Buscamos los mensajes en memoria y solamente consultamos a partir del último que exista.
                        var chatSettings = new SettingsChat();
                        var mensajes = chatSettings.GetByIdServicioChat(IdServicio);
                        var lastIdServicioChat = mensajes.Count > 0 ? mensajes.Max(x => x.IdServicioChat) : 0;
                        foreach (var mensaje in mensajes.OrderBy(x => x.IdServicioChat))
                        {
                            Mensajes.Add(mensaje);
                            if (mensaje.Mensaje == "El proveedor te envió una cotización" || mensaje.Mensaje == "El proveedor te envio una cotización" || mensaje.Mensaje ==
                                "El proveedor agregó materiales extra a la cotización" || mensaje.Mensaje == "El proveedor agrego materiales extra a la cotización")
                            {
                                CotizacionRecibida = true;
                            }
                        }

                        var model = new ChatModel();
                        var result = await model.GetAsync(IdServicio, lastIdServicioChat, Proveedor.IdProveedor);

                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            if (result.Error)
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                            }
                            else
                            {
                                foreach (var x in result.Result)
                                {
                                    _idEstatus = resultServicio.Result.Estatus;
                                    AddMessage(x, _idEstatus);

                                    chatSettings.AddElement(x);
                                }

                                RaisePropertyChanged("Envia");
                            }

                            IsLoading = false;
                        });
                    }
                }
                IsLoading = false;
            });
        }

        public async Task ActualizarMensaje()
        {
            var modelServicio = new ServiciosModel();
            var resultServicio = await modelServicio.GetByIdAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                Mensajes = new ObservableCollection<ServicioChat>();

                // Buscamos los mensajes en memoria y solamente consultamos a partir del último que exista.
                var chatSettings = new SettingsChat();
                var mensajes = chatSettings.GetByIdServicioChat(IdServicio);
                var lastIdServicioChat = mensajes.Count > 0 ? mensajes.Max(x => x.IdServicioChat) : 0;
                foreach (var mensaje in mensajes.OrderBy(x => x.IdServicioChat))
                {
                    Mensajes.Add(mensaje);
                    if (mensaje.Mensaje == "El proveedor te envió una cotización" ||
                        mensaje.Mensaje == "El proveedor te envio una cotización" || mensaje.Mensaje ==
                        "El proveedor agregó materiales extra a la cotización" || mensaje.Mensaje ==
                        "El proveedor agrego materiales extra a la cotización")
                    {
                        CotizacionRecibida = true;
                    }
                }

                var model = new ChatModel();
                var result = await model.GetAsync(IdServicio, lastIdServicioChat, Proveedor.IdProveedor);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        // Eliminamos los que no tengan id
                        var sinId = Mensajes.Where(x => x.IdServicioChat == 0).ToList();

                        foreach (var servicioChat in sinId)
                        {
                            Mensajes.Remove(servicioChat);
                        }


                        foreach (var x in result.Result)
                        {
                            _idEstatus = resultServicio.Result.Estatus;
                            AddMessage(x, _idEstatus);

                            chatSettings.AddElement(x);
                        }

                        RaisePropertyChanged("Envia");
                    }

                    IsLoading = false;
                });

            });
        }

        private void AddMessage(ServicioChat x, int estatus)
        {
            if (!string.IsNullOrWhiteSpace(x.Imagen))
            {
                if (x.TipoEnvio == 1)
                {
                    x.Mensaje = "Enviaste una imágen";
                }
                else
                {
                    x.Mensaje = "Te enviaron una imágen";
                }
            }

            if (!string.IsNullOrWhiteSpace(x.Video))
            {
                if (x.TipoEnvio == 1)
                {
                    x.Mensaje = "Enviaste un video";
                }
                else
                {
                    x.Mensaje = "Te enviaron un video";
                }
            }

            if (x.TipoEnvio != 1)
            {
                if (x.Mensaje == "El proveedor te envió una cotización" || x.Mensaje == "El proveedor te envio una cotización" || x.Mensaje ==
                    "El proveedor agregó materiales extra a la cotización" || x.Mensaje == "El proveedor agrego materiales extra a la cotización")
                {
                    CotizacionRecibida = true;
                }
            }

            Mensajes.Add(x);
        }

        //private async void Hub()
        //{
        //    // Connect to the server
        //    _hubConnection = new HubConnection(Settings.SignalrUri);
        //    _hubConnection.Headers.Add("Authorization", $"Bearer {Settings.Token}");

        //    // Create a proxy to the SignalR Hub
        //    var hubProxy = _hubConnection.CreateHubProxy("ChatHub");

        //    // Wire up a handler for the 'EdecanConfirmada' for the server
        //    // to be called on our client
        //    hubProxy.On<ServicioChat>("NotificarMensaje", (mensaje) =>
        //    {
        //        if (mensaje.IdServicio == IdServicio && mensaje.IdProveedor == Proveedor.IdProveedor)
        //        {
        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                AddMessage(mensaje, _idEstatus);
        //                RaisePropertyChanged("Envia");
        //            });
        //        }
        //    });

        //    // Start the connection
        //    await _hubConnection.Start();
        //}

        //public async void StartChatHub()
        //{
        //    if (_hubConnection == null && Settings.ChatPageId == _chatPageId)
        //    {
        //        if(!_cargandoArchivo)
        //            BuscarServicio();

        //        Hub();
        //    }
        //}

        //public async void StopChatHub()
        //{
        //    if (_hubConnection != null && Settings.ChatPageId == _chatPageId)
        //    {
        //        _hubConnection.Stop();
        //        _hubConnection = null;
        //    }
        //}

        public async Task TakePicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 30)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }

            if (array != null)
            {
                await EnviarMensaje(string.Empty, array, null);
            }
        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se otorgaron los permisos para accesar a las fotos", "OK");
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Small,
                CompressionQuality = 30
            });

            if (file == null)
                return;

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();

                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();

                        await Task.Delay(1000);
                    }

                    count++;
                }
            }

            if (array != null)
            {
                await EnviarMensaje(string.Empty, array, null);
            }
        }

        #endregion
    }
}
