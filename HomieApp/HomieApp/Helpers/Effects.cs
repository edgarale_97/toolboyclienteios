﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HomieApp.Helpers
{
    public static class Effects
    {
        public static readonly BindableProperty CircleProperty;
        public static readonly BindableProperty UnderlineProperty;
        public static readonly BindableProperty ShadowProperty;
        public static readonly BindableProperty ShadowIOSColorProperty;
        public static readonly BindableProperty ShadowSizeProperty;
        public static readonly BindableProperty CornerRadiusProperty;

        static Effects()
        {
            if (CircleProperty == null)
            {
                CircleProperty = BindableProperty.Create("Circle", typeof(bool), typeof(Effects), false, BindingMode.OneWay, null, OnChanged<CircleEffect, bool>);
            }

            if (UnderlineProperty == null)
            {
                UnderlineProperty = BindableProperty.Create("UnderlineProperty", typeof(bool), typeof(Effects), false, BindingMode.OneWay, null, OnChanged<UnderlineEffect, bool>);
            }

            if (ShadowProperty == null)
            {

                ShadowProperty = BindableProperty.CreateAttached("Shadow", typeof(bool), typeof(Effects), false, BindingMode.OneWay, null, OnChanged<ShadowEffect, bool>);
            }

            ShadowIOSColorProperty = BindableProperty.CreateAttached("ShadowIOSColor", typeof(Color), typeof(Effects), Color.Gray);
            ShadowSizeProperty = BindableProperty.CreateAttached("ShadowSize", typeof(double), typeof(Effects), 5.0);

            if (CornerRadiusProperty == null)
            {
                CornerRadiusProperty = BindableProperty.Create("CornerRadius", typeof(double), typeof(Effects), 0.0, BindingMode.OneWay, null, OnChanged<CornerRadiusEffect, double>);
            }
        }

        private static void OnChanged<TEffect, TProp>(BindableObject bindable, object oldValue, object newValue) where TEffect : Effect, new()
        {
            View view = bindable as View;
            if (view == null)
                return;
            if (Equals(newValue, default(TProp)))
            {
                Effect effect = view.Effects.FirstOrDefault(e => e is TEffect);
                if (effect == null)
                    return;
                view.Effects.Remove(effect);
            }
            else
                view.Effects.Add(Activator.CreateInstance<TEffect>());
        }

        public static void SetCircle(BindableObject view, bool circle)
        {
            view.SetValue(CircleProperty, circle);
        }

        public static bool GetCircle(BindableObject view)
        {
            return (bool)view.GetValue(CircleProperty);
        }

        public static void SetUnderline(BindableObject view, bool circle)
        {
            view.SetValue(UnderlineProperty, circle);
        }

        public static bool GetUnderline(BindableObject view)
        {
            return (bool)view.GetValue(UnderlineProperty);
        }

        public static void SetShadow(BindableObject view, bool shadow)
        {
            view.SetValue(ShadowProperty, shadow);
        }

        public static bool GetShadow(BindableObject view)
        {
            return (bool)view.GetValue(ShadowProperty);
        }

        public static void SetShadowIOSColor(BindableObject view, Color color)
        {
            view.SetValue(ShadowIOSColorProperty, color);
        }

        public static Color GetShadowIOSColor(BindableObject view)
        {
            return (Color)view.GetValue(ShadowIOSColorProperty);
        }

        public static void SetShadowSize(BindableObject view, double offset)
        {
            view.SetValue(ShadowSizeProperty, offset);
        }

        public static double GetShadowSize(BindableObject view)
        {
            return (double)view.GetValue(ShadowSizeProperty);
        }

        public static void SetCornerRadius(BindableObject view, double radius)
        {
            view.SetValue(CornerRadiusProperty, radius);
        }

        public static double GetCornerRadius(BindableObject view)
        {
            return (double)view.GetValue(CornerRadiusProperty);
        }

        private class CornerRadiusEffect : RoutingEffect
        {
            public CornerRadiusEffect() : base("HomieApp.CornerRadiusEffect")
            {
            }
        }

        private class CircleEffect : RoutingEffect
        {
            public CircleEffect() : base("HomieApp.CircleEffect")
            {

            }
        }

        private class ShadowEffect : RoutingEffect
        {
            public ShadowEffect() : base("HomieApp.ShadowEffect")
            {
            }
        }

        private class UnderlineEffect : RoutingEffect
        {
            public UnderlineEffect() : base("HomieApp.UnderlineEffect")
            {
            }
        }
    }
}
