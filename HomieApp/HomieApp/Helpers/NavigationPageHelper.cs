﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HomieApp.Helpers
{
    class NavigationPageHelper
    {
        public static NavigationPage Create(Page page)
        {
            return new NavigationPage(page) { BarTextColor = Color.White, BarBackgroundColor = Color.FromHex("#73CFB6"), BackgroundColor = Color.White };
        }
    }
}
