﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Helpers
{
    public enum BorderStyle
    {
        Default,
        None,
        BottomLine,
        Rect,
        RoundRect,
    }
}
