﻿using HomieApp.Entities;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace HomieApp.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        ////Producción
        public static string ApiUri => "https://toolboy.azurewebsites.net/api/";
        public static string TokenUri => "https://toolboy.azurewebsites.net/token";
        public static string SignalrUri => "https://toolboy.azurewebsites.net";


        //// DESARROLLO
        //public static string ApiUri => "http://toolboydev.azurewebsites.net/api/";
        //public static string TokenUri => "http://toolboydev.azurewebsites.net/token";
        //public static string SignalrUri => "http://toolboydev.azurewebsites.net";

        ////DESARROLLO LOCAL
        //public static string ApiUri => "http://192.168.0.105/HomieWeb/api/";
        //public static string TokenUri => "http://192.168.0.105/HomieWeb/token";
        //public static string SignalrUri => "http://192.168.0.105/HomieWeb";

        /////////Desarrollo OPEN PAY
        //public static string OpenPayApi = "https://sandbox-api.openpay.mx/v1/";
        //public static string OpenPayPublicKey = "pk_2b1d6e3fb4b94693a445ded92339d639";
        //public static string OpenPayId = "mbszlvjof7gjikkejl03";

        // Produccion OPEN PAY
        public static string OpenPayApi = "https://api.openpay.mx/v1/";
        public static string OpenPayPublicKey = "pk_03be2605ce244889af393e60535fd307";
        public static string OpenPayId = "m5qisbpsiqp3l0zpmqb2";

        // Terminos y condiciones
        public static string TerminosYCondiciones = "Términos y condiciones generales (en adelante, los \"Términos y Condiciones Generales\") aplicables al uso de los servicios ofrecidos por TOOLBOY (en adelante, \"los Servicios\") dentro del sitio https://www.toolboy.mx cualquier persona que desee acceder y/o usar el sitio o los servicios podrá hacerlo sujetándose a los Términos y Condiciones Generales, junto con todas las demás políticas y principios que rigen TOOLBOY y que son incorporados al presente por referencia.\n\nCUALQUIER PERSONA QUE NO ACEPTE ESTOS TÉRMINOS Y CONDICIONES GENERALES, LOS CUALES TIENEN UN CARÁCTER OBLIGATORIO Y VINCULANTE, DEBERÁ ABSTENERSE DE UTILIZAR EL SITIO Y/O LOS SERVICIOS.\n\nEl Usuario debe leer, entender y aceptar todas las condiciones establecidas en los Términos y Condiciones Generales y en el Aviso de Privacidad así como en los demás documentos incorporados a los mismos por referencia, previas a su inscripción como Usuario de TOOLBOY.\n\nLa utilización de los Servicios implica la aceptación expresa por parte del Usuario de todas las políticas establecidas en las Condiciones Generales y sus Anexos, y demás políticas y principios incorporados a las mismas por referencia.\n\n01 – CAPACIDAD\n\nLos Servicios sólo están disponibles para personas que tengan capacidad legal para contratar. No podrán utilizar los servicios las personas que no tengan esa capacidad, los menores de edad. Si estás inscribiendo un Usuario como Empresa, debes tener capacidad para contratar a nombre de tal entidad y de obligar a la misma en los términos de este Acuerdo.\n\n02 – INSCRIPCIÓN\n\nEs obligatorio completar el formulario de inscripción en todos sus campos con datos válidos para poder utilizar los servicios que brinda TOOLBOY. El futuro Usuario deberá completarlo con su información personal de manera exacta, precisa y verdadera (en adelante, los \"Datos Personales\") y asume el compromiso de actualizar y/o rectificar los Datos Personales conforme resulte necesario. \n\nEl Usuario autoriza a TOOLBOY a que utilice diversos medios para identificar sus datos personales, asumiendo el Usuario la obligación de revisarlos, rectificarlos y mantenerlos actualizados. TOOLBOY NO se responsabiliza por la certeza de los Datos Personales de sus Usuarios. Los Usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de sus Datos Personales.\n\nTOOLBOY se reserva el derecho de solicitar algún comprobante y/o dato adicional a efectos de corroborar los Datos Personales, así como de suspender temporal o definitivamente a aquellos Usuarios cuyos datos no hayan podido ser confirmados. \n\nEl Usuario accederá a su cuenta personal (\"Cuenta\") mediante el ingreso de su nombre de usuario o Seudónimo junto a la clave de seguridad personal elegida (\"Clave de Seguridad\"). El Usuario se obliga a mantener la confidencialidad de su Clave de Seguridad.\n\nAsimismo, todo lo que suceda con el estado y la facturación de su cuenta en TOOLBOY, se reflejará en la de los mencionados sitios web, y viceversa.\n\nLa Cuenta es personal, única e intransferible, y está prohibido que un mismo Usuario inscriba o posea más de una Cuenta. En caso que TOOLBOY detecte distintas Cuentas que contengan datos coincidentes o relacionados, podrá cancelar, suspender o inhabilitarlas.\n\nEl Usuario será responsable por todas las operaciones efectuadas en su Cuenta, pues el acceso a la misma está restringido al ingreso y uso de su Clave de Seguridad, de conocimiento exclusivo del Usuario. \n\nEl Usuario se compromete a notificar a TOOLBOY en forma inmediata y por medio idóneo y fehaciente, cualquier uso no autorizado de su Cuenta, así como el ingreso por terceros no autorizados a la misma. Se aclara que está prohibida la venta, cesión o transferencia de la Cuenta  bajo ningún título.\n\n03 - MODIFICACIONES DEL ACUERDO.\n\nTOOLBOY podrá modificar los Términos y Condiciones Generales en cualquier momento haciendo públicos en el Sitio los términos modificados. \n\nTodos los términos modificados entrarán en vigor a los 10 (diez) días de su publicación. Dichas modificaciones serán comunicadas por TOOLBOY a los usuarios que en la Configuración de su Cuenta de TOOLBOY hayan indicado que desean recibir notificaciones de los cambios en estos Términos y Condiciones. \n\nEl uso del sitio y/o sus servicios implica la aceptación de estos Términos y Condiciones generales de uso.\n\n04 - PRIVACIDAD DE LA INFORMACIÓN.\n\nPara utilizar los Servicios ofrecidos por TOOLBOY, los Usuarios deberán facilitar determinados datos de carácter personal. Su información personal se procesa y almacena en servidores o medios magnéticos que mantienen altos estándares de seguridad y protección tanto física como tecnológica. \n\nPara mayor información sobre la privacidad de los Datos Personales y casos en los que será revelada, se pueden consultar en nuestro aviso de privacidad.\n\n\n05 - PROHIBICIONES\n\nLos Usuarios no podrán: \n\nmanipular los precios de los servicios.\nLa utilización de diversas cuentas asociadas a sus datos particulares\n\nEste tipo de actividades será investigado por TOOLBOY y el infractor podrá ser sancionado con la suspensión o cancelación de la cuenta y/o de cualquier otra forma que estime pertinente, sin perjuicio de las acciones legales a que pueda dar lugar por la configuración de delitos o contravenciones o los perjuicios civiles que pueda causar a los Usuarios oferentes.\n\n06 - VIOLACIONES DEL SISTEMA O BASES DE DATOS.\n\nNo está permitida ninguna acción o uso de dispositivo, software, u otro medio tendiente a interferir tanto en las actividades y operatoria de TOOLBOY como en las ofertas, descripciones, cuentas o bases de datos. \n\nCualquier intromisión, tentativa o actividad violatoria o contraria a las leyes sobre derecho de propiedad intelectual y/o a las prohibiciones estipuladas en estos TÉRMINOS Y CONDICIONES harán pasible a su responsable de las acciones legales pertinentes, y a las sanciones previstas por este acuerdo, así como lo hará responsable de indemnizar los daños ocasionados.\n\n07 – SANCIONES, SUSPENSIÓN DE OPERACIONES\n\nSin perjuicio de otras medidas, TOOLBOY podrá advertir, suspender en forma temporal o inhabilitar definitivamente la Cuenta de un Usuario, aplicar una sanción, iniciar las acciones que estime pertinentes y/o suspender la prestación de sus Servicios si:\n\nSe quebrantara alguna ley, o cualquiera de las estipulaciones de los Términos y Condiciones Generales y demás políticas.\n Si incumpliera sus compromisos como Usuario.\nSi se incurriera a criterio de TOOLBOY en conductas o actos dolosos o fraudulentos.\nNo pudiera verificarse la identidad del Usuario o cualquier información proporcionada por el mismo fuere errónea.\n\n08 - ALCANCE DE LOS SERVICIOS DE TOOLBOY.\n\nEste acuerdo no crea ningún contrato de sociedad, de mandato, de franquicia, o relación laboral entre TOOLBOY y el Usuario. \n\n09 - FALLAS EN EL SISTEMA.\n\nTOOLBOY no se responsabiliza por cualquier daño, perjuicio o pérdida al Usuario causados por fallas en el sistema, en el servidor o en Internet. \nLos Usuarios NO podrán imputarle responsabilidad alguna ni exigir pago por lucro cesante, en virtud de perjuicios resultantes de dificultades técnicas o fallas en los sistemas o en Internet. \n\nEl sistema puede eventualmente no estar disponible debido a dificultades técnicas o fallas de Internet, o por cualquier otra circunstancia ajena a TOOLBOY; en tales casos se procurará restablecerlo con la mayor celeridad posible sin que por ello pueda imputársele algún tipo de responsabilidad. TOOLBOY no será responsable por ningún error u omisión contenidos en su sitio web.\n\n10 - TARIFAS FACTURACIÓN.\n\nLa inscripción en TOOLBOY estará sujetado a la cotización y aceptación del costo de los paquetes empleados en el portal web https://www.toolboy.mx TOOLBOY se reserva el derecho de modificar, cambiar, agregar, o eliminar las tarifas vigentes, en cualquier momento, lo cual será notificado a los Usuarios, en la forma establecida en la Cláusula 3. \n\nSin embargo, TOOLBOY podrá modificar temporalmente la Política de Tarifas y las tarifas por sus servicios por razón de promociones, siendo efectivas estas modificaciones cuando se haga pública la promoción o se realice el anuncio.\n\nEn caso de haberse facturado cargos que no hubiesen correspondido, el Usuario deberá comunicarse con nuestro equipo de Atención al Cliente para resolver dicha cuestión.\n\n11 - PROPIEDAD INTELECTUAL ENLACES.\n\nLos contenidos de las pantallas relativas a los servicios de TOOLBOY como así también los programas, bases de datos, redes, archivos que permiten al Usuario acceder y usar su Cuenta, son de propiedad de TOOLBOY y están protegidas por las leyes y los tratados internacionales de derecho de autor, marcas, patentes, modelos y diseños industriales. El uso indebido y la reproducción total o parcial de dichos contenidos quedan prohibidos, salvo autorización expresa y por escrito de TOOLBOY.\n\nEl Sitio puede contener enlaces a otros sitios web lo cual no indica que sean propiedad u operados por TOOLBOY. En virtud que TOOLBOY no tiene control sobre tales sitios, NO será responsable por los contenidos, materiales, acciones y/o servicios prestados por los mismos, ni por daños o pérdidas ocasionadas por la utilización de los mismos, sean causadas directa o indirectamente. La presencia de enlaces a otros sitios web no implica una sociedad, relación, aprobación, respaldo de TOOLBOY a dichos sitios y sus contenidos.\n\n12 – INDEMNIDAD.\n\nEl Usuario mantendrá indemne a TOOLBOY, así como a sus filiales, empresas controladas y/o controlantes, funcionarios, directivos, sucesores, administradores, representantes y empleados, por cualquier reclamo iniciado por otros Usuarios, terceros o por cualquier Organismo, relacionado con sus actividades en el Sitio, el cumplimiento y/o el incumplimiento de los Términos y Condiciones Generales o demás Políticas, así como respecto de cualquier violación de leyes o derechos de terceros.\n\n13 – ANEXOS.\n\nForman parte integral e inseparable de los Términos y Condiciones Generales, los siguientes documentos y/o secciones de TOOLBOY incorporados por referencia, donde se detallan políticas y/o Términos y Condiciones de diferentes servicios ofrecidos en el sitio. Los mismos se podrán consultar dentro del sitio mediante el enlace provisto o accediendo directamente a las páginas correspondientes:\n\nAVISO DE PRIVACIDAD\nTÉRMINOS Y CONDICIONES\nPOLITICAS DE DEVOLUCIÓN\n\n14 - JURISDICCIÓN Y LEY APLICABLE.\n\nEste acuerdo estará regido en todos sus puntos por las leyes vigentes en la República Mexicana, en particular respecto de mensajes de datos, contratación electrónica y comercio electrónico se regirá por lo dispuesto por la legislación federal respectiva.\n\nCualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a las leyes aplicables y a los Tribunales competentes.\n\nPara la interpretación, cumplimiento y ejecución del presente contrato, las partes expresamente se someten a la jurisdicción de los tribunales competentes de la Ciudad de México, renunciando en consecuencia a cualquier fuero que en razón de su domicilio presente o futuro pudiera corresponderles.\n\n15 – DOMICILIO.\n\nSe fija como domicilio de TOOLBOY perteneciente a SE CHISPO S.A. DE C.V. moral responsable directa, el ubicado en AVENIDA PATRIOTISMO NO. 12 INT. 3-301 COLONIA HIPODROMO, DELEGACIÓN CUAHUTEMOC, CIUDAD DE MÉXICO; Si tienes alguna duda sobre los Términos y Condiciones Generales o demás políticas y principios que rigen TOOLBOY consulta nuestra página ayuda. \n";

        // Aviso de privacidad
        public static string AvisoDePrivacidad = "AVISO DE PRIVACIDAD\n\u00a0\nDe acuerdo a lo Previsto en la “Ley Federal de Protección de Datos Personales en Posesión de Terceros”, declara SE CHISPO S.A. DE C.V., ser una empresa legalmente constituida de conformidad con las leyes mexicanas, con domicilio en AVENIDA PATRIOTISMO NO. 12 INT. 3-301, C.P. 06100, COLONIA HIPODROMO, DELEGACIÓN CUAHUTEMOC CIUDAD DE MÉXICO y como RESPONSABLE DIRECTA de TOOLBOY la cual es una marca registrada ante el Instituto Mexicano de la Propiedad Industrial, la cual es propiedad de SE CHISPO S.A. DE C.V. del tratamiento de sus datos personales, hace de su conocimiento que la información de nuestros\u00a0 clientes es tratada de forma estrictamente confidencial por lo que al proporcionar sus datos personales, tales como:\n\u00a0\n\u00a0\nNombre completo\nDirección\nRegistro Federal de Contribuyentes\nTeléfonos de hogar, oficina y móviles\nCorreo electrónico\n\u00a0\n\u00a0\nEstos serán utilizados única y exclusivamente para los siguientes fines:\n\u00a0\nRecolección de información científica\nCampañas de publicidad\nInformación y prestación de servicios\nActualización de la base de datos\nCualquier finalidad análoga o compatible con las anteriores\n\u00a0\n\u00a0\nEn el caso de datos sensibles, tales como:\n\u00a0\nDatos financieros (ingresos, estados de cuenta, y demás relacionados)\nDatos patrimoniales (bienes materiales, Inmuebles, y demás relacionados)\n\u00a0\n\u00a0\nEstos serán utilizados única y exclusivamente para los siguientes fines:\n\u00a0\nInvestigación y/u obtención de créditos ante las instituciones financieras\nCualquier finalidad análoga o compatible con la anterior\n\u00a0\nPara prevenir el acceso no autorizado a sus datos personales y con el fin de asegurar que la información sea utilizada para los fines establecidos en este aviso de privacidad, hemos establecido diversos procedimientos con la finalidad de evitar el uso o divulgación no autorizados de sus datos, permitiéndonos tratarlos debidamente.\n\u00a0\n\nAsimismo, le informamos que sus datos personales pueden ser transmitidos para ser tratados por personas distintas a esta empresa.\n\u00a0\n\nTodos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento los derechos (ARCO) de acceder, rectificar, cancelar u oponerse al tratamiento que le damos a sus datos personales; derecho que podrá hacer valer a través del Área de Privacidad encargada de la seguridad de datos personales en el Teléfono +52 (55) 4336-7483, o por medio de su correo electrónico aprivacidad.homieapp@gmail.com este será exclusivo para estos asuntos\n\u00a0\nA través de estos canales usted podrá actualizar sus datos y especificar el medio por el cual desea recibir información, ya que en caso de no contar con esta especificación de su parte SE CHISPO S.A. DE C.V. establecerá libremente el canal que considere pertinente para enviarle información.\n\u00a0\nEste aviso de privacidad podrá ser modificado por SE CHISPO S.A. DE C.V., dichas modificaciones serán oportunamente informadas a través de correo electrónico, teléfono, o cualquier otro medio de comunicación que éste determine para tal efecto.\n\nEl presente Aviso de Privacidad, así como sus modificaciones, estarán a su entera disposición en la página de internet https://www.toolboy.mx\n \n\u00a0\n\u00a0\n\u00a0\nA T E N T A M E N T E\n\n\n\n\nSE CHISPO S.A. DE C.V.";

        // Endpoint SMS
        public static string SenderSMS = "http://95.216.37.253:3000/";
        public static string MessageSMS = "Su contraseña es: ";

        public static string GooglePlacesApiKey
        {
            get { return AppSettings.GetValueOrDefault("GooglePlacesApiKey", ""); }
            set { AppSettings.AddOrUpdateValue("GooglePlacesApiKey", value); }
        }

        public static DateTime FechaExpiracionToken
        {
            get { return AppSettings.GetValueOrDefault("FechaExpiracionToken", DateTime.Now); }
            set { AppSettings.AddOrUpdateValue("FechaExpiracionToken", value); }
        }

        public static bool Walkthrough
        {
            get { return AppSettings.GetValueOrDefault("Walkthrough", true); }
            set { AppSettings.AddOrUpdateValue("Walkthrough", value); }
        }

        public static int Notificaciones
        {
            get { return AppSettings.GetValueOrDefault("Notificaciones", 0); }
            set { AppSettings.AddOrUpdateValue("Notificaciones", value); }
        }

        public static string Culture
        {
            get { return AppSettings.GetValueOrDefault("Culture", "es"); }
            set { AppSettings.AddOrUpdateValue("Culture", value); }
        }

        public static bool MetodoPago
        {
            get { return AppSettings.GetValueOrDefault("MetodoPago", false); }
            set { AppSettings.AddOrUpdateValue("MetodoPago", value); }
        }

        public static bool Registrado
        {
            get { return AppSettings.GetValueOrDefault("Registrado", false); }
            set { AppSettings.AddOrUpdateValue("Registrado", value); }
        }

        public static bool Confirmado
        {
            get { return AppSettings.GetValueOrDefault("Confirmado", false); }
            set { AppSettings.AddOrUpdateValue("Confirmado", value); }
        }

        public static string Token
        {
            get { return AppSettings.GetValueOrDefault("Token", ""); }
            set { AppSettings.AddOrUpdateValue("Token", value); }
        }

        public static string User
        {
            get { return AppSettings.GetValueOrDefault("User", ""); }
            set { AppSettings.AddOrUpdateValue("User", value); }
        }

        public static string Password
        {
            get { return AppSettings.GetValueOrDefault("Password", ""); }
            set { AppSettings.AddOrUpdateValue("Password", value); }
        }

        public static int IdDireccion
        {
            get { return AppSettings.GetValueOrDefault("IdDireccion", 0); }
            set { AppSettings.AddOrUpdateValue("IdDireccion", value); }
        }

        public static string Direccion
        {
            get { return AppSettings.GetValueOrDefault("Direccion", ""); }
            set { AppSettings.AddOrUpdateValue("Direccion", value); }
        }

        public static double Latitud
        {
            get { return AppSettings.GetValueOrDefault("Latitud", 0.0); }
            set { AppSettings.AddOrUpdateValue("Latitud", value); }
        }

        public static double Longitud
        {
            get { return AppSettings.GetValueOrDefault("Longitud", 0.0); }
            set { AppSettings.AddOrUpdateValue("Longitud", value); }
        }

        public static bool Principal
        {
            get { return AppSettings.GetValueOrDefault("Principal", false); }
            set { AppSettings.AddOrUpdateValue("Principal", value); }
        }

        public static bool Aprobado
        {
            get { return AppSettings.GetValueOrDefault("Aprobado", false); }
            set { AppSettings.AddOrUpdateValue("Aprobado", value); }
        }

        public static string Marca
        {
            get { return AppSettings.GetValueOrDefault("Marca", ""); }
            set { AppSettings.AddOrUpdateValue("Marca", value); }
        }

        public static string Modelo
        {
            get { return AppSettings.GetValueOrDefault("Modelo", ""); }
            set { AppSettings.AddOrUpdateValue("Modelo", value); }
        }

        public static string Factura
        {
            get { return AppSettings.GetValueOrDefault("Factura", ""); }
            set { AppSettings.AddOrUpdateValue("Factura", value); }
        }

        public static string NumeroSerie
        {
            get { return AppSettings.GetValueOrDefault("NumeroSerie", ""); }
            set { AppSettings.AddOrUpdateValue("NumeroSerie", value); }
        }

        public static string Descripcion
        {
            get { return AppSettings.GetValueOrDefault("Descripcion", ""); }
            set { AppSettings.AddOrUpdateValue("Descripcion", value); }
        }

        public static ImageSource FotoTrabajo1
        {
            get { return AppSettings.GetValueOrDefault("FotoTrabajo1", ""); }
            set { AppSettings.AddOrUpdateValue("FotoTrabajo1", value); }
        }

        public static ImageSource FotoTrabajo2
        {
            get { return AppSettings.GetValueOrDefault("FotoTrabajo2", ""); }
            set { AppSettings.AddOrUpdateValue("FotoTrabajo2", value); }
        }


        #region Device

        public static string RegistrationId
        {
            get { return AppSettings.GetValueOrDefault("RegistrationId", ""); }
            set { AppSettings.AddOrUpdateValue("RegistrationId", value); }
        }

        public static string DeviceId
        {
            get { return AppSettings.GetValueOrDefault("DeviceId", ""); }
            set { AppSettings.AddOrUpdateValue("DeviceId", value); }
        }

        public static int Platform
        {
            get { return AppSettings.GetValueOrDefault("Platform", 1); }
            set { AppSettings.AddOrUpdateValue("Platform", value); }
        }

        #endregion

        #region Chat

        /// <summary>
        /// Lo vamos a utilizar para identificar el último view que se está utilizando debido a que en iOS no hace dispose de las Page
        /// y si utilizamos SignalR se duplican los mensajes.
        /// </summary>
        public static string ChatPageId
        {
            get { return AppSettings.GetValueOrDefault("ChatPageId", string.Empty); }
            set { AppSettings.AddOrUpdateValue("ChatPageId", value); }
        }

        public static string BuscandoProveedorId
        {
            get { return AppSettings.GetValueOrDefault("BuscandoProveedorId", string.Empty); }
            set { AppSettings.AddOrUpdateValue("BuscandoProveedorId", value); }
        }

        public static List<ServicioChatMin> ChatList
        {
            get
            {
                string value = AppSettings.GetValueOrDefault<string>("ChatList", string.Empty);
                List<ServicioChatMin> myList;
                if (string.IsNullOrEmpty(value))
                    myList = new List<ServicioChatMin>();
                else
                    myList = JsonConvert.DeserializeObject<List<ServicioChatMin>>(value);
                return myList;
            }
            set
            {
                string listValue = JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue<string>("ChatList", listValue);
            }
        }

        #endregion

        #region Notificaciones

        public static string NotificationParameters
        {
            get { return AppSettings.GetValueOrDefault("NotificationParameters", ""); }
            set { AppSettings.AddOrUpdateValue("NotificationParameters", value); }
        }

        public static string NotificationPageToOpen
        {
            get { return AppSettings.GetValueOrDefault("NotificationPageToOpen", ""); }
            set { AppSettings.AddOrUpdateValue("NotificationPageToOpen", value); }
        }

        #endregion
    }

    public class SettingsChat
    {
        public void AddElement(ServicioChat chat)
        {
            // Se obtienen los chats y se revisa que no exista.
            var lista = Settings.ChatList;

            if (!lista.Any(x => x.IdServicioChat == chat.IdServicioChat))
            {
                lista.Add(new ServicioChatMin
                {
                    IdServicioChat = chat.IdServicioChat,
                    IdServicio = chat.IdServicio,
                    Chat = JsonConvert.SerializeObject(chat)
                });
            }

            Settings.ChatList = lista;
        }

        public static void RemoveElement(long idServicio)
        {
            // Se obtienen los chats y se revisa que no exista.
            var lista = Settings.ChatList;
            lista.RemoveAll(x => x.IdServicio == idServicio);
            Settings.ChatList = lista;
        }

        public List<ServicioChat> GetByIdServicioChat(long idServicio)
        {
            // Se obtienen los chats y se revisa que no exista.
            var lista = Settings.ChatList.Where(x => x.IdServicio == idServicio);
            var listaCompleta = new List<ServicioChat>();
            foreach (var servicioChat in lista)
            {
                listaCompleta.Add(JsonConvert.DeserializeObject<ServicioChat>(servicioChat.Chat));
            }

            return listaCompleta;
        }

       

    }
}
