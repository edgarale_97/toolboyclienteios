﻿using Xamarin.Forms;

namespace HomieApp.Helpers
{
    public class ArchivoDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ArchivoTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ArchivoTemplate;
        }
    }
}
