﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomieApp.Helpers
{
    public static class TaskExtensions
    {
        public static event EventHandler<ForgottenExceptionEventArgs> ForgottenExceptionOccurred;

        public static void Forget(this Task task)
        {
            task.ContinueWith((Action<Task>)(t =>
            {
                // ISSUE: reference to a compiler-generated field
                if (TaskExtensions.ForgottenExceptionOccurred == null)
                    return;
                // ISSUE: reference to a compiler-generated field
                TaskExtensions.ForgottenExceptionOccurred((object)null, new ForgottenExceptionEventArgs(t));
            }), TaskContinuationOptions.OnlyOnFaulted);
        }
    }
    public class ForgottenExceptionEventArgs : EventArgs
    {
        public Task FaultedTask { get; }

        public ForgottenExceptionEventArgs(Task task)
        {
            this.FaultedTask = task;
        }
    }
}
