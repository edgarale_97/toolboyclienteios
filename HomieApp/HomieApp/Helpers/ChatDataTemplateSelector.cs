﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Helpers
{
    public class ChatDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate LeftChatTemplate { get; set; }
        public DataTemplate RightChatTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((ServicioChat)item).TipoEnvio == 2 ? LeftChatTemplate : RightChatTemplate;
        }
    }
}