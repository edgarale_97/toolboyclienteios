﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HomieApp.Helpers
{
    public class EntryProperties
    {
        public static readonly BindableProperty PlaceholderColorProperty = BindableProperty.CreateAttached("PlaceholderColor", typeof(Color), typeof(EntryProperties), Color.Default);
        public static readonly BindableProperty BorderStyleProperty = BindableProperty.CreateAttached("BorderStyle", typeof(BorderStyle), typeof(EntryProperties), BorderStyle.Default);
        public static readonly BindableProperty BorderWidthProperty = BindableProperty.CreateAttached("BorderWidth", typeof(float), typeof(EntryProperties), 1f);
        public static readonly BindableProperty BorderCornerRadiusProperty = BindableProperty.CreateAttached("BorderCornerRadius", typeof(float), typeof(EntryProperties), 10f);
        public static readonly BindableProperty BorderColorProperty = BindableProperty.CreateAttached("BorderColor", typeof(Color), typeof(EntryProperties), Color.White);

        public static Color GetPlaceholderColor(BindableObject bo)
        {
            return (Color)bo.GetValue(EntryProperties.PlaceholderColorProperty);
        }

        public static void SetPlaceholderColor(BindableObject bo, Color value)
        {
            bo.SetValue(EntryProperties.PlaceholderColorProperty, value);
        }

        public static void OnPlaceholderColorChanged(BindableObject bo, Color oldValue, Color newValue)
        {
        }

        public static BorderStyle GetBorderStyle(BindableObject bo)
        {
            return (BorderStyle)bo.GetValue(EntryProperties.BorderStyleProperty);
        }

        public static void SetBorderStyle(BindableObject bo, BorderStyle value)
        {
            bo.SetValue(EntryProperties.BorderStyleProperty, value);
        }

        public static void OnBorderStyleChanged(BindableObject bo, BorderStyle oldValue, BorderStyle newValue)
        {
        }

        public static float GetBorderWidth(BindableObject bo)
        {
            return (float)bo.GetValue(EntryProperties.BorderWidthProperty);
        }

        public static void SetBorderWidth(BindableObject bo, float value)
        {
            bo.SetValue(EntryProperties.BorderWidthProperty, value);
        }

        public static void OnBorderWidthChanged(BindableObject bo, float oldValue, float newValue)
        {
        }

        public static float GetBorderCornerRadius(BindableObject bo)
        {
            return (float)bo.GetValue(EntryProperties.BorderCornerRadiusProperty);
        }

        public static void SetBorderCornerRadius(BindableObject bo, float value)
        {
            bo.SetValue(EntryProperties.BorderCornerRadiusProperty, value);
        }

        public static void OnBorderCornerRadiusChanged(BindableObject bo, float oldValue, float newValue)
        {
        }

        public static Color GetBorderColor(BindableObject bo)
        {
            return (Color)bo.GetValue(EntryProperties.BorderColorProperty);
        }

        public static void SetBorderColor(BindableObject bo, Color value)
        {
            bo.SetValue(EntryProperties.BorderColorProperty, value);
        }

        public static void OnBorderColorChanged(BindableObject bo, Color oldValue, Color newValue)
        {
        }
    }
}
