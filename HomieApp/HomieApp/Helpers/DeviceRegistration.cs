﻿using Xamarin.Forms;

public class DeviceRegistration
{
    public string Platform { get; set; }
    public string DeviceToken { get; set; }

    public DeviceRegistration(string deviceToken)
    {
        if(Device.RuntimePlatform == Device.Android)
        {
            Platform = "gcm";
        }
        else
        {
            Platform = "apns";
        }
        DeviceToken = deviceToken;
    }
}