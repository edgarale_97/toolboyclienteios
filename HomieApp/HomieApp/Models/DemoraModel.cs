﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Models
{
    class DemoraModel
    {
        private const string ApiControllerName = "DemoraApi";

        public async Task<RootObject<int>> GetAsync(long idServicio)
        {
            var model = new ModelBase<int>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                { nameof(idServicio), idServicio }
            };
            return await model.GetAsync(parameters);
        }
    }
}

