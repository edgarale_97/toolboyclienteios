﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    public class ChatModel
    {
        private const string ApiControllerName = "ServicioChat";

        public async Task<RootObject<ServicioChat>> PostAsync(long idServicio, string mensaje, long idProveedor, byte[] imagenBytes, byte[] videoBytes, bool notificar = true)
        {
            var modelBase = new ModelBase<ServicioChat>(ApiControllerName);
            var paramters = new Dictionary<string, object>
            {
                {nameof(notificar), notificar }
            };

            return await modelBase.PostAsync(new
            {
                Mensaje = mensaje,
                IdServicio = idServicio,
                IdProveedor = idProveedor,
                ImagenBytes = imagenBytes,
                VideoBytes = videoBytes,
                TipoEnvio = 1
            }, paramters);
        }

        public async Task<RootObject<List<ServicioChat>>> GetAsync(long idServicio, long lastIdServicioChat, long idProveedor)
        {
            var modelBase = new ModelBase<List<ServicioChat>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {"appName", "homie_cliente"},
                {nameof(idServicio), idServicio},
                {nameof(lastIdServicioChat), lastIdServicioChat},
                {nameof(idProveedor), idProveedor}
            };

            return await modelBase.GetAsync(parameters);
        }
    }
}
