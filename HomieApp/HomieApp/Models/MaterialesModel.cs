﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class MaterialesModel
    {
        private const string ApiControllerName = "MaterialesProveedorApi";

        // Obtiene la lista de materiales de un servicio.
        public async Task<RootObject<List<ServicioMateriales>>> GetAsync(long idServicio)
        {
            var modelBase = new ModelBase<List<ServicioMateriales>>(ApiControllerName);
            var param = new Dictionary<string, object>
            {
                {nameof(idServicio),idServicio }
            };
            return await modelBase.GetAsync(param);
        }
    }
}
