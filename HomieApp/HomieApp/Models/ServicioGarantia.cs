﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Models
{
    class ServicioGarantia
    {
        private const string ApiControllerName = "GarantiaApi";

        public async Task<RootObject<long>> PostAsync(Entities.ServicioGarantia servicioGarantia)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);            
            return await modelBase.PostSolicitarGarantia(servicioGarantia);
        }
    }
}

