﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class ServiciosModel
    {
        private const string ApiControllerName = "ServiciosApi";

        public async Task<RootObject<List<Servicio>>> GetListAsync(int page = 0, int pageSize = 30, int estatus = 0)
        {
            var modelBase = new ModelBase<List<Servicio>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(page), page },
                {nameof(pageSize), pageSize },
                {nameof(estatus), estatus }
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<bool>> DeleteAsync(long idServicio)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            var parameters = new Dictionary<string, object>();
            parameters.Add(nameof(idServicio), idServicio);
            return await modelBase.DeleteAsync(parameters);
        }

        public async Task<RootObject<long>> PostAsync(ServicioCreate servicio)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostSolicitarServicio(servicio);
        }

        public async Task<RootObject<long>> PostAsyncGartantia(Servicio  servicio)
        {
            var modelBase = new ModelBase<long>("");
            return await modelBase.PostSolicitarGarantia(servicio);
        }

        public async Task<RootObject<Servicio>> GetByIdAsync(long idServicio)
        {
            var modelBase = new ModelBase<Servicio>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };
            return await modelBase.GetAsync(parameters);
        }

        // Cancelar un servicio.
        public async Task<RootObject<int>> PostAsync(long idServicio)
        {
            var modelBase = new ModelBase<int>("CancelarServicioApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio },
                {"isProveedor", false }
            };
            return await modelBase.PostAsync(null,parameters);
        }

        // Terminar un servicio
        //public async Task<RootObject<int>> TerminarAsync(long idServicio)
        //{
        //    var modelBase = new ModelBase<int>("TerminarServicioApi");
        //    var parameters = new Dictionary<string, object>
        //    {
        //        {nameof(idServicio), idServicio }
        //    };
        //    return await modelBase.PostAsync(null, parameters);
        //}

    }
}
