﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Models
{
    class ActivosModel
    {
        private const string ApiControllerName = "ActivosApi";

        public async Task<RootObject<List<Activos>>> GetListAsync(int page = 0, int pageSize = 10)
        {
            var modelBase = new ModelBase<List<Activos>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(page), page },
                {nameof(pageSize), pageSize }
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<List<Categorias>>> GetListActivos(int page = 0, int pageSize = 10, int idCategoriaPadre = 0)
        {
            var modelBase = new ModelBase<List<Categorias>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(page), page },
                {nameof(pageSize), pageSize },
                {nameof(idCategoriaPadre), idCategoriaPadre}
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<long>> PostAsync(Garantias garantias)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostAsync(garantias);
        }

        public async Task<RootObject<Activos>> GetByIdAsync(long idServicio)
        {
            var modelBase = new ModelBase<Activos>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<Categorias>> GetListCategoria(long idCategoria)
        {
            var modelBase = new ModelBase<Categorias>("DetallesActivosApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idCategoria), idCategoria }
            };
            return await modelBase.GetAsync(parameters);
        }

    }
}

