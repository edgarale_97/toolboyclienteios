﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class DetalleProveedorModel
    {
        private const string ApiControllerName = "ProveedorApi";
        
        public async Task<RootObject<DetalleProveedor>> GetByIdAsync(long idProveedor)
        {
            var modelBase = new ModelBase<DetalleProveedor>(ApiControllerName);

            var parameters = new Dictionary<string, object>
            {
                {nameof(idProveedor), idProveedor }
            };

            return await modelBase.GetAsync(parameters);
        }
    }
}
