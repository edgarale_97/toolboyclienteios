﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Models
{
    public class CategoriasModel
    {
        private const string ApiControllerName = "CategoriasApi";

        public async Task<RootObject<List<Categorias>>> GetListAsync(int page = 0, int pageSize = 100, int idCategoriaPadre = 0)
        {
            var modelBase = new ModelBase<List<Categorias>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(page), page},
                {nameof(pageSize), pageSize},
                {nameof(idCategoriaPadre), idCategoriaPadre}
            };

            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<Categorias>> GetCategoriaAsync(int idCategoria, int page = 0, int pageSize = 100, int idCategoriaPadre = 0)
        {
            var modelBase = new ModelBase<Categorias>("CategoriaTarifaApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idCategoria), idCategoria },
                {nameof(page), page},
                {nameof(pageSize), pageSize},
                {nameof(idCategoriaPadre), idCategoriaPadre}
            };

            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<List<Categorias>>> GetList(int page = 0, int pageSize = 100, int idCategoriaPadre = 0)
        {
            var senderBase = new SenderModel<List<Categorias>>("DireccionApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(page), page},
                {nameof(pageSize), pageSize},
                {nameof(idCategoriaPadre), idCategoriaPadre}
            };

            return await senderBase.GetAsync(parameters);
        }

    }
}