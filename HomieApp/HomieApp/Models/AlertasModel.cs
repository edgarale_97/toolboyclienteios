﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class AlertasModel
    {
        private const string ApiControllerName = "AlertasApi";

        public async Task<RootObject<long>> PostAsync(long idServicio)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };
            return await modelBase.PostAsync(null, parameters);
        }
    }
}
