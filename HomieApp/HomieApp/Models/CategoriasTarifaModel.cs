﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    public class CategoriasTarifaModel
    {
        private const string ApiControllerName = "CategoriasTarifaApi";

        public async Task<RootObject<List<Categorias>>> GetListAsync(int idCategoriaPadre, long idDireccion)
        {
            var modelBase = new ModelBase<List<Categorias>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idCategoriaPadre), idCategoriaPadre},
                {nameof(idDireccion), idDireccion }
            };

            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<List<Categorias>>> GetSubList(int idCategoriaPadre, long idDireccion)
        {
            var modelBase = new ModelBase<List<Categorias>>("SubCategoriasTarifaApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idCategoriaPadre), idCategoriaPadre},
                {nameof(idDireccion), idDireccion}
            };

            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<List<Categorias>>> GetList(int idCategoriaPadre, long idDireccion)
        {
            var modelBase = new SenderModel<List<Categorias>>("CategoriaTarifaApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idCategoriaPadre), idCategoriaPadre},
                {nameof(idDireccion), idDireccion}
            };

            return await modelBase.GetTarifa(parameters);
        }

    }
}