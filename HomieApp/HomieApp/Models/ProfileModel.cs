﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class ProfileModel
    {
        private const string ApiControllerName = "ClienteProfileApi";

        public async Task<RootObject<long>> PostAsync(ProfileUser user)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostAsync(user);
        }

        public async Task<RootObject<ProfileUser>> GetAsync()
        {
            var modelBase = new ModelBase<ProfileUser>(ApiControllerName);

            var parameters = new Dictionary<string, object>();

            return await modelBase.GetAsync(parameters);
        }
    }
}
