﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class SignUpModel
    {
        private const string ApiControllerName = "SignUp";

        public async Task<RootObject<long>> PostAsync(SignUpUser user)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostAsync(user);
        }

        public async Task<RootObject<List<Categorias>>> GetListAsync()
        {
            var modelBase = new ModelBase<List<Categorias>>(ApiControllerName);
            var parameters = new Dictionary<string, object>();

            return await modelBase.GetAsync(parameters);
        }
    }
}
