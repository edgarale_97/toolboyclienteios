﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    public class CalificacionesModel
    {
        private const string ApiControllerName = "CalificarProveedorApi";

        public async Task<RootObject<CalificacionData>> GetAsync()
        {
            var modelBase = new ModelBase<CalificacionData>(ApiControllerName);
            return await modelBase.GetAsync(new Dictionary<string, object>());
        }

        public async Task<RootObject<bool>> PostAsync(Calificacion calificacion)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            return await modelBase.PostAsync(calificacion);
        }

        public async Task<RootObject<CalificacionData>> Get()
        {
            var modelBase = new ModelBase<CalificacionData>("CalificacionProveedorApi");
            return await modelBase.GetAsync(new Dictionary<string, object>());
        }

        public async Task<RootObject<bool>> Post(Calificacion calificacion)
        {
            var modelBase = new ModelBase<bool>("CalificacionProveedorApi");
            return await modelBase.PostAsync(calificacion);
        }
    }
}
