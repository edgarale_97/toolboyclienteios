﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    public class CotizacionClienteModel
    {
        private const string ApiControllerName = "CotizacionClienteApi";

        public async Task<RootObject<int>> PostAsync(long idServicio, bool aceptar, bool materialesExtra = false)
        {
            var modelBase = new ModelBase<int>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio },
                {nameof(aceptar), aceptar },
                {nameof(materialesExtra),materialesExtra }
            };
            return await modelBase.PostAsync(null, parameters);
        }
    }
}
