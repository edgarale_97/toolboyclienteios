﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    public class TarjetasModel
    {
        public string ApiControllerName = "TarjetasClienteApi";

        public async Task<RootObject<List<Tarjeta>>> GetListAsync()
        {
            var modelBase = new ModelBase<List<Tarjeta>>(ApiControllerName);

            var parameters = new Dictionary<string, object>();

            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<long>> PostTarjeta(Tarjeta tarjeta, string deviceSessionId)
        {
            var modelToken = new OpenpayTokenModel();
            var respuestaToken = await modelToken.GetTokenCard(tarjeta);

            if(respuestaToken == null || respuestaToken.Error)
            {
                return new RootObject<long>
                {
                    Error = true,
                    ErrorDescription = $"Hubo un problema al registrar la tarjeta. \"{respuestaToken?.description}\""
                };
            }

            var modelBase = new ModelBase<long>(ApiControllerName);
            var type = GetTypeBrand(respuestaToken.card.brand);
            return await modelBase.PostAsync(new
            {
                tarjeta.IdTarjeta,
                tarjeta.CardNumber,
                tarjeta.Csc,
                tarjeta.ExpiresMonth,
                tarjeta.ExpireYear,
                type,
                Token = respuestaToken.id,
                tarjeta.FirstName,
                DeviceSessionId = deviceSessionId
            });
        }

        public int GetTypeBrand(string type)
        {
            if(type == "visa")
            {
                return 50;
            }
            if(type == "mastercard")
            {
                return 51;
            }
            if(type == "amex")
            {
                return 52;
            }
            return 0;
        }

        public async Task<RootObject<long>> PostAsync(Tarjeta tarjeta, string deviceSessionId)
        {
            // Obtenemos el nombre del cliente
            var modelUsuario = new ProfileModel();
            var respuestaUsuario = await modelUsuario.GetAsync();

            if (respuestaUsuario == null || respuestaUsuario.Error)
            {
                return new RootObject<long>
                {
                    Error = true,
                    ErrorDescription = "No se pudo obtener la información del perfil, intentelo mas tarde"
                };
            }

            // 2.- Tokenización de la tarjeta
            var modelToken = new OpenpayTokenModel();
            tarjeta.FirstName = respuestaUsuario.Result.Nombres;
            tarjeta.LastName = $"{respuestaUsuario.Result.ApellidoPaterno} {respuestaUsuario.Result.ApellidoMaterno}".Trim();
            var respuestaToken = await modelToken.GetToken(tarjeta);

            if (respuestaToken == null || respuestaToken.Error)
            {
                return new RootObject<long>
                {
                    Error = true,
                    ErrorDescription = $"Hubo un problema al registrar la tarjeta. \"{respuestaToken?.description}\""
                };
            }

            var modelBase = new ModelBase<long>(ApiControllerName);

            return await modelBase.PostAsync(new
            {
                tarjeta.IdTarjeta,
                tarjeta.CardNumber,
                tarjeta.Csc,
                tarjeta.ExpiresMonth,
                tarjeta.ExpireYear,
                tarjeta.PaymentType,
                Token = respuestaToken.id,
                DeviceSessionId = deviceSessionId
            });
        }

        public async Task<RootObject<bool>> DeleteAsync(long idTarjeta)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);

            var parameters = new Dictionary<string, object>();
            parameters.Add(nameof(idTarjeta), idTarjeta);

            return await modelBase.DeleteAsync(parameters);
        }

        public async Task<RootObject<bool>> PutAsync(long idTarjeta, bool principal)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);

            var parameters = new Dictionary<string, object>();
            parameters.Add(nameof(idTarjeta), idTarjeta);
            parameters.Add(nameof(principal), principal);

            return await modelBase.PutAsync(null, parameters);
        }
    }
}
