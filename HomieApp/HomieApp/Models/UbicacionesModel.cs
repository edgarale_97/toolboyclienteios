﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class UbicacionesModel
    {
        private const string ApiControllerName = "UbicacionesApi";

        public async Task<RootObject<Ubicaciones>> GetAsync(long idServicio)
        {
            var modelBase = new ModelBase<Ubicaciones>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };
            return await modelBase.GetAsync(parameters);
        }
    }
}
