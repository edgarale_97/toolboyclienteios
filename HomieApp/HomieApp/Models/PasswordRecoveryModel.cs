﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Models
{
    class PasswordRecoverModel
    {
        private const string ApiControllerName = "ResetPasswordApi";

        public async Task<RootObject<bool>> PostAsync(string email, string phone)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            var parameters = new Dictionary<string, object>();
            parameters.Add("email", email);
            parameters.Add("phone", phone);

            return await modelBase.PostAsync(parameters, null);
        }

    }
}

