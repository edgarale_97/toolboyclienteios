﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class ChangePasswordModel
    {
        private const string ApiControllerName = "ChangePasswordApi";

        public async Task<RootObject<bool>> PostAsync(string password)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            var parameters = new Dictionary<string, object>();
            parameters.Add("password", password);
            
            return await modelBase.PostAsync(null, parameters);
        }
    }
}
