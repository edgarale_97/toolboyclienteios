﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;

namespace HomieApp.Models
{
    class DireccionesModel
    {
        private const string ApiControllerName = "DireccionesApi";

        public async Task<RootObject<long>> PostAsync(Direcciones direccion)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostAsync(direccion);
        }

        public async Task<RootObject<List<Direcciones>>> UpdateAsync(Direcciones direcciones)
        {
            var modelBase = new ModelBase<List<Direcciones>>(ApiControllerName);
            var parameters = new Dictionary<string, object>();
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<List<Direcciones>>> GetListAsync()
        {
            var modelBase = new ModelBase<List<Direcciones>>(ApiControllerName);

            var parameters = new Dictionary<string, object>();

            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<bool>> DeleteAsync(long idDireccion)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);

            var parameters = new Dictionary<string, object>();
            parameters.Add(nameof(idDireccion), idDireccion);

            return await modelBase.DeleteAsync(parameters);
        }

        public async Task<RootObject<List<Direcciones>>> GetList()
        {
            var modelBase = new SenderModel<List<Direcciones>>(ApiControllerName);

            var parameters = new Dictionary<string, object>();

            return await modelBase.GetAsync(parameters);
        }

    }
}
