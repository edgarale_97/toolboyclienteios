﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.Helpers;
using Newtonsoft.Json;

namespace HomieApp.Models
{
    public class OpenpayTokenModel
    {

        public async Task<OpenPayData> GetTokenCard(Tarjeta tarjeta)
        {
            string number = tarjeta.CardNumber.Replace("-", "");
            string month = tarjeta.ExpiresMonth.ToString().PadLeft(2, '0');
            string year = tarjeta.ExpireYear.ToString();
            var res = new OpenPayData();
            var client = new HttpClient
            {
                MaxResponseContentBufferSize = 256000
            };
            var openPayCard = new OpenPayCard
            {
                card_number = number.Trim(),
                expiration_month = month,
                expiration_year = year,
                cvv2 = tarjeta.Csc,
                holder_name = tarjeta.FirstName
            };

            string authInfo = Settings.OpenPayPublicKey + ":";
            authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
            var json = JsonConvert.SerializeObject(openPayCard, Formatting.Indented, new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            });
            try
            {
                var uri = $"{Settings.OpenPayApi}{Settings.OpenPayId}/tokens";
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(uri, content);
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    res = JsonConvert.DeserializeObject<OpenPayData>(body);
                }
                else
                {
                    res.description = "Error al enviar la información";
                }
            }
            catch (Exception ex)
            {
                res.description = ex.ToString().Contains("ConnectFailure") || ex.ToString().Contains("NameResolutionFailure")
                    ? Messages.ConnectFailure
                    : ex.Message;
            }
            return res;
        }

        public async Task<OpenPayData> GetToken(Tarjeta tarjeta)
        {
            var res = new OpenPayData();
            var client = new HttpClient
            {
                MaxResponseContentBufferSize = 256000
            };

            var openPayCard = new OpenPayCard
            {
                //brand = tarjeta.CardType,
                card_number = tarjeta.CardNumber,
                expiration_month = tarjeta.ExpiresMonth.ToString().PadLeft(2, '0'),
                expiration_year = tarjeta.ExpireYear.ToString().Substring(2, 2),
                cvv2 = tarjeta.Csc,
                holder_name = $"{tarjeta.FirstName} {tarjeta.LastName}".Trim()
            };

            string authInfo = Settings.OpenPayPublicKey + ":";
            authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);

            var json = JsonConvert.SerializeObject(openPayCard, Formatting.Indented, new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            });

            try
            {
                var uri = $"{Settings.OpenPayApi}{Settings.OpenPayId}/tokens";

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    res = JsonConvert.DeserializeObject<OpenPayData>(body);
                }
                else
                {
                    res.description = "Error al enviar la información";
                }
            }
            catch (Exception ex)
            {
                res.description = ex.ToString().Contains("ConnectFailure") || ex.ToString().Contains("NameResolutionFailure")
                    ? Messages.ConnectFailure
                    : ex.Message;
            }
            return res;
        }
    }

    public class OpenPayCard
    {
        public string holder_name { get; set; }
        public string card_number { get; set; }
        public string expiration_year { get; set; }
        public string expiration_month { get; set; }
        public string cvv2 { get; set; }
        public OpenPayAddress address { get; set; }

        public object creation_date { get; set; }
        public string brand { get; set; }
    }

    public class OpenPayAddress
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string line3 { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country_code { get; set; }
    }

    //public class OpenPayToken
    //{
    //    public OpenPayData data { get; set; }
    //    public int status { get; set; }
    //    public string message { get; set; }

    //    // TODO: Obtener los mensajes.
    //    public bool Error => status > 400;
    //}

    public class OpenPayData
    {
        // Success
        public string id { get; set; }
        public OpenPayCard card { get; set; }

        // Error
        public string category { get; set; }
        public string description { get; set; }
        public int http_code { get; set; }
        public int error_code { get; set; }

        // TODO: Obtener los mensajes.
        public bool Error => !string.IsNullOrEmpty(description);
    }
}
