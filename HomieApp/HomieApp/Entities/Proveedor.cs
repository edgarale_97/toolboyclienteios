﻿using System.Collections.Generic;

namespace HomieApp.Entities
{
    public class Proveedor
    {
        public long IdProveedor { get; set; }
        public string Nombre { get; set; }
        public string Foto { get; set; }
    }

    public class DetalleProveedor
    {
        public long IdProveedor { get; set; }
        public string NombreCompleto { get; set; }
        public int Calificacion { get; set; }
        public string TrabajoFoto1 { get; set; }
        public string TrabajoFoto2 { get; set; }
        public string TrabajoFoto3 { get; set; }
        public string Foto { get; set; }
        public string Detalle { get; set; }

        public List<Categorias> Categorias { get; set; }
    }

    public class Catalogo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
