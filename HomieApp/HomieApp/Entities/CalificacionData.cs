﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class CalificacionData
    {
        public string Detalle { get; set; }
        public long IdServicio { get; set; }
        public string Nombre { get; set; }
        public string Foto { get; set; }
        public DateTime Fecha { get; set; }


        public string NoServicioString => "No. de Servicio: " + IdServicio;
    }
}
