﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    class Ubicaciones
    {
        public double LatitudProveedor { get; set; }
        public double LongitudProveedor { get; set; }
        public string NombreProveedor { get; set; }

        public double LatitudDestino { get; set; }
        public double LongitudDestino { get; set; }
        public string DireccionDestino { get; set; }
    }
}
