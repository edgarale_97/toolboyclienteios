﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class Cargo
    {
        public string Descripcion { get; set; }
        public decimal Importe { get; set; }
    }
}
