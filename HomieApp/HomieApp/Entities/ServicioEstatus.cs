﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class ServicioEstatus
    {
        public const int Creado = 13;
        public const int Confirmado = 14;
        public const int PorIniciar = 15;
        public const int Iniciado = 16;
        public const int Terminado = 17;
        public const int Cancelado = 18;
    }

    public class ServicioProveedorEstatus
    {
        public const int Solicitado = 20;
        public const int SolicitudAceptada = 21;
        public const int SolicitudDenegada = 22;
        public const int AceptadoCliente = 23;
        public const int Confirmado = 25;
        public const int Iniciado = 26;
        public const int Terminado = 27;
        public const int Cancelado = 28;
    }
}
