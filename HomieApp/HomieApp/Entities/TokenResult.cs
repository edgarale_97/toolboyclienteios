﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomieApp.Entities
{
    class TokenResult
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(".expires")]
        public DateTime Expires { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("metodoPago")]
        public bool MetodoPago { get; set; }
    }
}
