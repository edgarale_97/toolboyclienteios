﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class TipoServicio
    {
        public const int Urgente = 67;
        public const int Agendado = 68;
        public const int Garantia = 69;
        public const int Activo = 70;
    }

    public class EstatusServicio
    {
        public const int Creado = 13;
        public const int Confirmado = 14;
        public const int PorIniciar = 15;
        public const int Iniciado = 16;
        public const int Terminado = 17;
        public const int Cancelado = 18;
    }
}
