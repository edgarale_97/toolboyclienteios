﻿using System;
using System.Collections.Generic;
using System.Text;
using HomieApp.Helpers;

namespace HomieApp.Entities
{
    public class Messages
    {
        public Messages()
        {
            if (Settings.Culture == "es")
            {
                GetGenericError = "Hubo un problema al obtener la información";

                ConnectFailure = "No se encontró una conexión a internet disponible";
                SignUpError = "Hubo un problema al realizar el registro. Vuelva a intentarlo más tarde";
                ProfileError = "Hubo un problema al guardar la información. Vuelva a intentarlo más tarde";
            }
            else
            {
                GetGenericError = "";

                ConnectFailure = "";
                SignUpError = "";
                ProfileError = "";
            }
        }

        public static string GetGenericError;

        public static string ConnectFailure;
        public static string SignUpError;
        public static string ProfileError;
    }
}
