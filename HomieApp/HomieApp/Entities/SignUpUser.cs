﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class SignUpUser
    {
        public Int64 Id { get; set; }
        public string Correo { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string TelefonoMovil { get; set; }

        public byte[] Foto { get; set; }
    }
}
