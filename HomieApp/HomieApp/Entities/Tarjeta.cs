﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class Tarjeta
    {
        public long IdTarjeta { get; set; }
        public int Country { get; set; }
        public string CardNumber { get; set; }
        public int PaymentType { get; set; }
        public int ExpiresMonth { get; set; }
        public int ExpireYear { get; set; }
        public string Csc { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int City { get; set; }
        public int State { get; set; }
        public int ZipCode { get; set; }
        public long IdUser { get; set; }
        public bool Principal { get; set; }

        public string ExpireText => $"{ExpiresMonth} / {ExpireYear}";

        public string CardType
        {
            get
            {
                if (PaymentType == (int)CreditCardType.Amex)
                {
                    return "American Express";
                }
                else if (PaymentType == (int)CreditCardType.Visa)
                {
                    return "Visa";
                }
                else if (PaymentType == (int)CreditCardType.Mastercard)
                {
                    return "Master Card";
                }

                return string.Empty;
            }
        }

        public string TextoPrincipal => Principal ? "Principal" : string.Empty;
    }
}
