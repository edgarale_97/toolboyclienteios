﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace HomieApp.Entities
{
    public class ProfileUser : INotifyPropertyChanged
    {
        private string _nombres;
        private string _apellidoPaterno;
        private string _apellidoMaterno;

        public string Nombres
        {
            get { return _nombres; }
            set
            {
                _nombres = value;
                NotifyPropertyChanged("Nombre Completo");
            }
        }

        public string ApellidoPaterno
        {
            get { return _apellidoPaterno; }
            set
            {
                _apellidoPaterno = value;
                NotifyPropertyChanged("Nombre Completo");
            }
        }

        public string ApellidoMaterno
        {
            get { return _apellidoMaterno; }
            set
            {
                _apellidoMaterno = value;
                NotifyPropertyChanged("NombreCompleto");
            }
        }

        public string Correo { get; set; }
        public string FotoUri { get; set; }

        public byte[] Foto { get; set; }
        public string TelefonoMovil { get; set; }
        public string TelefonoContacto { get; set; }

        public string NombreCompleto => Nombres + " " + ApellidoPaterno + " " + ApellidoMaterno;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
