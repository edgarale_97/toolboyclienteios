﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    /// <summary>
    /// Indica el nombre de la pantalla que se abrirá en la notificación.
    /// </summary>
    public class NotificacionPage
    {
        public const string Chat = "chat";
        public const string SolicitudServicio = "solicitudServicio";
        public const string CancelacionServicio = "cancelacionServicio";
        public const string ConfirmacionServicio = "confirmacionServicio";

        public const string SeguimientoProveedor = "seguimientoProveedor";

        public const string Servicio = "servicio";
        public const string ServicioTerminado = "servicioTerminado";

        public const string BuscandoProveedor = "buscandoProveedor";
    }

    public class NotificationParameters
    {
        public long IdServicioChat { get; set; }
        public long IdServicio { get; set; }
        public long IdCliente { get; set; }
        public long IdProveedor { get; set; }
    }
}
