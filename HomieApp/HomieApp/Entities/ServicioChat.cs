﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class ServicioChat
    {
        public long IdServicioChat { get; set; }
        public string Mensaje { get; set; }
        public long IdServicio { get; set; }
        public long IdCliente { get; set; }
        public long IdProveedor { get; set; }
        public bool Activo { get; set; }
        public int TipoEnvio { get; set; }
        public DateTime Fecha { get; set; }
        public string Imagen { get; set; }
        public string Video { get; set; }
        public string ChatHabilitado { get; set; }

        public byte[] ImagenBytes { get; set; }
        public byte[] VideoBytes { get; set; }


        public bool IsVisibleDownload => !string.IsNullOrWhiteSpace(Imagen) || !string.IsNullOrWhiteSpace(Video) || Mensaje == "El proveedor te envió una cotización" || Mensaje == "El proveedor agregó materiales extra a la cotización";
    }

    public class ServicioChatMin
    {
        public long IdServicioChat { get; set; }
        public long IdServicio { get; set; }
        public string Chat { get; set; } // Se guardará ServicioChat serializado.
    }
}
