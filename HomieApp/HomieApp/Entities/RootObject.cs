﻿using Newtonsoft.Json;

namespace HomieApp.Entities
{
    public class RootObject<T>
    {
        [JsonProperty("Error")]
        public bool Error { get; set; }

        [JsonProperty("ErrorDescription")]
        public string ErrorDescription { get; set; }

        [JsonProperty("TotalCount")]
        public int TotalCount { get; set; }

        [JsonProperty("TotalPages")]
        public int TotalPages { get; set; }

        [JsonProperty("Result")]
        public T Result { get; set; }
    }
}
