﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HomieApp.Entities
{
    public class CategoriaTarifa : INotifyPropertyChanged
    {
        private bool _seleccionado;
        public int IdCategorias { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Icono { get; set; }

        public decimal Monto { get; set; }

        public bool Seleccionado
        {
            get { return _seleccionado; }
            set
            {
                _seleccionado = value;

                if (_seleccionado)
                {
                    //BackgroundColor = Color.FromHex("#73CFB6");
                    BackgroundColor = Color.FromHex("#FFFFFF");
                }
                else
                {
                    BackgroundColor = Color.FromHex("#FFFFFF");
                }
                NotifyPropertyChanged("Seleccionado");
                NotifyPropertyChanged("BackgroundColor");
            }
        }

        public Color BackgroundColor { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
        [JsonIgnore]
        public bool IsVisibleDesde => IdCategorias != 0;
    }
}
