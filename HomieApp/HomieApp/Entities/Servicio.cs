﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomieApp.Entities
{
    public class Servicio
    {
        public long IdServicio { get; set; }
        public long IdCliente { get; set; }
        public int IdTipo { get; set; }
        public bool Garantia { get; set; }
        public DateTime FechaProgramado { get; set; }
        public TimeSpan InicioProgramado { get; set; }
        public TimeSpan FinProgramado { get; set; }
        public string Direccion { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public Nullable<long> IdDireccion { get; set; }
        public string Foto { get; set; }
        public string Video { get; set; }
        public int Estatus { get; set; }
        public decimal MontoTarifa { get; set; }
        public int IdCategoria { get; set; }

        public long IdProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public string EstatusProveedor { get; set; }

        public string IconoCategoria { get; set; }
        public string NombreCategoria { get; set; }
        public string Detalle { get; set; }
        public string DetalleDireccion { get; set; }

        public List<ServicioMateriales> Materiales { get; set; }

        public bool ChatHabilitado { get; set; }
        public List<Categorias> Categorias { get; set; }

        [JsonIgnore]
        public string TipoString
        {
            //get { return IdTipo == TipoServicio.Agendado ? "Agendado" : "Urgente"; }
            get
            {
                switch (IdTipo)
                {
                    case TipoServicio.Agendado:
                        return "Agendado";
                    case TipoServicio.Urgente:
                        return "Urgente";
                    default:
                        return "Garantía";

                }
            }
        }

        [JsonIgnore]
        public string EstatusString
        {
            get
            {
                switch (Estatus)
                {
                    case EstatusServicio.Creado:
                        return "Se ha creado el servicio";
                    case EstatusServicio.PorIniciar:
                        return "Se hizo match con el proveedor";
                    case EstatusServicio.Confirmado:
                        return "Se aceptó la cotización";
                    case EstatusServicio.Iniciado:
                        return "Servicio en progreso";
                    case EstatusServicio.Terminado:
                        return "Servicio Finalizado";
                    case EstatusServicio.Cancelado:
                        return "Servicio Cancelado";
                    default:
                        return "";
                }
            }
        }

        [JsonIgnore]
        public string FechaString => FechaProgramado.ToString("dd/MM/yyyy");

        [JsonIgnore]
        public List<Cargo> Cargos
        {
            get
            {
                var cargo = new List<Cargo>();

                cargo.Add(new Cargo
                {
                    Descripcion = "Tarifa",
                    Importe = MontoTarifa
                });

                if (Materiales != null && Materiales.Count > 0)
                {
                    Materiales.ForEach(x =>
                    {
                        cargo.Add(new Cargo
                        {
                            Descripcion = x.Descripcion,
                            Importe = x.Importe
                        });
                    });
                }

                decimal total = 0;
                cargo.ForEach(x =>
                {
                    total += x.Importe;
                });
                total = total<0 ? 0 : total;

                cargo.Add(new Cargo
                {
                    Descripcion = "Total",
                    Importe = total
                });

                return cargo;
            }
        }

        [JsonIgnore]
        public string MapImage
        {
            get
            {
                var map = new MapImage((double)Latitud,(double)Longitud);
                return map.GetUri();
            }
        }
    }
}
