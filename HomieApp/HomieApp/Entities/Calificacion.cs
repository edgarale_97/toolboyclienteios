﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class

        Calificacion
    {
        public long IdCalificacion { get; set; }
        public Nullable<long> IdCliente { get; set; }
        public Nullable<long> IdProveedor { get; set; }
        public long IdServicio { get; set; }
        public int Tipo { get; set; }
        public short CalificacionGeneral { get; set; }
        public string Observaciones { get; set; }

        // SubZero
        public short Equipo { get; set; }
        public short Servicio { get; set; }
        public short TiempoRespuesta { get; set; }
        public short ProfesionalismoAtencion { get; set; }
        public short Puntualidad { get; set; }
        public short PresentacionTecnico { get; set; }
        public short Uniforme { get; set; }
        public short ProfesionalismoTecnico { get; set; }
        public short TiempoResolucion { get; set; }
        public short Experiencia { get; set; }
        public short TiempoEntrega { get; set; }
        public short CostoServicio { get; set; }

    }
}
