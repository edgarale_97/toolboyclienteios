﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HomieApp.Entities
{
    public class ServicioGarantia
    {
        public long IdServicio { get; set; }
        public List<int> IdsCategorias { get; set; }
    }
}

