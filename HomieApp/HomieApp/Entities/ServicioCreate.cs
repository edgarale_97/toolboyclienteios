﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomieApp.Entities
{
    public class ServicioCreate
    {
        public long IdTipo { get; set; }
        public long Activo { get; set; }
        public bool IsVigente { get; set; }

        [JsonIgnore]
        public List<Categorias> Categorias { get; set; }
        [JsonIgnore]
        public Categorias CategoriaPadre { get; set; }

        public int IdCategoriaPadre { get; set; }
        public List<int> IdsCategorias { get; set; }

        public Direcciones Direccion { get; set; }

        public string Descripcion { get; set; }
        public string Foto { get; set; }
        public byte[] FotoBytes { get; set; }

        public string Video { get; set; }

        public DateTime Fecha { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }

        public string CodigoDescuento { get; set; }

        public int IdRelacionCliente { get; set; }

        public string Tipo
        {
            get { return IdTipo == TipoServicio.Agendado ? "Servicio Agendado" : "Servicio Urgente"; }
        }

        public string NotaDireccion { get; set; }

    }
}
