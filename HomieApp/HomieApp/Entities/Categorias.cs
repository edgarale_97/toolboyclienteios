﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HomieApp.Entities
{
    public class Categorias : INotifyPropertyChanged
    {
        private bool _seleccionado;
        public int IdCategorias { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Icono { get; set; }
        public decimal Monto { get; set; }

        public int? IdCategoriaPadre { get; set; }

        public int IdRelacion { get; set; }

        [JsonIgnore]
        public bool IsVisibleFondo => IdCategoriaPadre == null;

        public bool Seleccionado
        {
            get { return _seleccionado; }
            set
            {
                _seleccionado = value;

                if (_seleccionado)
                {
                    //BackgroundColor = Color.FromHex("#11000000");
                    BackgroundColor = Color.FromHex("#73CFB6");
                }
                else
                {
                    BackgroundColor = Color.FromHex("#FFFFFF");
                }

                NotifyPropertyChanged("Seleccionado");
                NotifyPropertyChanged("BackgroundColor");
            }
        }

        public Color BackgroundColor { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        [JsonIgnore]
        public bool IsVisibleDesde => Monto != 0;
    }
}
