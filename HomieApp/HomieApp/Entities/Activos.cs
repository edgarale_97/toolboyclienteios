﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HomieApp.Entities
{
    public class Activos
    {
        public long IdActivos { get; set; }
        public long IdCliente { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        //public string Factura { get; set; }
        //public string NumeroSerie { get; set; }
        public string Descripcion { get; set; }
        public string FotoEquipo { get; set; }
        public string FotoFactura { get; set; }
        public DateTime FechaCompra { get; set; }
        public DateTime FechaVigencia { get; set; }
        public bool IsActivo { get; set; }
        public int IdCategoria { get; set; }
        public string NombreCategoria { get; set; }
        //public bool IsService { get; set; }

        public Categorias Categorias { get; set; }

        [JsonIgnore]
        public string FechaString => FechaCompra.ToString("dd/MM/yyyy");

        [JsonIgnore]
        public string Vigencia
        {
            get
            {
                DateTime oldDate = FechaVigencia;
                DateTime newDate = DateTime.Now;

                TimeSpan ts = oldDate - newDate;
                int difDays = ts.Days;
                if(difDays <= 0)
                {
                    return "Vigencia expirada";
                }
                return difDays.ToString() + " días restantes";
            }
        }

        [JsonIgnore]
        public Color IsVigente
        {
            get
            {
                DateTime oldTime = FechaVigencia;
                DateTime newTime = DateTime.Now;

                TimeSpan ts = oldTime - newTime;
                int difDias = ts.Days;
                if(difDias < 10)
                {
                    return (Color)Application.Current.Resources["WizardOrange"];
                }
                if(difDias > 10)
                {
                    return (Color)Application.Current.Resources["LabelBlueColor"];
                }
                return Color.Black;
            }
        }

        [JsonIgnore]
        public string VigenciaString
        {
            get
            {
                DateTime oldDate = FechaVigencia;
                DateTime newDate = DateTime.Now;

                TimeSpan ts = oldDate - newDate;
                int difDays = ts.Days;
                if (difDays <= 0)
                {
                    return "Expirada";
                }

                return "Vigente";
            }
        }

        [JsonIgnore]
        public bool BoolVigente
        {
            get
            {
                DateTime oldDate = FechaVigencia;
                DateTime newDate = DateTime.Now;

                TimeSpan ts = oldDate - newDate;
                int difDays = ts.Days;
                if(difDays <= 0)
                {
                    return false;
                }
                return true;
            }
        }
        

    }
}

