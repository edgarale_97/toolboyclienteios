﻿using System;

namespace HomieApp.Entities
{
    public class Archivo
    {
        public int IdServicio { get; set; }
        public DateTime Fecha { get; set; }
        public string TipoArchivo { get; set; }
        public string Ruta { get; set; }

        public string FechaString => Fecha.ToString("dd/MM/yyyy hh:mm tt");

        
    }
}

