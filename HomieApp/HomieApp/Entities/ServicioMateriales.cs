﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieApp.Entities
{
    public class ServicioMateriales
    {
        public long IdServicioMaterial { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> Cantidad { get; set; }
        public decimal Importe { get; set; }
        public bool Aplicado { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaAplicacion { get; set; }
        public bool MaterialExtra { get; set; }
        public long IdServicio { get; set; }
        public bool Activo { get; set; }
        public bool Editable { get; set; }
        public Nullable<bool> AceptadoCliente { get; set; }

        public string TextCantidad => Cantidad.ToString();
        public string TextImporte => Importe.ToString("C");
    }
}
