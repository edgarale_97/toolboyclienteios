﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Helpers;
using HomieApp.Models;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views;
using HomieApp.Views.Common;
using HomieApp.Views.Navigation;
using HomieApp.Views.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp
{
    public partial class RootPage : MasterDetailPage
    {
        public event EventHandler CerrarSesion;

        public RootPage() : this(false)
        {
        }

        public RootPage(bool sayWelcome)
        {
            InitializeComponent();
            // Empty pages are initially set to get optimal launch experience
            Master = new ContentPage { Title = "Homie"};

            var page = new ContentPage
            {
                BackgroundColor = Color.White,
                Content = new CustomActivityIndicator
                {
                    IsVisible = true,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                }
            };
            Detail = NavigationPageHelper.Create(page);

            MessagingCenter.Subscribe<MainLabel>(this, "Menu", (sender) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.IsPresented = true;
                });
            });

            this.IsGestureEnabled = false;
        }

        public async void OnSettingsTapped(Object sender, EventArgs e)
        {

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var navigationPage = string.Empty;
            if (Detail?.Navigation != null && Detail.Navigation.NavigationStack.Count > 0)
            {
                navigationPage = Detail.Navigation.NavigationStack[0].GetType().Name;
            }

            if (navigationPage != "TarjetasPage")
            {
                await NavigationService.BeginInvokeOnMainThreadAsync(InitializeMasterDetail);

                var model = new LoginModel();
                var result = await model.AutenticarUsuarioAsync(Settings.User, Settings.Password);

                if (result.Error)
                {
                    await model.LogOutAsync();
                    Settings.User = string.Empty;
                    Settings.Password = string.Empty;
                    Settings.Token = string.Empty;
                    Settings.Confirmado = false;
                    Settings.Registrado = true;
                    OnCerrarSesion();
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void LaunchSampleInDetail(Page page, bool animated)
        {
            if (page is ServiciosPage)
            {
                //if (!Settings.MetodoPago)
                //{
                //    await Application.Current.MainPage.DisplayAlert("Notificación", "Debe registrar un método de pago.", "OK");
                //    Detail = NavigationPageHelper.Create(new TarjetasPage());
                //}
                //else
                //{
                //    Detail = NavigationPageHelper.Create(page);
                //}

                Detail = NavigationPageHelper.Create(page);
            }
            else
            {
                Detail = NavigationPageHelper.Create(page);
            }

            IsPresented = false;
        }

        private async void InitializeMasterDetail()
        {
            var master = new MainMenuPage(new NavigationService(Navigation, LaunchSampleInDetail));
            master.CerrarSesion += Master_CerrarSesion;
            Master = master;

            if (!Settings.MetodoPago)
            {
                var detail = new TabPage(true);
                detail.CerrarSesion += Master_CerrarSesion;
                Detail = NavigationPageHelper.Create(detail);
            }
            else
            {
                //Detail = NavigationPageHelper.Create(new ServiciosPage());
                var detail = new TabPage();
                detail.CerrarSesion += Master_CerrarSesion;
                Detail = NavigationPageHelper.Create(detail);
            }
        }

        private void Master_CerrarSesion(object sender, EventArgs e)
        {
            OnCerrarSesion();
        }

        protected virtual void OnCerrarSesion()
        {
            EventHandler handler = CerrarSesion;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}