﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BottomBar.XamarinForms;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using HomieApp.ViewModel;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Common;
using HomieApp.Views.Servicio;
using Newtonsoft.Json;
using Plugin.Badge;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Navigation
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TabPage : BottomBarPage
	{
	    public event EventHandler CerrarSesion;
		public TabPage (bool registrarTarjeta = false)
		{
		    Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();

            this.BarTextColor = Color.FromHex("#00a99d"); 
		    this.FixedMode = true;

		    MessagingCenter.Subscribe<MultimediaViewModel, long>(this, "BuscandoProveedor", async (sender, id) => {

		        AbrirServicios();

                // Mostramos el view de buscando proveedor.
                await Navigation.PushAsync(new Page5BuscandoProveedor(id), true);
		    });

            MessagingCenter.Subscribe<DetalleServicioViewModel, long>(this, "BuscandoProveedor", async (sender, id) =>
            {
                AbrirServicios();
                await Navigation.PushAsync(new Page5BuscandoProveedor(id), true);
            });

            MessagingCenter.Subscribe<App, long>((App)Application.Current, "BuscandoProveedor", async (sender, id) =>
            {
                AbrirServicios();
                await Navigation.PushAsync(new Page5BuscandoProveedor(id), true);
            });

		    MessagingCenter.Subscribe<App, long>((App)Application.Current, "ChatProveedor", async (sender, id) => {

		        AbrirServicios();

                // Mostramos el view del chat.
                await Navigation.PushAsync(new ChatPage(id, false));
		    });

		    MessagingCenter.Subscribe<App, long>((App)Application.Current, "AceptaCotizacion", async (sender, id) => {

		        // Mostramos el view del servicio.
		        try
		        {
		            await Navigation.PushAsync(new DetalleServicioPage(id, false));
		        }
		        catch (Exception e)
		        {
		            Console.WriteLine(e);
		            throw;
		        }
		    });

		    MessagingCenter.Subscribe<App, long>((App)Application.Current, "DetalleServicio", async (sender, id) => {

                await Navigation.PopToRootAsync();
                await Navigation.PushAsync(new DetalleServicioPage(id, false));

            });

            MessagingCenter.Subscribe<MainLabelNavigation>(this, "AbrirServicios", (sender) =>
		    {
		        AbrirServicios();
		    });

            MessagingCenter.Subscribe<App>((App)Application.Current,"SeTerminoElServicio", app =>
            {
                AbrirServicios();
            } );

		    // Cuando llega notificación con la app en primer plano.
		    MessagingCenter.Subscribe<App, Notificaciones>((App)Application.Current, "HubNotification", async (sender, notificacion) =>
		    {
		        OpenFromNotification(notificacion);
		    });

		    // Notificación seleccionada desde la pantalla de notificaciones.
		    MessagingCenter.Subscribe<App, Notificaciones>((App)Application.Current, "NotificacionSeleccionada", async (sender, notificacion) =>
		    {
		        OpenNotificacionSeleccionada(notificacion);
		    });

		    // Notificación abierta desde el notification bar.
		    MessagingCenter.Subscribe<App, Notificaciones>((App)Application.Current, "OpenFromNotificationBar", async (sender, notificacion) =>
		    {
		        OpenFromNotificacionBar(notificacion);
		    });

            if (Device.RuntimePlatform == Device.iOS)
		    {
		        TabServicios.Padding = new Thickness(0, 30, 0, 0);
                TabHome.Padding = new Thickness(0, 30, 0, 0);
                TabGarantias.Padding = new Thickness(0, 30, 0, 0);
                TabNotificaciones.Padding = new Thickness(0, 30, 0, 0);
                TabPerfil.Padding = new Thickness(0, 30, 0, 0);
            }
            TabPerfil.CerrarSesion += TabPerfilOnCerrarSesion;

		    if (registrarTarjeta)
		    {
		        AbrirTarjetas();
		    }

		    BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);
        }

	    private async Task AbrirTarjetas()
	    {
	        await Application.Current.MainPage.DisplayAlert("Notificación", "Debe registrar un método de pago", "OK");
	        await Navigation.PushAsync(new TarjetasPage());
	    }

	    private void TabPerfilOnCerrarSesion(object o, EventArgs eventArgs)
	    {
	        OnCerrarSesion();
	    }

	    protected virtual void OnCerrarSesion()
	    {
	        EventHandler handler = CerrarSesion;
	        if (handler != null)
	        {
	            handler(this, EventArgs.Empty);
	        }
	    }

        private async Task AbrirServicios()
        {
            await Navigation.PopToRootAsync();
            
            var vm = TabServicios.BindingContext as ServiciosViewModel;
            await vm.BuscarServicios();
            this.SelectedItem = TabServicios;
        }

	    protected async override void OnAppearing()
	    {
	        base.OnAppearing();
	        BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);
	    }

        private async void OpenFromNotification(Notificaciones notificacion)
        {
			if (!string.IsNullOrWhiteSpace(notificacion.PageToOpen))
			{
				var parameters = JsonConvert.DeserializeObject<NotificationParameters>(notificacion.Parameters);
				Settings.Notificaciones = 1;
				CrossBadge.Current.ClearBadge();
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, 0);
				CrossBadge.Current.SetBadge(Settings.Notificaciones);
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);
                switch (notificacion.PageToOpen)
                {
                    case NotificacionPage.ServicioTerminado:
                    case NotificacionPage.Servicio:
                        await AbrirServicios();

                        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                            await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, false));

                        await Application.Current.MainPage.DisplayAlert(notificacion.Titulo, notificacion.Descripcion, "OK");
                        /*if(notificacion.Titulo == "Servicio atrasado")
                        {
                            Console.WriteLine($"Mensaje de atraso ----> {notificacion.Titulo}");
                            var demora = new DemoraModel();
                            var result = await demora.GetAsync(parameters.IdServicio);
                            if (result.Error)
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                            }
                            else
                            {
                                Console.WriteLine($"Se mando correctamente y supuestamente la notificacion {result.ErrorDescription}");
                            }
                        }*/

                        break;

                    case NotificacionPage.Chat:

                        if (Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
                        {
                            var page = Navigation.NavigationStack.FirstOrDefault(x => x.GetType() == typeof(ChatPage));

                            if (page != null)
                            {
                                var chat = (ChatPage)page;
                                await chat.UpdateAsync();
                            }
                        }
                        else
                        {
                            if (Device.RuntimePlatform == Device.iOS)
                                await Application.Current.MainPage.DisplayAlert(notificacion.Titulo, notificacion.Descripcion, "OK");
                        }

                        break;

                    case NotificacionPage.SeguimientoProveedor:

                        await AbrirServicios();

                        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)))
                            await Navigation.PushAsync(new SeguimientoProveedorPage(parameters.IdServicio));

                        await Application.Current.MainPage.DisplayAlert(notificacion.Titulo, notificacion.Descripcion, "OK");

                        break;
                    case NotificacionPage.BuscandoProveedor:

                        await AbrirServicios();

                        var model = new ServiciosModel();
                        var res = await model.GetByIdAsync(parameters.IdServicio);
                        if (!res.Error)
                        {
                            if (res.Result.Estatus == EstatusServicio.PorIniciar)
                            {
                                if (res.Result.ChatHabilitado)
                                {
                                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
                                    {
                                        // Mostramos el view del chat.
                                        await Navigation.PushAsync(new ChatPage(parameters.IdServicio, false));
                                    }
                                }
                                else
                                {
                                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                                        await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, false));
                                }
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(notificacion.Titulo, notificacion.Descripcion, "OK");

                        break;
                }
            }
        }

	    private async void OpenNotificacionSeleccionada(Notificaciones notificacion)
	    {
	        if (!string.IsNullOrWhiteSpace(notificacion.PageToOpen))
	        {
	            var parameters = JsonConvert.DeserializeObject<NotificationParameters>(notificacion.Parameters);
				CrossBadge.Current.ClearBadge();
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, 0);
				Settings.Notificaciones = 0;
				switch (notificacion.PageToOpen)
	            {
	                case NotificacionPage.ServicioTerminado:
                    case NotificacionPage.Servicio:
	                    await AbrirServicios();

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
	                        await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, false));

	                    break;

	                case NotificacionPage.Chat:

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
	                    {
	                        await AbrirServicios();

	                        // Mostramos el view del chat.
	                        await Navigation.PushAsync(new ChatPage(parameters.IdServicio, false));
	                    }
	                    else
	                    {
	                        var page = Navigation.NavigationStack.FirstOrDefault(x => x.GetType() == typeof(ChatPage));

	                        if (page != null)
	                        {
	                            var chat = (ChatPage)page;
	                            await chat.UpdateAsync();
	                        }
	                    }

	                    break;

	                case NotificacionPage.SeguimientoProveedor:
	                    await AbrirServicios();

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)))
	                        await Navigation.PushAsync(new SeguimientoProveedorPage(parameters.IdServicio));

	                    break;
	                case NotificacionPage.BuscandoProveedor:

	                    await AbrirServicios();

	                    var model = new ServiciosModel();
	                    var res = await model.GetByIdAsync(parameters.IdServicio);
	                    if (!res.Error)
	                    {
	                        if (res.Result.Estatus == EstatusServicio.PorIniciar)
	                        {
	                            if (res.Result.ChatHabilitado)
	                            {
	                                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
	                                {
	                                    // Mostramos el view del chat.
	                                    await Navigation.PushAsync(new ChatPage(parameters.IdServicio, false));
	                                }
	                            }
	                            else
	                            {
	                                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
	                                    await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, false));
	                            }
	                        }
	                    }

	                    break;
	            }
	        }
	    }

	    private async void OpenFromNotificacionBar(Notificaciones notificacion)
	    {
	        if (!string.IsNullOrWhiteSpace(notificacion.PageToOpen))
	        {
	            var parameters = JsonConvert.DeserializeObject<NotificationParameters>(notificacion.Parameters);
				CrossBadge.Current.ClearBadge();
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, 0);
				Settings.Notificaciones = 0;
				switch (notificacion.PageToOpen)
	            {
	                case NotificacionPage.ServicioTerminado:
                    case NotificacionPage.Servicio:
	                    await AbrirServicios();

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
	                        await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, false));

	                    break;

	                case NotificacionPage.Chat:

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
	                    {
	                        await AbrirServicios();

	                        // Mostramos el view del chat.
	                        await Navigation.PushAsync(new ChatPage(parameters.IdServicio, false));
	                    }
	                    else
	                    {
	                        var page = Navigation.NavigationStack.FirstOrDefault(x => x.GetType() == typeof(ChatPage));

	                        if (page != null)
	                        {
	                            var chat = (ChatPage)page;
	                            await chat.UpdateAsync();
	                        }
	                    }

	                    break;

	                case NotificacionPage.SeguimientoProveedor:
	                    await AbrirServicios();

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)))
	                        await Navigation.PushAsync(new SeguimientoProveedorPage(parameters.IdServicio));

	                    break;
	                case NotificacionPage.BuscandoProveedor:

	                    await AbrirServicios();

	                    var model = new ServiciosModel();
	                    var res = await model.GetByIdAsync(parameters.IdServicio);
	                    if (!res.Error)
	                    {
	                        if (res.Result.Estatus == EstatusServicio.PorIniciar)
	                        {
	                            if (res.Result.ChatHabilitado)
	                            {
	                                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
	                                {
	                                    // Mostramos el view del chat.
	                                    await Navigation.PushAsync(new ChatPage(parameters.IdServicio, false));
	                                }
	                            }
	                            else
	                            {
	                                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
	                                    await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, false));
	                            }
	                        }
	                    }

	                    break;
	            }
	        }
	    }
    }
}