﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HomieApp
{
    public class LetterSpacingLabel : Label
    {
        public float LetterSpacing { get; set; }
    }
}
