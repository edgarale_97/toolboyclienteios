﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp
{
	public partial class GradientButton : Grid
    {
		public GradientButton ()
		{
			InitializeComponent ();
		    var tapGestureRecognizer = new TapGestureRecognizer();
		    tapGestureRecognizer.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(async () =>
		        {
		            if (this.IsEnabled)
		            {
		                this.AnchorX = 0.48;
		                this.AnchorY = 0.48;
		                await this.ScaleTo(0.8, 50, Easing.Linear);
		                //await Task.Delay(100);
		                await this.ScaleTo(1, 50, Easing.Linear);

		                if (Command != null && this.IsEnabled)
		                {
		                    Command.Execute(CommandParameter);
		                }

		                Clicked?.Invoke(this, EventArgs.Empty);
		            }
		        });
		    };

		    this.GestureRecognizers.Add(tapGestureRecognizer);
        }

        public event EventHandler Clicked;

        public static readonly BindableProperty CommandProperty = BindableProperty.Create<GradientButton, ICommand>(p => p.Command, null);
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create<GradientButton, object>(p => p.CommandParameter, null);
        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static BindableProperty TextProperty =
            BindableProperty.Create(
                nameof(Text),
                typeof(string),
                typeof(GradientButton),
                string.Empty,
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: (bindable, oldValue, newValue) => {
                    var ctrl = (GradientButton)bindable;
                    ctrl.Text = (string)newValue;
                }
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set
            {
                SetValue(TextProperty, value);
                TextLabel.Text = value;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    TextLabel.IsVisible = true;
                }
            }
        }

        public new static BindableProperty TextColorProperty =
            BindableProperty.Create(
                nameof(TextColor),
                typeof(Color),
                typeof(GradientButton),
                defaultValue: Color.White,
                defaultBindingMode: BindingMode.OneWay
            );

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public static BindableProperty ButtonTypeProperty =
            BindableProperty.Create(
                nameof(ButtonType),
                typeof(GradientButtonType),
                typeof(GradientButton),
                GradientButtonType.Silver,
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: (bindable, oldValue, newValue) => {
                    var ctrl = (GradientButton)bindable;

                    ctrl.ButtonType = (GradientButtonType)newValue;
                }
            );

        public GradientButtonType ButtonType
        {
            get { return (GradientButtonType)GetValue(ButtonTypeProperty); }
            set
            {
                SetValue(ButtonTypeProperty, value);

                switch (value)
                {
                    case GradientButtonType.Accent:
                        BackgroundImage.Source = "accent_button.png";
                        //TextLabel.TextColor = Color.White;
                        break;
                    case GradientButtonType.Silver:
                        BackgroundImage.Source = "light_button.png";
                        break;
                    case GradientButtonType.Light:
                        BackgroundImage.Source = "light_button.png";
                        break;
                    case GradientButtonType.Gray:
                        BackgroundImage.Source = "gray_button.png";
                        //TextLabel.TextColor = Color.White;
                        break;
                    default:
                        BackgroundImage.Source = "accent_button.png";
                        break;
                }
            }
        }
    }

    public enum GradientButtonType
    {
        Accent,
        Silver,
        Light,
        Gray
    }
}