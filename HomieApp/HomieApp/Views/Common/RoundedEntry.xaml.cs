﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp
{
	public partial class RoundedEntry : ContentView
	{
		public RoundedEntry ()
		{
			InitializeComponent ();
		}

	    public static BindableProperty TextProperty =
	        BindableProperty.Create(
	            nameof(Text),
	            typeof(string),
	            typeof(RoundedEntry),
	            "",
	            BindingMode.TwoWay
	        );

	    public string Text
	    {
	        get { return (string)GetValue(TextProperty); }
	        set { SetValue(TextProperty, value); }
	    }

	    public static BindableProperty PlaceholderProperty =
	        BindableProperty.Create(
	            nameof(Placeholder),
	            typeof(string),
	            typeof(RoundedEntry),
	            defaultValue: "",
	            defaultBindingMode: BindingMode.OneWay
	        );

	    public string Placeholder
	    {
	        get { return (string)GetValue(PlaceholderProperty); }
	        set { SetValue(PlaceholderProperty, value); }
	    }

	    public static BindableProperty IsPasswordProperty =
	        BindableProperty.Create(
	            nameof(IsPassword),
	            typeof(bool),
	            typeof(RoundedEntry),
	            defaultValue: false,
	            defaultBindingMode: BindingMode.OneWay
	        );

	    public bool IsPassword
	    {
	        get { return (bool)GetValue(IsPasswordProperty); }
	        set { SetValue(IsPasswordProperty, value); }
	    }

	    protected override void OnPropertyChanged(string propertyName)
	    {
	        base.OnPropertyChanged(propertyName);

	        if (propertyName == nameof(Text))
	        {
	            if (string.IsNullOrWhiteSpace(Placeholder))
	            {
	                PlaceholderControl.Text = string.Empty;
	            }
	            else
	            {
	                // Placeholder dp
	                PlaceholderControl.Text = Placeholder;
	            }
	        }
	    }
    }
}