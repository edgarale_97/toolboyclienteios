﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;        
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VideoPage : ContentPage
	{
		public VideoPage (string video)
		{
		    NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();

		    loadingGrid.IsVisible = false;
            
            Media.Source = new Uri(video);
            //Media.Source = new Uri("http://video.ch9.ms/ch9/334f/891b78a5-642d-40b4-8d02-ff40ffdd334f/LoginToLinkedinUSingXamarinAuth_mid.mp4");
        }
    }
}