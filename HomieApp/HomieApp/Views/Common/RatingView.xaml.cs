﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RatingView : ContentView
	{
        public static BindableProperty RatingProperty =
            BindableProperty.Create(
                nameof(Rating),
                typeof(int),
                typeof(RatingView),
                0,
                BindingMode.TwoWay);

	    public int Rating
	    {
            get { return (int) GetValue(RatingProperty); }
            set { SetValue(RatingProperty, value); }
	    }

        public static  BindableProperty ReadOnlyProperty = 
            BindableProperty.Create(
                nameof(ReadOnly),
                typeof(bool),
                typeof(RatingView),
                false);

	    public bool ReadOnly
	    {
            get { return (bool) GetValue(ReadOnlyProperty); }
            set { SetValue(ReadOnlyProperty, value); }
	    }

        public static BindableProperty SizeProperty = 
            BindableProperty.Create(
                nameof(Size),
                typeof(int),
                typeof(RatingView),
                12);

	    public int Size
	    {
            get { return (int) GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
	    }

		public RatingView ()
		{
			InitializeComponent ();
            var tgr = new TapGestureRecognizer();
		    tgr.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            if (!ReadOnly)
		            {
		                Rating = 2;
		            }
		        });
		    };
            StarOne.GestureRecognizers.Add(tgr);

            var tgr2 = new TapGestureRecognizer();
		    tgr2.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            if (!ReadOnly)
		            {
		                Rating = 4;
		            }
		        });
		    };
            StarTwo.GestureRecognizers.Add(tgr2);

            var tgr3 = new TapGestureRecognizer();
		    tgr3.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            if (!ReadOnly)
		            {
		                Rating = 6;
		            }
		        });
		    };
            StarThree.GestureRecognizers.Add(tgr3);

            var tgr4 = new TapGestureRecognizer();
		    tgr4.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            if (!ReadOnly)
		            {
		                Rating = 8;
		            }
		        });
		    };
            StarFour.GestureRecognizers.Add(tgr4);

            var tgr5 = new TapGestureRecognizer();
		    tgr5.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            if (!ReadOnly)
		            {
		                Rating = 10;
		            }
		        });
		    };
            StarFive.GestureRecognizers.Add(tgr5);
		}

	    protected override void OnPropertyChanged(string propertyName = null)
	    {
	        base.OnPropertyChanged(propertyName);

	        if (propertyName == nameof(Size))
	        {
	            StarOne.FontSize = Size;
	            StarTwo.FontSize = Size;
	            StarThree.FontSize = Size;
	            StarFour.FontSize = Size;
	            StarFive.FontSize = Size;
	        }

	        if (propertyName == nameof(Rating))
	        {
	            var vacio = Icon.FAStarO;
	            var medio = Icon.FAStarHalfO;
	            var lleno = Icon.FAStar;

	            switch (Rating)
	            {
                    case 0:
                        StarOne.Text = vacio;
                        StarTwo.Text = vacio;
                        StarThree.Text = vacio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 1:
                        StarOne.Text = medio;
                        StarTwo.Text = vacio;
                        StarThree.Text = vacio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 2:
                        StarOne.Text = lleno;
                        StarTwo.Text = vacio;
                        StarThree.Text = vacio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 3:
                        StarOne.Text = lleno;
                        StarTwo.Text = medio;
                        StarThree.Text = vacio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 4:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = vacio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 5:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = medio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 6:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = lleno;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;
                    case 7:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = lleno;
                        StarFour.Text = medio;
                        StarFive.Text = vacio;
                        break;
                    case 8:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = lleno;
                        StarFour.Text = lleno;
                        StarFive.Text = vacio;
                        break;
                    case 9:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = lleno;
                        StarFour.Text = lleno;
                        StarFive.Text = medio;
                        break;
                    case 10:
                        StarOne.Text = lleno;
                        StarTwo.Text = lleno;
                        StarThree.Text = lleno;
                        StarFour.Text = lleno;
                        StarFive.Text = lleno;
                        break;
                    default:
                        StarOne.Text = vacio;
                        StarTwo.Text = vacio;
                        StarThree.Text = vacio;
                        StarFour.Text = vacio;
                        StarFive.Text = vacio;
                        break;

                }
	        }
	    }
	}
}