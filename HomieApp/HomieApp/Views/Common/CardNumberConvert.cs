﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace HomieApp.Views.Common
{
    public class CardNumberConvert : Behavior<Entry>
    {
        public int? MaxLength { get; set; }
        public string Mask { get; set; }
        public bool? IsNumeric { get; set; }
        public bool? IsAmount { get; set; }
        public bool? IsNumericWithSpace { get; set; }

        XamarinMaxLength xamarinMaxLength;
        XamarinIsNumeric xamarinIsNumeric;
        XamarinIsAmount xamarinIsAmount;
        XamarinMask xamarinMask;
        XamarinIsNumericWithSpace xamarinIsNumericWithSpace;

        public CardNumberConvert()
        {
            xamarinMaxLength = new XamarinMaxLength();
            xamarinIsNumeric = new XamarinIsNumeric();
            xamarinIsAmount = new XamarinIsAmount();
            xamarinMask = new XamarinMask();
            xamarinIsNumericWithSpace = new XamarinIsNumericWithSpace();
        }

        protected override void OnAttachedTo(Entry entry)
        {
            if (entry == null) return;
            entry.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            if (entry == null) return;
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            if (sender == null || args == null || !(sender is Entry)) return;
            var entry = (Entry)sender;
            var oldString = args.OldTextValue;
            var newString = args.NewTextValue;
            string entryText = entry.Text;

            if(MaxLength != null && MaxLength >= 0 && entryText?.Length > 0)
            {
                var output = xamarinMaxLength.ProcessLength(entryText, oldString, newString, MaxLength);
                if(output != entryText)
                {
                    entryText = output;
                    entry.Text = entryText;
                    return;
                }
            }

            if (IsNumeric != null && IsNumeric == true && entryText?.Length > 0)
            {
                var output = xamarinIsNumeric.ProcessIsNumeric(entryText, oldString, newString);
                if (output != entryText)
                {
                    entryText = output;
                    entry.Text = entryText;
                    return;
                }
            }

            if (IsAmount != null && IsAmount == true && entryText?.Length > 0)
            {
                var output = xamarinIsAmount.ProcessIsAmount(entryText, oldString, newString);
                if (output != entryText)
                {
                    entryText = output;
                    entry.Text = entryText;
                    return;
                }
            }

            if (IsNumericWithSpace != null && IsNumericWithSpace == true && entryText?.Length > 0)
            {
                var output = xamarinIsNumericWithSpace.ProcessIsNumericWithSpace(entryText, oldString, newString);
                if (output != entryText)
                {
                    entryText = output;
                    entry.Text = entryText;
                    return;
                }
            }

            if (Mask != null && Mask.Length > 0 && entryText?.Length > 0)
            {
                var output = xamarinMask.ProcessMask(entryText, oldString, newString, Mask);
                if (output != entryText)
                {
                    entryText = output;
                    entry.Text = entryText;
                    return;
                }
            }
            entry.Text = entryText;
        }
    }
}

