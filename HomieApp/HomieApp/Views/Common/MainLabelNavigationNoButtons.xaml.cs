﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using HomieApp.Views.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainLabelNavigationNoButtons : ContentView
	{
        [Obsolete]
        public MainLabelNavigationNoButtons()
		{
			InitializeComponent();
		   
        }


	    public static BindableProperty LabelTextProperty =
	        BindableProperty.Create(
	            nameof(LabelText),
	            typeof(string),
	            typeof(RoundedLabel),
	            defaultValue: "",
	            defaultBindingMode: BindingMode.OneWay
	        );

	    public string LabelText
	    {
	        get { return (string)GetValue(LabelTextProperty); }
	        set { SetValue(LabelTextProperty, value); }
	    }

	    public static BindableProperty IsVisibleTextProperty =
	        BindableProperty.Create(
	            nameof(IsVisibleText),
	            typeof(bool),
	            typeof(RoundedLabel),
	            defaultValue: true,
	            defaultBindingMode: BindingMode.OneWay
	        );

	    public bool IsVisibleText
	    {
	        get { return (bool)GetValue(IsVisibleTextProperty); }
	        set { SetValue(IsVisibleTextProperty, value); }
	    }


	    public static BindableProperty IsVisibleMenuProperty =
	        BindableProperty.Create(
	            nameof(IsVisibleMenu),
	            typeof(bool),
	            typeof(MainLabel),
	            defaultValue: false,
	            defaultBindingMode: BindingMode.OneWay);

	    public bool IsVisibleMenu
	    {
	        get { return (bool)GetValue(IsVisibleMenuProperty); }
	        set { SetValue(IsVisibleMenuProperty, value); }
	    }

	    public static readonly BindableProperty CommandProperty = BindableProperty.Create<MainLabel, ICommand>(p => p.Command, null);

	    public ICommand Command
	    {
	        get { return (ICommand)GetValue(CommandProperty); }
	        set { SetValue(CommandProperty, value); }
	    }

	    public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create<MainLabel, object>(p => p.CommandParameter, null);

	    public object CommandParameter
	    {
	        get { return (object)GetValue(CommandParameterProperty); }
	        set { SetValue(CommandParameterProperty, value); }
	    }

	    public event EventHandler Clicked;
    }
}