﻿using System;
using System.Threading.Tasks;
using HomieApp.Helpers;
using Xamarin.Forms;

namespace HomieApp
{
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();

            Settings.Principal = false;
            //if(Device.RuntimePlatform == Device.Android)
            //{
            //    Navigation.IsVisible = true;
            //}

		}

	    private void ButtonIniciarSesion_OnClicked(object sender, EventArgs e)
	    {
	        MessagingCenter.Send(this,"Login");
	    }

	    private void ButtonUnete_OnClicked(object sender, EventArgs e)
	    {
	        MessagingCenter.Send(this,"SignUp");
	    }

        // Boton para navegar en la app.
        private void ButtonServices_OnClicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "Navigation");
        }


        protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        // Workaround to fix image placement issue in iOS. It occurrs only when this 
	        // page (within a NavigationPage) is set as the detail of the MasterDetailPage 
	        // i.e. Display it from the RootPage
	        // navigation view
	        var content = this.Content;
	        this.Content = null;
	        this.Content = content;

	        Animations();
        }

	    private async void Animations()
	    {
	        ImageVuker.Opacity = 0;
	        ImageVuker.IsVisible = true;
	        ImageVuker.Scale = 0.8;

	        ButtonUnete.Opacity = 0;
	        ButtonUnete.IsVisible = true;
	        ButtonIniciarSesion.Opacity = 0;
	        ButtonIniciarSesion.IsVisible = true;

	        await Task.WhenAll(
	            ImageVuker.FadeTo(1, 600),
	            ImageVuker.ScaleTo(1, 600)
	        );

	        await Task.WhenAll(
	            ButtonUnete.FadeTo(1, 500),
	            ButtonIniciarSesion.FadeTo(1, 500)
	        );
	    }
    }
}