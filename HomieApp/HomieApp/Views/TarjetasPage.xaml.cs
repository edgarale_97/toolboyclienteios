﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.DependencyServices;
using HomieApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
	public partial class TarjetasPage : ContentPage
    {
        private TarjetasViewModel viewModel;

        public TarjetasPage()
        {
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent();

            viewModel = new TarjetasViewModel();
            viewModel.PropertyChanged += _viewModel_PropertyChanged;
            BindingContext = viewModel;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            //DependencyService.Get<ICardService>().StartCapture();
            var addCreditCard = new AgregarTarjetaPage();
            await Navigation.PushAsync(addCreditCard);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await viewModel.Buscar();
            /*var cardInfo = DependencyService.Get<ICardService>().GetCreditCard();

            if (cardInfo != null)
            {
                viewModel.NuevaTarjeta(cardInfo);
                DependencyService.Get<ICardService>().CloseCardService();
            }*/
        }

        private async void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Completed")
            {
                await viewModel.Buscar();
            }
        }

    }
}
