﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DurianCode.PlacesSearchBar;
using HomieApp.Helpers;
using HomieApp.ViewModel;
using HomieApp.Views.Common;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BusquedaDireccionPage : ContentPage
    {
        #region Propiedades

        private BusquedaDireccionViewModel _viewModel;
        public event EventHandler RegistroCompletado;

        protected virtual void OnRegistroCompletado()
        {
            EventHandler handler = RegistroCompletado;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public BusquedaDireccionPage()
        {
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent();

            map.IsShowingUser = true;
            map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(new Position(19.4326018d, -99.1353936d), 12d);
            map.PinDragEnd += Map_PinDragEnd;
            map.MyLocationButtonClicked += Map_MyLocationButtonClicked;
            search_bar.ApiKey = Settings.GooglePlacesApiKey;

            search_bar.Type = PlaceType.All;
            search_bar.Bias = new LocationBias(19.4326018d, -99.1353936d, 50000); // 50 km desde el zócalo (restringir las sugerencias de autocompletado a una región)
            search_bar.PlacesRetrieved += Search_Bar_PlacesRetrieved;
            search_bar.TextChanged += Search_Bar_TextChanged;
            search_bar.MinimumSearchText = 3;
            results_list.ItemSelected += Results_List_ItemSelected;

            _viewModel = new BusquedaDireccionViewModel();
            BindingContext = _viewModel;

            _viewModel.PropertyChanged += _viewModel_PropertyChanged;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }

        #endregion

        #region ViewModel PropertyChanged

        private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //Application.Current.MainPage.DisplayAlert("ViewModel", "Property", "OK");
            if (e.PropertyName == "Close")
            {
                OnRegistroCompletado();
            }
        }

        #endregion

        #region Places Events

        void Search_Bar_PlacesRetrieved(object sender, AutoCompleteResult result)
        {
            if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
            {
                foreach (var place in result.AutoCompletePlaces)
                {
                    var array = place.Description.Split(',');

                    if (array.Length > 0)
                    {
                        place.Reference = string.Empty;

                        for (int i = 0; i < array.Length; i++)
                        {
                            if (i == 0)
                            {
                                place.Description = array[i];
                            }
                            else
                            {
                                if (i == array.Length - 1)
                                {
                                    place.Reference += array[i];
                                }
                                else
                                {
                                    place.Reference += array[i] + ", ";
                                }
                            }
                        }
                    }
                }
            }

            results_list.ItemsSource = result.AutoCompletePlaces;
            _viewModel.IsLoading = false;

            if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
            {
                results_list.IsVisible = true;
                map.IsVisible = false;
            }
        }

        void Search_Bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                results_list.IsVisible = false;
                _viewModel.IsLoading = true;
            }
            else
            {
                results_list.IsVisible = true;
                _viewModel.IsLoading = false;
            }
        }

        async void Results_List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            map.IsVisible = true;
            var prediction = (AutoCompletePrediction)e.SelectedItem;
            results_list.SelectedItem = null;

            var place = await Places.GetPlace(prediction.Place_ID, Settings.GooglePlacesApiKey);

            if (place != null)
            {
                AddMarker(place.Latitude, place.Longitude, $"{prediction.Description}, {prediction.Reference}");
            }
        }

        private async void AddMarkerByPin(Pin pin)
        {
            _viewModel.IsLoading = true;

            var geoCoder = new Geocoder();
            var address = await geoCoder.GetAddressesForPositionAsync(new Position(pin.Position.Latitude, pin.Position.Longitude));

            _viewModel.IsLoading = true;

            search_bar.MinimumSearchText = 10000;
            search_bar.Text = address != null ? address.ToList().FirstOrDefault() : string.Empty;
            results_list.IsVisible = false;

            search_bar.MinimumSearchText = 3;
            _viewModel.IsLoading = false;

            _viewModel.Direccion = address.FirstOrDefault();
            _viewModel.Latitud = pin.Position.Latitude;
            _viewModel.Longitud = pin.Position.Longitude;
            _viewModel.IsLoading = false;
        }

        private void AddMarker(double latitud, double longitud, string direccion)
        {
            map.Pins.Clear();
            var pin = new Pin()
            {
                Type = PinType.Place,
                Label = direccion,
                Position = new Position(latitud, longitud),
                IsDraggable = true,
                Icon = BitmapDescriptorFactory.FromView(new BindingPinView())
            };

            map.Pins.Add(pin);
            map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(pin.Position, 18d);
            map.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromMeters(50)), false);

            search_bar.MinimumSearchText = 10000;
            search_bar.Text = direccion;
            results_list.IsVisible = false;

            search_bar.MinimumSearchText = 3;
            _viewModel.IsLoading = false;

            _viewModel.Direccion = direccion;
            _viewModel.Latitud = latitud;
            _viewModel.Longitud = longitud;
        }

        private async void Map_MyLocationButtonClicked(object sender, MyLocationButtonClickedEventArgs e)
        {
            _viewModel.IsLoading = true;

            if (CrossGeolocator.Current.IsGeolocationAvailable && CrossGeolocator.Current.IsGeolocationEnabled)
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;

                    var position = await locator.GetLastKnownLocationAsync();

                    if (position == null)
                    {
                        //got a cahched position, so let's use it.
                        position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
                    }

                    var geoCoder = new Geocoder();
                    var address = await geoCoder.GetAddressesForPositionAsync(new Position(position.Latitude, position.Longitude));

                    AddMarker(position.Latitude, position.Longitude, address != null ? address.ToList().FirstOrDefault() : string.Empty);
                    _viewModel.IsLoading = false;
                }
                catch (Exception ex)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", "No podemos accesar a tu ubicación", "OK");
                    _viewModel.IsLoading = false;
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No podemos accesar a tu ubicación", "OK");
            }
        }

        private void Map_PinDragEnd(object sender, PinDragEventArgs e)
        {
            AddMarkerByPin(e.Pin);
        }

        #endregion

       
        #region Override

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await Task.Delay(1000); // workaround for #30 [Android]Map.Pins.Add doesn't work when page OnAppearing

            if (map.Pins.Count == 0)
            {
                var pin = new Pin()
                {
                    Type = PinType.Place,
                    Label = "CDMX",
                    Position = new Position(19.4326018d, -99.1353936d),
                    IsDraggable = true,
                    Icon = BitmapDescriptorFactory.FromView(new BindingPinView())
                };
                map.Pins.Add(pin);
            }

            Debug.WriteLine("Appearing");
        }

        #endregion

    }
}