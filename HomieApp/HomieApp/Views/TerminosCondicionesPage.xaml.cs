﻿using System;
using HomieApp.Helpers;
using Xamarin.Forms;

namespace HomieApp.Views
{
    public partial class TerminosCondicionesPage : ContentPage
	{
	    public bool Aceptado { get; set; }
        public TerminosCondicionesPage ()
		{
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
		    Aceptado = false;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

            label.Text = Settings.TerminosYCondiciones;

        }


	    private void AceptarButton_OnClicked(object sender, EventArgs e)
	    {
	        Aceptado = true;
	        Navigation.PopModalAsync(true);
        }

	    private void CancelarButton_OnClicked(object sender, EventArgs e)
	    {
	        Aceptado = false;
	        Navigation.PopModalAsync(true);
        }
	}
}