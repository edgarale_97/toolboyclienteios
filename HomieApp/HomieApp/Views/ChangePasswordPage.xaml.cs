﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChangePasswordPage : ContentPage
	{
	    private ChangePasswordViewModel _vm;
		public ChangePasswordPage ()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            _vm = new ChangePasswordViewModel();
            _vm.PropertyChanged += VmOnPropertyChanged;
		    this.BindingContext = _vm;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private async void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            await Navigation.PopModalAsync();
	        }
	    }

	    private async void RoundedButton_OnClicked(object sender, EventArgs e)
	    {
	        await Navigation.PopModalAsync();
        }
	}
}