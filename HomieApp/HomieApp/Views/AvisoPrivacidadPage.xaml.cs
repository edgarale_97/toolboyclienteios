﻿using System;
using System.Collections.Generic;
using HomieApp.Helpers;
using Xamarin.Forms;

namespace HomieApp.Views
{
    public partial class AvisoPrivacidadPage : ContentPage
    {
        public AvisoPrivacidadPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

            labelAviso.Text = Settings.AvisoDePrivacidad;

        }
    }
}
