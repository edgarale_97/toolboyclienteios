﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.ViewModel;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServiciosPage : ContentPage
    {
        private ServiciosViewModel _vm;

        public ServiciosPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            _vm = new ServiciosViewModel();
            InitializeComponent();

            _vm.PropertyChanged += VmOnPropertyChanged;
            BindingContext = _vm;

            LabelListServicios.IsVisible = true;
            LabelListFinalizados.IsVisible = false;

            ListaServicios.IsVisible = true;
            ListFinalizados.IsVisible = false;

            var service = new TapGestureRecognizer();
            service.Tapped += (s, e) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    ListaServicios.IsVisible = true;
                    ServiciosLabel.TextColor = (Color)Application.Current.Resources["WizardCompleteColor"];

                    ListFinalizados.IsVisible = false;
                    FinalizadosLabel.TextColor = (Color)Application.Current.Resources["WizardInclompleteColor"];

                    _vm.PaginaServicios = 0;
                    await _vm.BuscarServicios();
                });
            };
            ServiciosLayout.GestureRecognizers.Add(service);

            var active = new TapGestureRecognizer();
            active.Tapped += (s, e) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    ListaServicios.IsVisible = false;
                    ServiciosLabel.TextColor = (Color)Application.Current.Resources["WizardInclompleteColor"];

                    ListFinalizados.IsVisible = true;
                    FinalizadosLabel.TextColor = (Color)Application.Current.Resources["WizardCompleteColor"];

                    _vm.PaginaFinalizados = 0;
                    await _vm.BuscarFinalizados();
                });
            };
            FinalizadosLayout.GestureRecognizers.Add(active);

            //_vm.Initialize();

            MessagingCenter.Subscribe<CotizacionViewModel, long>(this, "AceptaCotizacion", Callback);
        }

        private async void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Servicios")
            {
                if (_vm.Servicios.Count() == 0)
                {
                    LabelListServicios.IsVisible = true;
                }
                else
                {
                    LabelListServicios.IsVisible = false;
                }
                _vm.Servicios.CollectionChanged += ServiciosOnCollectionChanged;
            }

            if (e.PropertyName == "Finalizados")
            {
                if (_vm.Finalizados.Count() == 0)
                {
                    if (_vm.Finalizados.Count() == 0)
                    {
                        LabelListFinalizados.IsVisible = true;
                    }
                    else
                    {
                        LabelListFinalizados.IsVisible = false;
                    }
                    _vm.Finalizados.CollectionChanged += FinalizadosOnCollectionChanged;
                }
            }
            if (e.PropertyName == "PendientesCalificar")
            {
                if (_vm.PendientesCalificar)
                {
                    var calificar = new CalificarProveedorPage(_vm.CalificacionData);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CalificarProveedorPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(CalificarProveedorPage)))
                        await Navigation.PushAsync(calificar);
                }
            }
        }

        private void FinalizadosOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (_vm.Finalizados.Count() == 0)
            {
                LabelListFinalizados.IsVisible = true;
            }
            else
            {
                LabelListFinalizados.IsVisible = false;
            }
        }

        private void ServiciosOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (_vm.Servicios.Count() == 0)
            {
                LabelListServicios.IsVisible = true;
            }
            else
            {
                LabelListServicios.IsVisible = false;
            }
        }

        /*private async void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "PendientesCalificar")
	        {
	            if (_vm.PendientesCalificar)
	            {
                    var calificar = new CalificarProveedorPage(_vm.CalificacionData);
	                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CalificarProveedorPage)) &&
	                    !Navigation.ModalStack.Any(x => x.GetType() == typeof(CalificarProveedorPage)))
	                    await Navigation.PushAsync(calificar);
	            }
	        }
	    }*/


        private async void Callback(CotizacionViewModel cotizacionPage, long idServicio)
        {
            if (idServicio != 0)
            {
                await Navigation.PopAsync();
                var detalle = new DetalleServicioPage(idServicio, false);
                detalle.Disappearing += ServicioPage_Disappearing;
                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                    !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                    await Navigation.PushAsync(detalle);
            }
        }

        private void RoundedButton_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Page1SeleccionTipo());
        }

        private async void OnItemTapped(Object sender, EventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var servicio = (Entities.Servicio)selectedItem;

            switch (servicio.Estatus)
            {
                case ServicioEstatus.Creado:
                    var buscandoProveedor = new Page5BuscandoProveedor(servicio.IdServicio);
                    buscandoProveedor.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(Page5BuscandoProveedor)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(Page5BuscandoProveedor)))
                        await Navigation.PushAsync(buscandoProveedor);
                    break;

                case ServicioEstatus.Confirmado:
                case ServicioEstatus.Iniciado:
                    var detalle = new DetalleServicioPage(servicio.IdServicio, false);
                    detalle.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                        await Navigation.PushAsync(detalle);
                    break;

                case ServicioEstatus.PorIniciar:
                    var page = new ChatPage(servicio.IdServicio, false);
                    page.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChatPage)))
                        await Navigation.PushAsync(page);
                    break;
                case ServicioEstatus.Cancelado:
                    await Application.Current.MainPage.DisplayAlert("Error", "El servicio se encuentra cancelado", "OK");
                    break;
                case ServicioEstatus.Terminado:
                    var resumen = new DetalleServicioPage(servicio.IdServicio, true);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                        await Navigation.PushAsync(resumen);
                    break;

            }

        }

        private async void ServicioPage_Disappearing(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(ChatPage))
            {
                _vm.PaginaServicios = 0;
                await _vm.BuscarServicios();
            }

            //if (sender.GetType() == typeof(EventoDiasPage))
            //{
            //    var resumen = (EventoDiasPage)sender;
            //    if (resumen.IsCompleted)
            //    {
            //        viewModel.Pagina = 0;
            //        await viewModel.Buscar();
            //    }
            //}
        }

        private async void ListViewEspera_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var servicio = (Entities.Servicio)selectedItem;
            var detalle = new DetalleServicioPage(servicio.IdServicio, true);
            detalle.Disappearing += ServicioPage_Disappearing;
            if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                await Navigation.PushAsync(detalle, true);
        }

        private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var servicio = (Entities.Servicio)selectedItem;

            switch (servicio.Estatus)
            {
                case ServicioEstatus.Creado:
                    var buscandoProveedor = new Page5BuscandoProveedor(servicio.IdServicio);
                    buscandoProveedor.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(Page5BuscandoProveedor)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(Page5BuscandoProveedor)))
                        await Navigation.PushAsync(buscandoProveedor);
                    break;

                case ServicioEstatus.Confirmado:
                case ServicioEstatus.Iniciado:
                    var detalle = new DetalleServicioPage(servicio.IdServicio, false);
                    detalle.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                        await Navigation.PushAsync(detalle);
                    break;

                case ServicioEstatus.PorIniciar:
                    var page = new ChatPage(servicio.IdServicio, false);
                    page.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChatPage)))
                        await Navigation.PushAsync(page);
                    break;
                case ServicioEstatus.Cancelado:
                    await Application.Current.MainPage.DisplayAlert("Error", "El servicio se encuentra cancelado", "OK");
                    break;
                case ServicioEstatus.Terminado:
                    var resumen = new DetalleServicioPage(servicio.IdServicio, true);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                        await Navigation.PushAsync(resumen);
                    break;

            }
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            _vm.BuscarServicios();
        }
    }
}