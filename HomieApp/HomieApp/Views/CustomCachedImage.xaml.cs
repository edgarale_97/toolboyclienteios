﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp
{
	public partial class CustomCachedImage : CachedImage
    {
		public CustomCachedImage ()
		{
			InitializeComponent ();
            this.Source = null; // prevent showing old images occasionally
        }

        public static BindableProperty CustomSourceProperty =
            BindableProperty.Create(
                nameof(CustomSource),
                typeof(string),
                typeof(CustomCachedImage),
                string.Empty,
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: (bindable, oldValue, newValue) => {

                    var ctrl = (CustomCachedImage)bindable;
                    ctrl.Source = null; 
                    ctrl.Source = (string)newValue;
                }
            );

        public string CustomSource
        {
            get { return (string)GetValue(CustomSourceProperty); }
            set
            {
                SetValue(CustomSourceProperty, value);

                this.Source = null;
                if (!string.IsNullOrWhiteSpace(value)) 
                    this.Source = value;
            }
        }

        protected override void OnBindingContextChanged()
        {
            this.Source = null; // prevent showing old images occasionally

            this.Source = CustomSource;

            base.OnBindingContextChanged();
        }
    }
}