﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HomieApp.ViewModel;

namespace HomieApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{

	    private SettingViewModel _vm;

        public SettingsPage ()
		{
		    NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
		    _vm = new SettingViewModel();
		    this.BindingContext = _vm;

		    avatar.GestureRecognizers.Add(new TapGestureRecognizer(async view =>
		    {
		        var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");

		        if (action == "Cámara")
		        {
		            await _vm.TakePicture();
		        }
		        if (action == "Galería")
		        {
		           await  _vm.PickPicture();
		        }

		    }));

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void RoundedButton_OnClicked(object sender, EventArgs e)
	    {
	        // Cambiar contraseña
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChangePasswordPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChangePasswordPage)))
	            Navigation.PushModalAsync(NavigationPageHelper.Create(new ChangePasswordPage()));
        }
	}
}