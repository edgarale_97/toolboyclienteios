﻿using System;
using HomieApp.Models;
using HomieApp.ViewModel;
using Plugin.VersionTracking;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PerfilPage : ContentPage
	{
	    private readonly SamplesViewModel _vm;

	    public event EventHandler CerrarSesion;

		public PerfilPage ()
		{
			InitializeComponent ();

            _vm = new SamplesViewModel();
		    BindingContext = _vm;

		    var vt = CrossVersionTracking.Current;
		    LabelVersion.Text = $"v{vt.CurrentVersion}";
        }

	    private async void SampleListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var sample = sampleListView.SelectedItem as Sample;

	        if (sample != null)
	        {
	            if (sample.PageType == typeof(CerrarSesionPage))
	            {
	                var answer = await DisplayAlert("Cerrar Sesión", "¿Estás seguro que deseas cerrar sesión?", "Si",
	                    "No");
	                if (answer)
	                {
	                    var model = new LoginModel();
	                    await model.LogOutAsync();
	                    OnCerrarSesion();
	                }
	            }
	            else
	            {
	                var page = Activator.CreateInstance(sample.PageType) as Page;
	                await Navigation.PushAsync(page);
                }
	            
	        }
	    }

	    protected virtual void OnCerrarSesion()
	    {
	        EventHandler handler = CerrarSesion;
	        if (handler != null)
	        {
	            handler(this, EventArgs.Empty);
	        }
	    }

	    private async void SampleListView_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        var sample = sampleListView.SelectedItem as Sample;

	        if (sample != null)
	        {
	            if (sample.PageType == typeof(CerrarSesionPage))
	            {
	                var answer = await DisplayAlert("Cerrar Sesión", "¿Estás seguro que deseas cerrar sesión?", "Si",
	                    "No");
	                if (answer)
	                {
	                    var model = new LoginModel();
	                    await model.LogOutAsync();
	                    OnCerrarSesion();
	                }
	            }
	            else
	            {
	                var page = Activator.CreateInstance(sample.PageType) as Page;
	                await Navigation.PushAsync(page);
	            }
	        }
	    }
	}
}