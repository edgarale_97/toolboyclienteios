﻿using HomieApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PasswordRecoveryPage : ContentPage
    {
        private PasswordRecoveryViewModel _vm;

        public PasswordRecoveryPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            _vm = new PasswordRecoveryViewModel();
            BindingContext = _vm;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }


    }
}
