﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomieApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarGarantiaPage : ContentPage
    {
        #region
        private AgregarGarantiaViewModel _vm;

        public event EventHandler RegistroCompletado;

        protected virtual void OnRegistroCompletado()
        {
            EventHandler handler = RegistroCompletado;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        public AgregarGarantiaPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            _vm = new AgregarGarantiaViewModel();
            BindingContext = _vm;
            _vm.PropertyChanged += _viewModel_PropertyChanged;
            SetGestureRecognizaers();
            _vm.BuscarDireccion();

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

            Fecha.Date = new DateTime(2019, 11, 26);
            DpFecha.Date = new DateTime(2019, 11, 26);
            DpInstalacion.Date = new DateTime(2019, 11, 26);

        }

        public void SetGestureRecognizaers()
        {
            LayoutPhoto1.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    await _vm.TakePicture(1);
                }
                if (action == "Galería")
                {
                    await _vm.PickPicture(1);
                }
            }));

            LayoutPhoto2.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    await _vm.TakePicture(2);
                }
                if (action == "Galería")
                {
                    await _vm.PickPicture(2);
                }
            }));
        }

        private async void BtnAddImages_OnClicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
            if (action == "Cámara")
            {
                _vm.TakePicture(1);
            }
            if (action == "Galería")
            {
                _vm.PickPicture(1);
            }
        }

        private async void BtnAddFactura_OnClicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
            if (action == "Cámara")
            {
                _vm.TakePicture(2);
            }
            if (action == "Galería")
            {
                _vm.PickPicture(2);
            }
        }

        private void PickerMarca_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            
            _vm.Garantias.Marca = PickerMarca.Items[PickerMarca.SelectedIndex];
            //_vm.Garantias.Marca = PickerMarca.Items[PickerMarca.SelectedIndex];
            //if(_vm.Garantias.Marca == "Otro")
            //{
            //    EntryMarca.IsVisible = true;
            //}
            //if(_vm.Garantias.Marca != "Otro")
            //{
            //    EntryMarca.IsVisible = false;
            //}
            //if(_vm.Garantias.Marca == "SUBZERO")
            //{
            //    //PickerBanco = new Picker();
            //    PickerBanco.Items.Clear();
            //    _vm.Garantias.NombreCategoria = "";
            //    PickerBanco.Items.Add("Refrigerador empotrable");
            //    PickerBanco.Items.Add("Refrigerador Profesional");
            //    PickerBanco.Items.Add("Refrigerador Integrable");
            //    PickerBanco.Items.Add("CAVA");
            //}
            //if(_vm.Garantias.Marca == "WOLF")
            //{
            //    //PickerBanco = new Picker();
            //    PickerBanco.Items.Clear();
            //    PickerBanco.Items.Add("Horno Empotrable");
            //    PickerBanco.Items.Add("Horno de Vapor");
            //    PickerBanco.Items.Add("Horno Micoroondas");
            //    PickerBanco.Items.Add("Cajones Calentadores");
            //    PickerBanco.Items.Add("Cafetera y Cajón");
            //}
            //if(_vm.Garantias.Marca == "ASKO")
            //{
            //    PickerBanco.Items.Clear();
            //    PickerBanco.Items.Add("Horno_ASKO");
            //    PickerBanco.Items.Add("ASKO- Cajón");
            //    PickerBanco.Items.Add("Cafetera- ASKO");
            //    PickerBanco.Items.Add("PARRILLA-ASKO");
            //    PickerBanco.Items.Add("Campanas- ASKO");
            //    PickerBanco.Items.Add("Refrigeradores- ASKO");
            //    PickerBanco.Items.Add("Lavavajillas-ASKO");
            //    PickerBanco.Items.Add("Lavadoras y Secadoras-ASKO");
            //}
            //if(_vm.Garantias.Marca != "SUBZERO" && _vm.Garantias.Marca != "ASKO" && _vm.Garantias.Marca != "WOLF")
            //{
            //    PickerBanco.Items.Clear();
            //    PickerBanco.Items.Add("Refrigeradores");
            //    PickerBanco.Items.Add("Línea Blanca");
            //    PickerBanco.Items.Add("Electrodomésticos");
            //    PickerBanco.Items.Add("Eléctricos");
            //    PickerBanco.Items.Add("Calentadores");
            //    PickerBanco.Items.Add("Aire Acondicionado");
            //}
        }

        private void PickerBanco_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var x = PickerBanco.SelectedIndex;
            if (x != -1)
            {
                _vm.Garantias.NombreCategoria = PickerBanco.Items[PickerBanco.SelectedIndex];
            }
        }

        private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //Application.Current.MainPage.DisplayAlert("ViewModel", "Property", "OK");
            if (e.PropertyName == "Close")
            {
                OnRegistroCompletado();
            }
        }

    }
}
