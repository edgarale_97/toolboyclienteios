﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Helpers;
using HomieApp.ViewModel;
using HomieApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp
{
	public partial class SignUpPage : ContentPage
	{
	    private SignUpViewModel _vm;
	    public event EventHandler RegistroCompletado;
	    private TermsCoditionsPage _terminosCondiciones;
        public SignUpPage ()
		{
			InitializeComponent ();
		    _vm = new SignUpViewModel();
		    _vm.PropertyChanged += _vm_PropertyChanged;
		    this.BindingContext = _vm;

		    var tgr = new TapGestureRecognizer();
		    tgr.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            _terminosCondiciones = new TermsCoditionsPage();
		            Navigation.PushModalAsync(_terminosCondiciones);
                    _terminosCondiciones.Disappearing += Page_Disappearing;
		        });
		    };

            //StackLayoutTerminos.GestureRecognizers.Add(tgr);

           // CheckBoxTerminos.Toggled += CheckBoxTerminos_Toggled;

            if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void CheckBoxTerminos_Toggled(object sender, ToggledEventArgs e)
	    {
	        _terminosCondiciones = new TermsCoditionsPage();
	        Navigation.PushModalAsync(_terminosCondiciones);
            _terminosCondiciones.Disappearing += Page_Disappearing;
        }

        private void _vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            OnRegistroCompletado();
	        }
	    }

	    protected virtual void OnRegistroCompletado()
	    {
	        EventHandler handler = RegistroCompletado;
	        if (handler != null)
	        {
	            handler(this, EventArgs.Empty);
	        }
	    }

	    protected override async void OnAppearing()
	    {
	        base.OnAppearing();

	        if (Settings.Walkthrough)
	        {
	            //Navigation.PushModalAsync(NavigationPageHelper.Create(new WalkthroughFlatPage()));
	            //await Navigation.PushModalAsync(NavigationPageHelper.Create(new WalkthroughPage()));
	            Settings.Walkthrough = false;
	        }
	    }

	    private void Page_Disappearing(object sender, EventArgs e)
	    {
	        /*CheckBoxTerminos.Toggled -= CheckBoxTerminos_Toggled;

	        CheckBoxTerminos.IsToggled = _terminosCondiciones.Aceptado;
	        _vm.AceptoTerminos = _terminosCondiciones.Aceptado;

	        CheckBoxTerminos.Toggled += CheckBoxTerminos_Toggled; */
	    }

        private async void Terms_PageAsync(object sender, EventArgs e)
        {
            //await Navigation.PushModalAsync(NavigationPageHelper.Create(new TermsCoditionsPage()));
            _terminosCondiciones = new TermsCoditionsPage();
            await Navigation.PushModalAsync(_terminosCondiciones);
            _terminosCondiciones.Disappearing += Page_Disappearing;
        }

		void Nombre_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Nombre.Text.Length > 50)
			{
				Nombre.Text = Nombre.Text.Remove(Nombre.Text.Length - 1);
			}
		}

		void ApellidoPaterno_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Apellido_Paterno.Text.Length > 20)
			{
				Apellido_Paterno.Text = Apellido_Paterno.Text.Remove(Apellido_Paterno.Text.Length - 1);
			}
		}

		void ApellidoMaterno_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Apellido_Materno.Text.Length > 20)
			{
				Apellido_Materno.Text = Apellido_Materno.Text.Remove(Apellido_Materno.Text.Length - 1);
			}
		}

		void Email_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Email.Text.Length > 100)
			{
				Email.Text = Email.Text.Remove(Email.Text.Length - 1);
			}
		}

        private void Nombre_Unfocused(object sender, FocusEventArgs e)
        {
			NombreValidationMethod();
        }

        private void Apellido_Paterno_Unfocused(object sender, FocusEventArgs e)
        {
			ApellidoValidationMethod();
        }

        private void Email_Unfocused(object sender, FocusEventArgs e)
        {
			EmailValidationMethod();
        }

        private void PhoneNumber_Unfocused(object sender, FocusEventArgs e)
        {
			PhoneNumberValidationMethod();
        }

		private void NombreValidationMethod()
		{
			if (string.IsNullOrEmpty(Nombre.Text))
			{
				Nombre.PlaceholderColor = Color.Red;
				NombreValidation.IsVisible = true;
			}
			else
			{
				Nombre.PlaceholderColor = Color.Default;
				NombreValidation.IsVisible = false;
			}
		}

		private void ApellidoValidationMethod()
		{
			if (string.IsNullOrEmpty(Apellido_Paterno.Text))
			{
				Apellido_Paterno.PlaceholderColor = Color.Red;
				ApellidoValidation.IsVisible = true;
			}
			else
			{
				Apellido_Paterno.PlaceholderColor = Color.Default;
				ApellidoValidation.IsVisible = false;
			}
		}

		private void EmailValidationMethod()
		{
			if (string.IsNullOrEmpty(Email.Text))
			{
				Email.PlaceholderColor = Color.Red;
				EmailValidation.IsVisible = true;
			}
			else
			{
				Email.PlaceholderColor = Color.Default;
				EmailValidation.IsVisible = false;
			}
		}

		private void PhoneNumberValidationMethod()
		{
			if (string.IsNullOrEmpty(PhoneNumber.Text))
			{
				PhoneNumber.PlaceholderColor = Color.Red;
				PhoneNumberValidation.IsVisible = true;
			}
			else
			{
				PhoneNumber.PlaceholderColor = Color.Default;
				PhoneNumberValidation.IsVisible = false;
			}
		}

        private void RegistrarseButton_Clicked(object sender, EventArgs e)
        {
			NombreValidationMethod();
			ApellidoValidationMethod();
			EmailValidationMethod();
			PhoneNumberValidationMethod();
		}
    }
}