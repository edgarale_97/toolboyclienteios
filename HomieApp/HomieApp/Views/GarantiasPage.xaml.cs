﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using HomieApp.ViewModel;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GarantiasPage : ContentPage
    {
        private GarantiasViewModel _vm;

        public GarantiasPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            _vm = new GarantiasViewModel();
            InitializeComponent();

            _vm.PropertyChanged += VmOnPropertyChanged;
            BindingContext = _vm;

            LabelListServicios.IsVisible = false;
            LabelListFinalizados.IsVisible = true;

            ListaServicios.IsVisible = false;
            ListFinalizados.IsVisible = true;

            var service = new TapGestureRecognizer();
            service.Tapped += (s, e) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    ListaServicios.IsVisible = true;
                    ServiciosLabel.TextColor = (Color)Application.Current.Resources["WizardCompleteColor"];
                    FAB.IsVisible = true;

                    ListFinalizados.IsVisible = false;
                    FinalizadosLabel.TextColor = (Color)Application.Current.Resources["WizardInclompleteColor"];

                    _vm.PaginaActivos = 0;
                    await _vm.BuscarServicios();
                });
            };
            ServiciosLayout.GestureRecognizers.Add(service);

            var active = new TapGestureRecognizer();
            active.Tapped += (s, e) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    ListaServicios.IsVisible = false;
                    ServiciosLabel.TextColor = (Color)Application.Current.Resources["WizardInclompleteColor"];
                    FAB.IsVisible = false;

                    ListFinalizados.IsVisible = true;
                    FinalizadosLabel.TextColor = (Color)Application.Current.Resources["WizardCompleteColor"];

                    _vm.PaginaFinalizados = 0;
                    await _vm.BuscarFinalizados();
                });
            };
            FinalizadosLayout.GestureRecognizers.Add(active);
        }

        private async void Callback(CotizacionViewModel cotizacionPage, long idServicio)
        {
            if (idServicio != 0)
            {
                await Navigation.PopAsync();
                var detalle = new DetalleServicioPage(idServicio, false);
                detalle.Disappearing += ServicioPage_Disappearing;
                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                    !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                    await Navigation.PushAsync(detalle);
            }
        }

        private async void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Activos")
            {
                if (_vm.Activos.Count() == 0)
                {
                    LabelListServicios.IsVisible = true;
                }
                else
                {
                    LabelListServicios.IsVisible = false;
                }
                _vm.Activos.CollectionChanged += ServiciosOnCollectionChanged;
            }

            if (e.PropertyName == "Finalizados")
            {
                if (_vm.Finalizados.Count() == 0)
                {
                    if (_vm.Finalizados.Count() == 0)
                    {
                        LabelListFinalizados.IsVisible = true;
                    }
                    else
                    {
                        LabelListFinalizados.IsVisible = false;
                    }
                    _vm.Finalizados.CollectionChanged += FinalizadosOnCollectionChanged;
                }
            }
            if (e.PropertyName == "PendientesCalificar")
            {
                if (_vm.PendientesCalificar)
                {
                    var calificar = new CalificarProveedorPage(_vm.CalificacionData);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CalificarProveedorPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(CalificarProveedorPage)))
                        await Navigation.PushAsync(calificar);
                }
            }
        }

        private void FinalizadosOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (_vm.Finalizados.Count() == 0)
            {
                LabelListFinalizados.IsVisible = true;
            }
            else
            {
                LabelListFinalizados.IsVisible = false;
            }
        }

        private void ServiciosOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (_vm.Activos.Count() == 0)
            {
                LabelListServicios.IsVisible = true;
            }
            else
            {
                LabelListServicios.IsVisible = false;
            }
        }

        private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var servicio = (Entities.Activos)selectedItem;
            var detalle = new DetalleGarantiaPage(servicio.IdActivos);
            //await Navigation.PushAsync(new DetalleGarantiaPage());
            if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleGarantiaPage)) &&
                !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleGarantiaPage)))
                await Navigation.PushAsync(detalle);
        }

        private async void ListViewEspera_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var servicio = (Entities.Servicio)selectedItem;
            var detalle = new DetalleServicioPage(servicio.IdServicio, true);
            detalle.Disappearing += ServicioPage_Disappearing;
            if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                await Navigation.PushAsync(detalle, true);
        }

        private async void AddGarantia_Clicked(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new AgregarGarantiaPage());
            var page = new AgregarGarantiaPage();
            page.RegistroCompletado += Page_RegistroCompletado;
            Navigation.PushAsync(page);
        }

        private void Page_RegistroCompletado(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
            _vm.BuscarServicios();
        }

        private async void ServicioPage_Disappearing(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(DetalleGarantiaPage))
            {
                _vm.PaginaActivos = 0;
                await _vm.BuscarServicios();
            }

            //if (sender.GetType() == typeof(EventoDiasPage))
            //{
            //    var resumen = (EventoDiasPage)sender;
            //    if (resumen.IsCompleted)
            //    {
            //        viewModel.Pagina = 0;
            //        await viewModel.Buscar();
            //    }
            //}
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            _vm.BuscarServicios();
            _vm.BuscarFinalizados();
        }
    }
}
