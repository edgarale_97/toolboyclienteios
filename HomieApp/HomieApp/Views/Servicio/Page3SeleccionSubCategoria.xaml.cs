﻿using System;
using System.Linq;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3SeleccionSubCategoria : ContentPage
    {
        private int _idTipo;
        private int _activo;
        private bool _vigencia;
        private Categorias _categoriaPadre;
        private Direcciones _direcciones;
        private SeleccionSubCategoriaViewModel _viewModel;

        private int _idRelacionCliente;

        public Page3SeleccionSubCategoria(int idTipo, Categorias categoriaPadre, Direcciones direcciones, int activo, bool vigencia, int idRelacionCliente)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            _idTipo = idTipo;
            _activo = activo;
            _vigencia = vigencia;
            _categoriaPadre = categoriaPadre;
            _direcciones = direcciones;
            _idRelacionCliente = idRelacionCliente;
            MainLabel.LabelText = _categoriaPadre.Nombre;

            _viewModel = new SeleccionSubCategoriaViewModel(_categoriaPadre.IdCategorias, direcciones);
            BindingContext = _viewModel;

            _viewModel.PropertyChanged += _viewModel_PropertyChanged;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

        }

        private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FinalizarBuscar")
            {
                if (_viewModel.Categorias.Count != 0)
                {
                    if (_viewModel.Categorias != null)
                    {
                        _viewModel.Categorias.ForEach(x =>
                        {
                            //if (x.IdCategorias == 0)
                            //{
                            //    x.Seleccionado = true;
                            //}
                            //else
                            //{
                            //    x.Seleccionado = false;
                            //}
                        });
                    }
                }
                else
                {
                    var seleccionadas = _viewModel.Categorias.Where(x => x.Seleccionado).ToList();
                    var chat = new Categorias();

                    if (_viewModel.Chat.IdCategorias == 0)
                    {
                        chat = _viewModel.Chat;
                    }

                    if (chat.Seleccionado)
                    {
                        seleccionadas.Add(chat);
                        Navigation.PopAsync();
                        Navigation.PushAsync(new Page4Multimedia(_idTipo, _categoriaPadre, seleccionadas, _direcciones, _activo, _vigencia, _idRelacionCliente));
                    }
                }
            }
        }

        private async void ContinuarButton_OnClicked(object sender, EventArgs e)
        {
            var seleccionadas = _viewModel.Categorias.Where(x => x.Seleccionado).ToList();
            var chat = new Categorias();

            if(_viewModel.Chat.IdCategorias == 0)
            {
                chat = _viewModel.Chat;
            }

            if (chat.Seleccionado)
            {
                seleccionadas.Add(chat);
                await Navigation.PushAsync(new Page4Multimedia(_idTipo, _categoriaPadre, seleccionadas, _direcciones, _activo, _vigencia, _idRelacionCliente));
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe seleccionar Chat para continuar", "OK");
            }


        }

    }
}
