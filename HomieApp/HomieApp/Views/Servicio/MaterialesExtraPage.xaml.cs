﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MaterialesExtraPage : ContentPage
	{
	    private MaterialesExtraViewModel viewModel;
		public MaterialesExtraPage (long idServicio, long idproveedor)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            viewModel = new MaterialesExtraViewModel(idServicio, idproveedor);
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = viewModel;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            if (viewModel.Close)
	            {
	                Navigation.PopAsync();
	            }
	        }
	    }
	}
}