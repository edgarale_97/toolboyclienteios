﻿using System;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3Direccion : ContentPage
	{
	    private SeleccionDireccionViewModel _viewModel;

		public Page3Direccion()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();

            _viewModel = new SeleccionDireccionViewModel();
		    BindingContext = _viewModel;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void RoundedButton_OnClicked(object sender, EventArgs e)
	    {
	        var page = new BusquedaDireccionPage();
            page.RegistroCompletado += Page_RegistroCompletado;
            Navigation.PushAsync(page);
            
	    }

        private void Page_RegistroCompletado(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
            _viewModel.Buscar();
        }

	    private async void OnItemTapped(Object sender, EventArgs e)
	    {
	        var selectedItem = ((ListView)sender).SelectedItem;
	        var direccion = (Direcciones)selectedItem;
            //Console.WriteLine($"direccion ---> {direccion.IdDireccion}");
	        MessagingCenter.Send<Page3Direccion, Direcciones>(this, "CerrarDirecciones", direccion);
        }
    }
}