﻿using System;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationDireccionPage : ContentPage
    {
        private NavigationDireccionViewModel _viewModel;

        public NavigationDireccionPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            _viewModel = new NavigationDireccionViewModel();
            BindingContext = _viewModel;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }

        private void RoundedButton_OnClicked(object sender, EventArgs e)
        {
            var page = new NavigationBusquedaPage();
            page.RegistroCompletado += Page_RegistroCompletado;
            Navigation.PushAsync(page);
        }

        private void Page_RegistroCompletado(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
            _viewModel.Buscar();
        }

        private async void OnItemTapped(Object sender, EventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var direccion = (Direcciones)selectedItem;
            Application.Current.MainPage.DisplayAlert("Clicked", selectedItem.ToString(), "OK");

            MessagingCenter.Send<NavigationDireccionPage, Direcciones>(this, "CerrarDirecciones", direccion);
        }

    }
}
