﻿using System;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ArchivosServicioPage : ContentPage
	{
        private long idServicio;
	    private ArchivosViewModel _vm;

        public ArchivosServicioPage(long idServicio)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();
            this.idServicio = idServicio;
            _vm = new ArchivosViewModel(idServicio);
            BindingContext = _vm;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }

	    private void ListViewNotificaciones_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        
	    }

	    private async void ImagenButton_OnClicked(object sender, EventArgs e)
	    {

	        //Device.OnPlatform(
	        //    iOS: () =>
	        //    {
	        //        Device.OpenUri(new Uri(_vm.ListaArchivos));
	        //    },
	        //    Android:
	        //    () =>
	        //    {
	        //        Device.OpenUri(new Uri(_viewModel.Notificacion.ImagenDescargar));
	        //    });
        }
	}
}