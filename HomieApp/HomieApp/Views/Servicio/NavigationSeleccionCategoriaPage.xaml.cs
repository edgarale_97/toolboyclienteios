﻿using System;
using System.Collections.Generic;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace HomieApp.Views.Servicio
{
    public partial class NavigationSeleccionCategoriaPage : ContentPage
    {
        private int _idTipo;
        private Categorias _categoriaPadre;
        private NavigationSeleccionCategoriaViewModel _viewModel;

        public NavigationSeleccionCategoriaPage(int idTipo, Categorias categorias)
        {
            NavigationPage.SetHasNavigationBar(this, true);
            InitializeComponent();

            _idTipo = idTipo;
            _categoriaPadre = categorias;
            MainLabel.LabelText = _categoriaPadre.Nombre;

            _viewModel = new NavigationSeleccionCategoriaViewModel(_categoriaPadre.IdCategorias);
            BindingContext = _viewModel;

            _viewModel.PropertyChanged += _viewModel_PropertyChanged;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

        }

        private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FinalizarBuscar")
            {
                if (_viewModel.Categorias != null)
                {
                    _viewModel.Categorias.ForEach(x =>
                    {
                        if (x.IdCategorias == 0)
                        {
                            x.Seleccionado = true;
                        }
                        else
                        {
                            x.Seleccionado = false;
                        }
                    });
                }
            }
        }

        private async void ContinuarButton_OnClicked(object sender, EventArgs e)
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Confirmar", "Para seguir navengando debe registrarse", "OK", "NO");
            if (answer)
            {
                await Navigation.PushModalAsync(new SignUpPage());
            }
        }
    }
}

