﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page4Multimedia : ContentPage
	{
	    private MultimediaViewModel _viewModel;

		public Page4Multimedia(int idTipo, Categorias categoriaPadre, List<Categorias> categorias, Direcciones direccion, int activo, bool vigencia, int idRelacionCliente)
		{
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent();

            _viewModel = new MultimediaViewModel(idTipo, categoriaPadre, categorias, direccion, activo, vigencia, idRelacionCliente);
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = _viewModel;
		    if (idTipo == TipoServicio.Urgente)
		    {
		        LayoutFecha.IsVisible = false;
		        LayoutHora.IsVisible = false;
		    }

		    ImagenServicio.GestureRecognizers.Add(new TapGestureRecognizer(async view =>
		    {
                var answer = await DisplayActionSheet("Agregar imagen","Cancelar",null,new []{"Cámara","Galería"});
		        if (answer == "Cámara")
		        {
		            await _viewModel.TakePicture();
                }
                else if (answer == "Galería")
		        {
		            await _viewModel.PickPicture();
		        }
		        
		    }));

            TpHoraInicio.PropertyChanged += TpHoraInicioOnPropertyChanged;
            TpHoraFin.PropertyChanged += TpHoraFinOnPropertyChanged;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }

            DpFecha.MinimumDate = DateTime.Today;
            TpHoraInicio.Time = DateTime.Now.TimeOfDay;
        }

	    private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Image")
	        {
	            if (_viewModel.Image != null)
	            {
	                ImagenServicio.HeightRequest = 200;
	            }
	        }
        }

	    private async void TpHoraFinOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Time")
	        {
	            if (TpHoraFin.Time <= TpHoraInicio.Time)
	            {
	                await DisplayAlert("Error", "La hora final debe ser mayor a la hora de inicio", "OK");
	                TpHoraFin.Time = TpHoraInicio.Time.Add(TimeSpan.FromHours(1));
	            }
	        }
	        
	    }

	    private void TpHoraInicioOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Time")
	        {
	            var time = TpHoraInicio.Time.Add(TimeSpan.FromHours(1));

	            if (time.Days == 1)
	            {
	                TpHoraFin.Time = time.Add(TimeSpan.FromMinutes((time.Minutes * -1) -1));
                }
	            else
	            {
	                TpHoraFin.Time = time;
                }
	        }
	    }

	    protected async override void OnAppearing()
	    {
	        base.OnAppearing();

	        if (_viewModel.Servicio != null)
	        {
	            if (_viewModel.Servicio.Categorias.Count() == 1)
	            {
	                ListaCategorias.HeightRequest = 109;
	            }
	            else
	            {
	                ListaCategorias.HeightRequest = (_viewModel.Servicio.Categorias.Count * 42) + 25;
                }
	        }
	        else
	        {
	            ListaCategorias.HeightRequest = 25;
            }
        }
    }
}