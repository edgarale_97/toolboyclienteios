﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SeguimientoProveedorPage : ContentPage
	{
	    private SeguimientoProveedorViewModel viewModel;
        public SeguimientoProveedorPage (long idServicio)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
		    viewModel = new SeguimientoProveedorViewModel(idServicio);
		    viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = viewModel;
            map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(new Position(19.4326018d, -99.1353936d), 12d);
            map.HasZoomEnabled = true;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Ubicaciones")
	        {
	            map.Pins.Clear();
                AddMarker(viewModel.Ubicaciones.LatitudProveedor, viewModel.Ubicaciones.LongitudProveedor, viewModel.Ubicaciones.NombreProveedor);
            }
        }

	    private async void AddMarker(double latitud, double longitud, string direccion)
	    {
	        //var icon = BitmapDescriptorFactory.FromView(new BindingPinProveedorView());

            await Task.Delay(1000);

            var pin = new Pin
            {
                Type = PinType.Place,
                Label = string.IsNullOrEmpty(direccion) ? "-" : direccion,
                Position = new Position(latitud, longitud),
                IsDraggable = false
                //,Icon = icon
            };
            map.Pins.Add(pin);
	        map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitud, longitud), Distance.FromMeters(50)), true);
        }

	    private async void Bar_OnClicked(object sender, EventArgs e)
	    {
	        string[] options = {"Actualizar"};
	        var accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options);
	        switch (accion)
	        {
                case "Actualizar":
                    viewModel.ActualizarCommand.Execute(null);
                    break;

            }
	    }

	    protected async override void OnAppearing()
	    {
	        base.OnAppearing();

	        await viewModel.BuscarUbicaciones();
	    }
    }
}