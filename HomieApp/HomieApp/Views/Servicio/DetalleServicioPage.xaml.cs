﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.Models;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetalleServicioPage : ContentPage
	{
	    private DetalleServicioViewModel viewModel;

        public DetalleServicioPage (long idServicio, bool IsFinish)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();

		    viewModel = new DetalleServicioViewModel(idServicio);
		    viewModel.PropertyChanged += ViewModel_PropertyChanged;
		    this.BindingContext = viewModel;

		    //map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(new Position(19.4326018d, -99.1353936d), 12d);

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }

            viewModel.IsFinalizado = IsFinish;

            MessagingCenter.Subscribe<MaterialesExtraViewModel>(this, "MaterialesExtraAceptados", model =>
            {
                viewModel.Buscar(viewModel.Servicio.IdServicio);
            });

            /*map.GestureRecognizers.Add(new TapGestureRecognizer(view =>
		    {
		        var uri = string.Empty;
		        Device.OnPlatform(iOS: () => { uri = "http://maps.apple.com/maps"; }, Android: () => { uri = "http://maps.apple.com/maps"; });
                Device.OpenUri(new Uri($"{uri}?q={viewModel.Servicio.Latitud},{viewModel.Servicio.Longitud}"));
                //Device.OpenUri(new Uri(uri));
		    }));*/
            /*mapImage.GestureRecognizers.Add(new TapGestureRecognizer(view =>
            {
                var uri = string.Empty;
                Device.OnPlatform(iOS: () => { uri = "homie.png"; }, Android: () => { uri = "splash_homie.png"; });
                Device.OpenUri(new Uri(uri));
            }));*/
        }

        private async void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Close")
            {
                // Enviamos un message para abrir la pantalla del servicio.
                MessagingCenter.Send((App)Application.Current, "AceptaCotizacion");
            }
            if (e.PropertyName == "CloseCancelacion")
            {
                if (viewModel.CloseCancelacion)
                {
                    await Navigation.PopAsync();
                }
            }
            if (e.PropertyName == "Servicio")
            {
                //AddMarker(viewModel.Servicio.Latitud, viewModel.Servicio.Longitud, viewModel.Servicio.Direccion);

                if (viewModel.Servicio.Cargos.Count > 0)
                {
                    ListViewRecibos.HeightRequest = viewModel.Servicio.Cargos.Count * 33;
                }
                else
                {
                    ListViewRecibos.HeightRequest = 0;
                }

                if (viewModel.Servicio.Estatus == EstatusServicio.Terminado)
                {
                    //StackBotones.IsVisible = false;
                    bar.IsVisibleMenu = false;
                }

                if (viewModel.Servicio != null)
                {
                    ListViewCategorias.HeightRequest = viewModel.Servicio.Categorias.Count * 50;
                }
            }
        }

        //private async void AddMarker(double latitud, double longitud, string direccion)
        //{
        //    var icon = BitmapDescriptorFactory.FromView(new BindingPinView());
        //    await Task.Delay(1000);

        //    map.Pins.Clear();
        //    map.IsIndoorEnabled = true;
        //    var pin = new Pin
        //    {
        //        Type = PinType.Place,
        //        Label = string.IsNullOrEmpty(direccion) ? "-" : direccion,
        //        Position = new Position(latitud, longitud),
        //        IsDraggable = false,
        //        Icon = icon
        //    };
        //    map.Pins.Add(pin);
        //    map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitud, longitud), Distance.FromMeters(50)), true);
        //}

	    private async void MenuItem_OnClicked(object sender, EventArgs e)
	    {
	        
	    }

	    private async void MaterialesExtra_OnClicked(object sender, EventArgs e)
	    {
	        
	    }

	    private async void Seguimiento_OnClicked(object sender, EventArgs e)
	    {
	        
	    }

	    private async void Emergencia_OnClicked(object sender, EventArgs e)
	    {
	        
        }

	    private async void Bar_OnClicked(object sender, EventArgs e)
	    {
            var options = new List<string>();

            if (viewModel.Servicio.ChatHabilitado)
            {
                options = new List<string> { "Chat", "Materiales Extra", "Seguimiento de proveedor", "Reporte de Emergencia", "Cancelar Servicio"};
            }
            else
            {
                options = new List<string> { "Materiales Extra", "Seguimiento de proveedor", "Reporte de Emergencia", "Cancelar Servicio" };
            }

	        var accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options.ToArray());
	        switch (accion)
	        {
                case "Chat":
                    var chat = new ChatPage(viewModel.Servicio.IdServicio, false);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChatPage)))
                        await Navigation.PushAsync(chat);
                    break;
                case "Materiales Extra":
                    var extra = new MaterialesExtraPage(viewModel.Servicio.IdServicio, viewModel.Servicio.IdProveedor);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(MaterialesExtraPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(MaterialesExtraPage)))
                        await Navigation.PushAsync(extra);
                    break;
                case "Seguimiento de proveedor":
                    var seguimiento = new SeguimientoProveedorPage(viewModel.Servicio.IdServicio);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(SeguimientoProveedorPage)))
                    {
                        await Navigation.PushAsync(seguimiento);
                    }
                    break;
                case "Reporte de Emergencia":
                    if (await Application.Current.MainPage.DisplayAlert("Confirmación", "¿Está seguro que desea reportar una emergencia?", "Si", "No"))
                    {
                        var model = new AlertasModel();
                        var result = await model.PostAsync(viewModel.Servicio.IdServicio);
                        if (result.Error)
                        {
                            await DisplayAlert("Error", result.ErrorDescription, "OK");
                        }
                        else
                        {
                            await DisplayAlert("Correcto", "Se reportó una emergencia", "OK");
                        }
                    }
                    break;
                case "Cancelar Servicio":
                    viewModel.CancelarCommand.Execute(null);
                    break;

            }
	    }
	}
}