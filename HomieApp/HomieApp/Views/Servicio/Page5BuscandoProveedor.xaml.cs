﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.Models;
using Microsoft.AspNet.SignalR.Client;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page5BuscandoProveedor : ContentPage
	{
	    private HubConnection _hubConnection;
	    private long _idServicio;
	    private string _buscandoProveedorId;
	    private bool activeCountdown = true;

        public Page5BuscandoProveedor (long idServicio)
		{
		    _buscandoProveedorId = Guid.NewGuid().ToString();
		    Settings.BuscandoProveedorId = _buscandoProveedorId;
            NavigationPage.SetHasNavigationBar(this,false);
		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }

            InitializeComponent ();
		    _idServicio = idServicio;
		    Inicializar(false);

            MessagingCenter.Subscribe<App>((App)Application.Current, "OnActivated", (variable) => {
		        StartMotorBusquedaHub();
		        Debug.WriteLine("OnActivated");
		    });

		    MessagingCenter.Subscribe<App>((App)Application.Current, "DidEnterBackground", (variable) => {
		        StopMotorBusquedaHub();
		        Debug.WriteLine("DidEnterBackground");
		    });

		    CountDown();

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }


	    private async Task CountDown()
	    {
	        var time = 60;
	        while (time > 0)
	        {
	            if (activeCountdown)
	            {
	                await Task.Delay(1000);
	                time -= 1;
	            }
	            else
	            {
	                return;
	            }
	        }
            if(activeCountdown)
	            DisplayAlert(null, "Aún no encontramos a tu proveedor, nos comunicaremos contigo mas tarde", "OK");
	    }

	    protected async override void OnAppearing()
	    {
	        base.OnAppearing();
            //StartMotorBusquedaHub();
            //Revisamos si el servicio ya fue aceptado
	        var model = new ServiciosModel();
	        var res = await model.GetByIdAsync(_idServicio);
	        if (!res.Error)
	        {
	            if(res.Result.Estatus == EstatusServicio.PorIniciar)
                    Inicializar(true);
	        }

	    }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();
            StopMotorBusquedaHub();
	    }

	    private async void MotorBusquedaHub()
	    {
	        // Connect to the server
	        if (_hubConnection == null)
	        {
	            _hubConnection = new HubConnection(Settings.SignalrUri);
	            _hubConnection.Headers.Add("Authorization", $"Bearer {Settings.Token}");
                Console.Write("BuscandoProveedor", "hubConnection: " + _hubConnection);

	            // Create a proxy to the SignalR Hub
	            var hubProxy = _hubConnection.CreateHubProxy("MotorBusquedaHub");
                Console.Write("BuscandoProveedor", "hubProxy: " + hubProxy);

	            // Wire up a handler for the 'EdecanConfirmada' for the server
	            // to be called on our client
	            hubProxy.On<long>("ProveedorEncontrado", (id) =>
	            {

	                Device.BeginInvokeOnMainThread(() =>
	                {
	                    Inicializar(true);
	                });

	            });

	            // Start the connection
	            await _hubConnection.Start();
	        }
	    }

	    public async void StartMotorBusquedaHub()
	    {
	        if (_hubConnection == null && Settings.BuscandoProveedorId == _buscandoProveedorId)
	        {
	            MotorBusquedaHub();
	        }
	    }

	    public async void StopMotorBusquedaHub()
	    {
	        if (_hubConnection != null && Settings.BuscandoProveedorId == _buscandoProveedorId)
	        {
	            _hubConnection.Stop();
	            _hubConnection = null;
	        }
	    }

        private void ButtonContinuar_OnClicked(object sender, EventArgs e)
	    {
            // Redireccionamos a la pagina del chat.
	        MessagingCenter.Send((App)Application.Current, "ChatProveedor", _idServicio);
        }

	    private void Inicializar(bool confirmado)
	    {
	        if (confirmado)
	        {
	            activeCountdown = false;
	            LabelBuscando.Text = "Hemos encontrado a tu proveedor, te pondremos en contacto";
	            AnimationJson.IsVisible = false;
	            CheckJson.IsVisible = true;
	            CheckJson.Play();
                ButtonContinuar.IsVisible = true;
            }
	        else
	        {
	            activeCountdown = true;
	            LabelBuscando.Text = "Estamos buscando a tu proveedor, por favor espere";
	            AnimationJson.IsVisible = true;
	            CheckJson.IsVisible = false;
	            ButtonContinuar.IsVisible = false;
	        }
	    }
    }
}