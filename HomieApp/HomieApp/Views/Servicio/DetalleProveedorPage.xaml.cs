﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLToolkit.Forms.Controls;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Controls;

namespace HomieApp.Views.Servicio
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleProveedorPage : ContentPage
    {
        private DetalleProveedorViewModel _vm;

        public DetalleProveedorPage(long idProveedor)
        {
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent();

            _vm = new DetalleProveedorViewModel(idProveedor);
            _vm.PropertyChanged += _vm_PropertyChanged;
            this.BindingContext = _vm;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }

        private async void _vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Close")
            {
                // Enviamos un message para abrir la pantalla del servicio.
                //MessagingCenter.Send((App)Application.Current, "AceptaCotizacion");
            }
        }
        
        private async void FlowListImgs_OnFlowItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedItem = ((FlowListView)sender).FlowLastTappedItem;

           
            var trabajo = (Catalogo)selectedItem;
            if (!string.IsNullOrWhiteSpace(trabajo.Nombre))
            {
                var imagen = new ImagenPage(trabajo.Nombre);
                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ImagenPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ImagenPage)))
                   await Navigation.PushAsync(imagen);
            }
        }
    }
}