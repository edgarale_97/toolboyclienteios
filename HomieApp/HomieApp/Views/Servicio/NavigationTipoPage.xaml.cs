﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationTipoPage : ContentPage
    {
        private NavigationTipoViewModel _vm;

        public NavigationTipoPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            _vm = new NavigationTipoViewModel();
            BindingContext = _vm;

            direccion.GestureRecognizers.Add(new TapGestureRecognizer(async view =>
            {
                Locator_ClickedAsync();
            }));

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }

        private async System.Threading.Tasks.Task Locator_ClickedAsync()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;

            var position = await locator.GetLastKnownLocationAsync();
            if (position != null)
            {
                var geoCoder = new Geocoder();
                var address = await geoCoder.GetAddressesForPositionAsync(new Position(position.Latitude, position.Longitude));
                await Application.Current.MainPage.DisplayAlert("Resultado", "Se ocupara su ubicación actual para navegar: " + address.FirstOrDefault().ToString(), "Aceptar");
                LabelAddress.Text = address.FirstOrDefault().ToString();
            }

        }

        private async void ContinuarButton_OnClicked(object sender, EventArgs e)
        {
            var idTipo = _vm.TipoUrgente ? TipoServicio.Urgente : TipoServicio.Agendado;

            if (_vm.CategoriaSeleccionada == null)
            {
                await Application.Current.MainPage.DisplayAlert("Alerta", "Debe seleccionar una categoría", "OK");
                return;
            }

            await Navigation.PushModalAsync(new NavigationSeleccionCategoriaPage(idTipo, _vm.CategoriaSeleccionada));

        }

    }
}



