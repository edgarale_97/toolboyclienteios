﻿using System;
using HomieApp.Entities;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1SeleccionTipo : ContentPage
	{
	    private SeleccionTipoViewModel _vm;

		public Page1SeleccionTipo()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent();

            _vm = new SeleccionTipoViewModel();
		    BindingContext = _vm;

		    direccion.GestureRecognizers.Add(new TapGestureRecognizer(async view =>
		    {
		        MessagingCenter.Subscribe<Page3Direccion, Direcciones>(this, "CerrarDirecciones", async (sender, direccion) =>
		        {
		            MessagingCenter.Unsubscribe<Page3Direccion, Direcciones>(this, "CerrarDirecciones");

		            if (sender != null && direccion != null)
		            {
		                _vm.Direccion = direccion;
		            }

		            await Navigation.PopAsync();
		        });

                Navigation.PushAsync(new Page3Direccion());
            }));

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }

        }

	    private async void ContinuarButton_OnClicked(object sender, EventArgs e)
	    {
	        var idTipo = _vm.TipoUrgente ? TipoServicio.Urgente : TipoServicio.Agendado;

	        if (_vm.CategoriaSeleccionada == null)
	        {
	            await Application.Current.MainPage.DisplayAlert("Alerta", "Debe seleccionar una categoría", "OK");
                return;
            }

	        if (_vm.Direccion == null)
	        {
	            await Application.Current.MainPage.DisplayAlert("Alerta", "Debe seleccionar una dirección", "OK");
                return;
            }

	        await Navigation.PushAsync(new Page2SeleccionCategoria(idTipo, _vm.CategoriaSeleccionada, _vm.Direccion, 0, false));
        }

	}
}