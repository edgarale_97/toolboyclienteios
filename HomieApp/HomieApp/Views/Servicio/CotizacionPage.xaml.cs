﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CotizacionPage : ContentPage
	{
	    private CotizacionViewModel viewModel;

		public CotizacionPage (long idServicio)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();

            viewModel = new CotizacionViewModel(idServicio);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
		    this.BindingContext = viewModel;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }

		    /*mapImage.GestureRecognizers.Add(new TapGestureRecognizer(view =>
		    {
		        var uri = string.Empty;
		        Device.OnPlatform(iOS: () => { uri = "http://maps.apple.com/maps"; }, Android: () => { uri = "http://maps.google.com/maps"; });
		        Device.OpenUri(new Uri($"{uri}?q={viewModel.Servicio.Latitud},{viewModel.Servicio.Longitud}"));
		    }));*/
        }

        private async void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Close")
            {
                if (viewModel.Close)
                {
                    
                    await Navigation.PopAsync();
                    
                }
                
            }
            if (e.PropertyName == "Servicio")
            {
                if (viewModel.Servicio.Cargos.Count > 0)
                {
                    ListViewRecibos.HeightRequest = viewModel.Servicio.Cargos.Count * 33;
                }
                else
                {
                    ListViewRecibos.HeightRequest = 0;
                }
            }

            if (e.PropertyName == "Servicio")
            {
                if (viewModel.Servicio != null)
                {
                    ListViewCategorias.HeightRequest = viewModel.Servicio.Categorias.Count * 50;
                }
            }
        }

	    private void RoundedButton_OnClicked(object sender, EventArgs e)
	    {
             Navigation.PushAsync(new DetalleProveedorPage(viewModel.Servicio.IdProveedor));
        }

	    private async void Bar_OnClicked(object sender, EventArgs e)
	    {
	        string[] options = {"Perfil del Proveedor"};
	        var accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options);
	        switch (accion) 
	        {
                case "Perfil del Proveedor":
                    if(!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleProveedorPage)) && 
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleProveedorPage)))
                        await Navigation.PushAsync(new DetalleProveedorPage(viewModel.Servicio.IdProveedor));
                    break;

            }
	    }
	}
}