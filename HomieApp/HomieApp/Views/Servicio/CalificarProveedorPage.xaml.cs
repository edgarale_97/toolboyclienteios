﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Entities;
using HomieApp.Helpers;
using HomieApp.ViewModel.Servicio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalificarProveedorPage : ContentPage
	{
	    private CalificarProveedorViewModel viewModel;
	    private bool _animate;

        public CalificarProveedorPage (CalificacionData data)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            viewModel = new CalificarProveedorViewModel(data);
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = viewModel;

			if (Device.RuntimePlatform == Device.iOS)
			{
				this.Padding = new Thickness(0, 30, 0, 0);
			}

			_animate = true;
			AnimateIn().Forget();
		}

	    private async void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            if (viewModel.Close)
	            {
	                await Navigation.PopAsync();
	            }
	        }
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        var content = this.Content;
	        this.Content = null;
	        this.Content = content;

	        _animate = true;
	        AnimateIn().Forget();
	    }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();
	        _animate = false;
	    }

	    public async Task AnimateIn()
	    {
	        await AnimateItem(img, 10500);
	    }

	    public async Task AnimateItem(View uiElement, uint duration)
	    {
	        while (_animate)
	        {
	            await uiElement.ScaleTo(1.05, duration, Easing.SinInOut);
	            await Task.WhenAll(
	                uiElement.FadeTo(1, duration, Easing.SinInOut),
	                uiElement.LayoutTo(new Rectangle(new Point(0, 0), new Size(uiElement.Width, uiElement.Height))),
	                uiElement.FadeTo(.9, duration, Easing.SinInOut),
	                uiElement.ScaleTo(1.15, duration, Easing.SinInOut));
	        }
	    }
	}
}