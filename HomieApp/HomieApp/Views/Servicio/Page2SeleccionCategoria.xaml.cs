﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HomieApp.Entities;
using HomieApp.ViewModel;
using Xamarin.Forms.Internals;
using HomieApp.Helpers;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2SeleccionCategoria : ContentPage
	{
	    private int _idTipo;
        private int _activo;
        private bool _vigencia;
	    private Categorias _categoriaPadre;
        private Direcciones _direccion;
        private SeleccionCategoriaViewModel _viewModel;

        private int _idRelacionCliente;

        public Page2SeleccionCategoria (int idTipo, Categorias categoriaPadre, Direcciones direccion, int activo, bool vigencia)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent();

		    _idTipo = idTipo;
            _activo = activo;
            _vigencia = vigencia;
		    _categoriaPadre = categoriaPadre;
		    _direccion = direccion;
            _idRelacionCliente = categoriaPadre.IdRelacion;
            MainLabel.LabelText = _categoriaPadre.Nombre;

            _viewModel = new SeleccionCategoriaViewModel(_categoriaPadre.IdCategorias, direccion);
            BindingContext = _viewModel;

            _viewModel.PropertyChanged += _viewModel_PropertyChanged;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

        private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FinalizarBuscar")
            {
                if (_viewModel.Categorias != null)
                {
                    _viewModel.Categorias.ForEach(x =>
                    {
                        //if (x.IdCategorias == 0)
                        //{
                        //    x.Seleccionado = true;
                        //}
                        //else
                        //{
                        //    x.Seleccionado = false;
                        //}
                    });
                }
            }
        }

	    private async void ContinuarButton_OnClicked(object sender, EventArgs e)
	    {
            //if (Settings.Aprobado == false)
            //{
                var validation = _viewModel.Categorias.Where(x => x.IdCategorias == 0);
                if (!validation.Any())
                {
                    if (_viewModel.CategoriaSeleccionada == null)
                    {
                        await Application.Current.MainPage.DisplayAlert("Alerta", "Debe seleccionar una categoría", "OK");
                        return;
                    }

                    await Navigation.PushAsync(new Page3SeleccionSubCategoria(_idTipo, _viewModel.CategoriaSeleccionada, _direccion, _activo, _vigencia, _idRelacionCliente));

                }
                else
                {
                    var seleccionadas = _viewModel.Categorias.Where(x => x.Seleccionado).ToList();
                    await Navigation.PushAsync(new Page4Multimedia(_idTipo, _categoriaPadre, seleccionadas, _direccion, _activo, _vigencia, _idRelacionCliente));
                }
            //}
            //else
            //{
            //    await Application.Current.MainPage.DisplayAlert("Alerta", "Lo sentimos, aún no tenemos cobertura en tu zona.", "OK");
            //    await Navigation.PopAsync();
            //    return;
            //}
            
        }
    }
}