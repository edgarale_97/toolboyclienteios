﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieApp.Helpers;
using HomieApp.ViewModel.Servicio;
using HomieApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieApp.Views.Servicio
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChatPage : ContentPage
    {
        private ChatViewModel viewModel;
        
        public ChatPage(long idServicio, bool readOnly)
        {
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent();

            ContainerSend.IsVisible = !readOnly;

            viewModel = new ChatViewModel(idServicio);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;

            this.BindingContext = viewModel;

            SendButton.GestureRecognizers.Add(new TapGestureRecognizer(view =>
            {
                viewModel.EnviarMensajeCommand.Execute(null);
            }));

            MandarFoto.GestureRecognizers.Add(new TapGestureRecognizer(async view =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");

                if(action == "Cámara")
                {
                    await viewModel.TakePicture();
                }
                if(action == "Galería")
                {
                    await viewModel.PickPicture();
                }

            }));

            //MessagingCenter.Subscribe<App>((App)Application.Current, "OnActivated", (variable) => {
            //        viewModel.StartChatHub();
            //        Debug.WriteLine("OnActivated");
            //});

            //MessagingCenter.Subscribe<App>((App)Application.Current, "DidEnterBackground", (variable) => {

            //        viewModel.StopChatHub();
            //        Debug.WriteLine("DidEnterBackground");
            //});

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

            this.UpdateAsync();
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Envia")
            {
                if (viewModel.Mensajes.Count > 0)
                    ListViewMensajes.ScrollTo(viewModel.Mensajes[viewModel.Mensajes.Count - 1], ScrollToPosition.End, true);
            }
            if (e.PropertyName == "Proveedor")
            {
                
                bar.LabelText = viewModel.Proveedor.Nombre;
            }
        }

        //protected override void OnDisappearing()
        //{


        //    viewModel.StopChatHub();
        //    Debug.WriteLine("OnDisappearing");

        //    //MessagingCenter.Unsubscribe<App>((App)Application.Current, "OnActivated");
        //}

        //protected override void OnAppearing()
        //{
        //    base.OnAppearing();

        //    // OnAppearing funciona bien con Android pero no con iOS, por lo que usamos OnActivated
        //    viewModel.StartChatHub();
        //    Debug.WriteLine("Appearing");
        //}

        private async void OnItemTapped(Object sender, EventArgs e)
        {
            var selectedItem = ((ListView)sender).SelectedItem;
            var servicio = (Entities.ServicioChat)selectedItem;

            if (servicio.Mensaje == "El proveedor agregó materiales extra a la cotización" &&
                viewModel.CotizacionRecibida)
            {
                var extras = new MaterialesExtraPage(servicio.IdServicio, servicio.IdProveedor);
                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(MaterialesExtraPage)) &&
                    !Navigation.ModalStack.Any(x => x.GetType() == typeof(MaterialesExtraPage)))
                    await Navigation.PushAsync(extras);
            }

            if (servicio.Mensaje == "El proveedor te envió una cotización" && viewModel.CotizacionRecibida)
            {
                var cotizacion = new CotizacionPage(servicio.IdServicio);
                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CotizacionPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(CotizacionPage)))
                    await Navigation.PushAsync(cotizacion);
            }

            if (!string.IsNullOrWhiteSpace(servicio.Video))
            {
                //var video = new VideoPage(servicio.Video);
                //if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(VideoPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(VideoPage)))
                //    await Navigation.PushAsync(video);

                Device.OnPlatform(
                    iOS: () =>
                    {
                        Device.OpenUri(new Uri(servicio.Video));
                    },
                    Android:
                    () =>
                    {
                        Device.OpenUri(new Uri(servicio.Video));
                    });
            }

            if (!string.IsNullOrWhiteSpace(servicio.Imagen))
            {
                //var imagen = new ImagenPage(servicio.Imagen);
                //if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ImagenPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ImagenPage)))
                //    await Navigation.PushAsync(imagen);
                Device.OnPlatform(
                    iOS: () =>
                    {
                        Device.OpenUri(new Uri(servicio.Imagen));
                    },
                    Android:
                    () =>
                    {
                        Device.OpenUri(new Uri(servicio.Imagen));
                    });
            }
        }

        //private async void ToolbarItemCotizacion_OnClicked(object sender, EventArgs e)
        //{
            
        //}

        //private async void ToolbarItemArchivos_OnClicked(object sender, EventArgs e)
        //{
            
        //}

        private async void Bar_OnClicked(object sender, EventArgs e)
        {
            string accion;
            if (viewModel.CotizacionRecibida)
            {
                string [] options =  {"Cotización", "Archivos","Perfil del Proveedor"};
                accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options);
            }
            else
            {
                string[] options = { "Archivos", "Perfil del Proveedor" };
                accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options);
            }
            
            
            switch (accion)
            {
                case "Cotización":
                    var page = new CotizacionPage(viewModel.IdServicio);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CotizacionPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(CotizacionPage)))
                        await Navigation.PushAsync(page);
                    break;
                case "Archivos":
                    var archivo = new ArchivosServicioPage(viewModel.IdServicio);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ArchivosServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(ArchivosServicioPage)))
                        await Navigation.PushAsync(archivo);
                    break;
                case "Perfil del Proveedor":
                    if(!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleProveedorPage)) && 
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleProveedorPage)))
                        await Navigation.PushAsync(new DetalleProveedorPage(viewModel.Proveedor.IdProveedor));
                    break;
            }
        }

        public async Task UpdateAsync()
        {
            await viewModel.ActualizarMensaje();
        }
    }
}