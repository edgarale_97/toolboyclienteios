﻿using System;
using System.Collections.Generic;
using HomieApp.DependencyServices;
using HomieApp.ViewModel;
using Xamarin.Forms;

namespace HomieApp.Views
{
    public partial class AgregarTarjetaPage : ContentPage
    {
        private TarjetaCreditoViewModel _vm;

        public AgregarTarjetaPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            _vm = new TarjetaCreditoViewModel();
            _vm.PropertyChanged += _vm_PropertyChanged;
            this.BindingContext = _vm;

            if(Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

            ScanCard.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                DependencyService.Get<ICardService>().StartCapture();
            }));


        }

        private void _vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if(e.PropertyName == "Completed")
            {
                Navigation.PopAsync();
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
        }

        
    }
}
