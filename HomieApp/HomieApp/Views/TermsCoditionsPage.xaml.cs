﻿using System;
using System.Collections.Generic;
using HomieApp.Helpers;
using Xamarin.Forms;

namespace HomieApp.Views
{
    public partial class TermsCoditionsPage : ContentPage
    {
        public TermsCoditionsPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();

            label.Text = Settings.TerminosYCondiciones;

            if(Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }
    }
}
