﻿using System;
using System.Collections.Generic;
using System.Text;
using HomieApp.Entities;

namespace HomieApp.DependencyServices
{
    public interface ICardService
    {
        void StartCapture();

        CreditCard GetCreditCard();

        string GetCardNumber();

        string GetCardholderName();

        void CloseCardService();
    }
}
