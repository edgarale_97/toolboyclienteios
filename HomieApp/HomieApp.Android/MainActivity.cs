﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.App;
using Android.Util;
using Card.IO;
using FFImageLoading.Forms.Droid;
using HomieApp.Droid.DependencyServices;
using HomieApp.Droid.Notifications;
using HomieApp.Helpers;
using ImageCircle.Forms.Plugin.Droid;
using Lottie.Forms.Droid;
using Plugin.CurrentActivity;
using Plugin.DeviceInfo;
using Plugin.Permissions;
using Plugin.VersionTracking;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

using Android.Gms.Common;
using Firebase.Messaging;
using Firebase.Iid;
using Firebase;

[assembly: ResolutionGroupName("HomieApp")]
[assembly: UsesFeature("android.hardware.camera", Required = false)]
[assembly: UsesFeature("android.hardware.camera.autofocus", Required = false)]
namespace HomieApp.Droid
{
    [Activity(Label = "ToolBoy",
        Icon = "@drawable/icon",
        Theme = "@style/Theme.Splash",
        MainLauncher = true,
        LaunchMode = LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        static readonly string TAG = "MainActivity";
        internal static readonly string CHANNEL_ID = "com.energetika.toolboycliente";
        internal static readonly int NOTIFICATION_ID = 100;

        // Create a new instance field for this activity.
        public static MainActivity instance = null;

        // Return the current activity instance.
        public static MainActivity CurrentActivity
        {
            get
            {
                return instance;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            // Set the current instance of MainActivity.
            instance = this;

            // Changing to App's theme since we are OnCreate and we are ready to 
            // "hide" the splash
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            base.SetTheme(Resource.Style.AppTheme);

            FormsAppCompatActivity.ToolbarResource = Resource.Layout.Toolbar;
            FormsAppCompatActivity.TabLayoutResource = Resource.Layout.Tabs;

            base.OnCreate(bundle);

            //Initializing FFImageLoading
            CachedImageRenderer.Init();

            global::Xamarin.Forms.Forms.Init(this, bundle);

            ImageCircleRenderer.Init();
            CrossVersionTracking.Current.Track();

            Xamarin.FormsGoogleMaps.Init(this, bundle); // initialize for Xamarin.Forms.GoogleMaps

            InTheHand.Forms.Platform.Android.InTheHandForms.Init();

            Settings.Platform = (int)Entities.Platform.Android;
            Settings.GooglePlacesApiKey = ApiKeys.AndroidGooglePlacesApiKey;

            if (string.IsNullOrWhiteSpace(Settings.DeviceId))
            {
                Settings.DeviceId = CrossDeviceInfo.Current.GenerateAppId(true);
                Settings.Confirmado = false;
            }

            LoadApplication(new HomieApp.App());

            AnimationViewRenderer.Init();

            //RegisterWithGCM();

            CheckPermissions();

            Xamarin.Forms.Application.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Pan);

            IsPlayServicesAvailable();

            CreateNotificationChannel();

            //FirebaseApp.InitializeApp(this);
            //var token = FirebaseInstanceId.Instance.Token;
            //Log.Debug("MainActivity", "Token Firebase: " + token);

        }

        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if(resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    // msgText
                }
                else
                {
                    // msgText.Text = "This device is not supported";
                    Finish();
                }
                return false;
            }
            else
            {
                // msgText.Text = "Google Play Services is available";
                return true;
            }
        }

        void CreateNotificationChannel()
        {
            if(Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID, "FCM Notifications", NotificationImportance.Default)
            {
                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);

        }

        protected async override void OnNewIntent(Intent intent)
        {
            try
            {
                base.OnNewIntent(intent);
                var parameters = intent.GetStringExtra("parameters") ?? string.Empty;
                var pageToOpen = intent.GetStringExtra("pageToOpen") ?? string.Empty;
                var title = intent.GetStringExtra("title") ?? string.Empty;
                var message = intent.GetStringExtra("message") ?? string.Empty;


                //This method will get called while the app is launching from the app icon or from the notification
                if (pageToOpen != null)
                {
                    Settings.Notificaciones = 0;
                    Plugin.Badge.CrossBadge.Current.SetBadge(0);

                    //Means new Intent from push notification
                    //Code to open the page
                    var notificacion = new HomieApp.Entities.Notificaciones();
                    notificacion.PageToOpen = pageToOpen;
                    notificacion.Parameters = parameters;
                    notificacion.Titulo = title;
                    notificacion.Descripcion = message;
                    MessagingCenter.Send<App, HomieApp.Entities.Notificaciones>((App) Xamarin.Forms.Application.Current, "OpenFromNotificationBar", notificacion);
                }
            }
            catch
            {
                
            }
        }

        private void CheckPermissions()
        {
            // Permisos de la camara por si no los tiene
            var permissionCamera = Manifest.Permission.Camera;
            var permissionLocation = Manifest.Permission.AccessFineLocation;
            var permissionCoarseLocation = Manifest.Permission.AccessCoarseLocation;
            var permissionWriteStorage = Manifest.Permission.WriteExternalStorage;
            var permissionReadStorage = Manifest.Permission.ReadExternalStorage;
            var request = new List<string>();

            if (CrossDeviceInfo.Current.VersionNumber.Major < 6)
            {

            }
            else
            {
                if (CheckSelfPermission(permissionCamera) != (int)Permission.Granted)
                {
                    request.Add(Manifest.Permission.Camera);
                }

                if (CheckSelfPermission(permissionLocation) != (int)Permission.Granted)
                {
                    request.Add(Manifest.Permission.AccessFineLocation);
                }

                if (CheckSelfPermission(permissionCoarseLocation) != (int)Permission.Granted)
                {
                    request.Add(Manifest.Permission.AccessCoarseLocation);
                }

                if (CheckSelfPermission(permissionWriteStorage) != (int)Permission.Granted)
                {
                    request.Add(Manifest.Permission.WriteExternalStorage);
                }

                if (CheckSelfPermission(permissionReadStorage) != (int)Permission.Granted)
                {
                    request.Add(Manifest.Permission.ReadExternalStorage);
                }

                if (request.Any())
                {
                    ActivityCompat.RequestPermissions(this, request.ToArray(), 1);
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            //base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (data != null)
            {
                // Be sure to JavaCast to a CreditCard (normal cast won‘t work)      
                InfoShareHelper.Instance.CardInfo = data.GetParcelableExtra(CardIOActivity.ExtraScanResult).JavaCast<CreditCard>();
            }
            else
            {
                Console.WriteLine("Scanning Canceled!");
            }
        }

        //private void RegisterWithGCM()
        //{
        //    // Check to ensure everything's set up right
        //    GcmClient.CheckDevice(this);
        //    GcmClient.CheckManifest(this);

        //    // Register for push notifications
        //    System.Diagnostics.Debug.WriteLine("Registering...");
        //    PushHandlerService.Context = this;
        //    GcmClient.Register(this, ApiKeys.GoogleSenderId);
        //}

        protected override void OnDestroy()
        {
            try
            {
                Console.WriteLine("OnDestroy: Location app is becoming inactive");
                base.OnDestroy();
            }
            catch (Exception ex)
            {
                Console.WriteLine("OnDestroy Error");
            }

        }
    }
}

