﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HomieApp;
using HomieApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LetterSpacingLabel), typeof(LetterSpacingLabelRenderer))]
namespace HomieApp.Droid.Renderers
{
    public class LetterSpacingLabelRenderer : LabelRenderer
    {
        protected LetterSpacingLabel LetterSpacingLabel { get; private set; }

        #region -- Overrides --

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                this.LetterSpacingLabel = (LetterSpacingLabel)this.Element;
            }

            var letterSpacing = this.LetterSpacingLabel.LetterSpacing;
            this.Control.LetterSpacing = letterSpacing;

            this.UpdateLayout();
        }

        #endregion
    }
}