﻿using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.OS;
using System;
using System.ComponentModel;
using HomieApp.Droid.Renderers;
using HomieApp.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryProperties), typeof(ArtinaEntryRenderer))]
namespace HomieApp.Droid.Renderers
{
    public class ArtinaEntryRenderer : EntryRenderer
    {
        private bool disposed;
        private WeakReference<ShapeDrawable> _buttomLineShape;
        private WeakReference<ShapeDrawable> _otherShape;

        protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
        {
            base.OnSizeChanged(w, h, oldw, oldh);
            ShapeDrawable shapeDrawable;
            if (_buttomLineShape == null || !_buttomLineShape.TryGetTarget(out shapeDrawable))
                return;
            Path path = new Path();
            int num = h - 5;
            path.MoveTo(5f, num);
            path.LineTo(w - 10, num);
            shapeDrawable.Shape = new PathShape(path, w, h);
            Control.InvalidateDrawable(Control.Background);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement == null || Control == null)
                return;
            UpdatePlaceholderColor(e.NewElement);
            if (e.OldElement != null)
            {
                e.OldElement.Focused += OnFocused;
                e.OldElement.Unfocused += OnLostFocus;
            }
            switch (EntryProperties.GetBorderStyle(e.NewElement))
            {
                case BorderStyle.Default:
                    if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop || (Control.Background == null || !(GetBorderColor(e.NewElement) != Xamarin.Forms.Color.Default)))
                        break;
                    e.NewElement.Focused += OnFocused;
                    e.NewElement.Unfocused += OnLostFocus;
                    break;
                case BorderStyle.None:
                    Control.Background = CreateBackgroundShape(e.NewElement.BackgroundColor.ToAndroid());
                    break;
                case BorderStyle.BottomLine:
                    Xamarin.Forms.Color borderColor1 = GetBorderColor(e.NewElement);
                    if (!(borderColor1 != Xamarin.Forms.Color.Default))
                        break;
                    ShapeDrawable backgroundShape1 = CreateBackgroundShape(e.NewElement.BackgroundColor.ToAndroid());
                    ShapeDrawable shapeDrawable1 = new ShapeDrawable();
                    shapeDrawable1.Paint.SetStyle(Paint.Style.Stroke);
                    shapeDrawable1.Paint.Color = borderColor1.ToAndroid();
                    shapeDrawable1.Paint.StrokeWidth = (int)EntryProperties.GetBorderWidth(e.NewElement);
                    Control.Background = new LayerDrawable(new ShapeDrawable[2]
                    {
                        backgroundShape1,
                        shapeDrawable1
                    });
                    _buttomLineShape = new WeakReference<ShapeDrawable>(shapeDrawable1);
                    break;
                case BorderStyle.Rect:
                    Xamarin.Forms.Color borderColor2 = GetBorderColor(e.NewElement);
                    if (!(borderColor2 != Xamarin.Forms.Color.Default))
                        break;
                    ShapeDrawable backgroundShape2 = CreateBackgroundShape(e.NewElement.BackgroundColor.ToAndroid());
                    ShapeDrawable shapeDrawable2 = new ShapeDrawable(new RectShape());
                    shapeDrawable2.Paint.SetStyle(Paint.Style.Stroke);
                    shapeDrawable2.Paint.Color = borderColor2.ToAndroid();
                    shapeDrawable2.Paint.StrokeWidth = EntryProperties.GetBorderWidth(e.NewElement);
                    Control.Background = new LayerDrawable(new ShapeDrawable[2]
                    {
                        backgroundShape2,
                        shapeDrawable2
                    });
                    _otherShape = new WeakReference<ShapeDrawable>(shapeDrawable2);
                    break;
                case BorderStyle.RoundRect:
                    Xamarin.Forms.Color borderColor3 = GetBorderColor(e.NewElement);
                    if (!(borderColor3 != Xamarin.Forms.Color.Default))
                        break;
                    ShapeDrawable backgroundShape3 = CreateBackgroundShape(e.NewElement.BackgroundColor.ToAndroid());
                    float borderCornerRadius = EntryProperties.GetBorderCornerRadius(e.NewElement);
                    ShapeDrawable shapeDrawable3 = new ShapeDrawable(new RoundRectShape(new float[8]
                    {
                        borderCornerRadius,
                        borderCornerRadius,
                        borderCornerRadius,
                        borderCornerRadius,
                        borderCornerRadius,
                        borderCornerRadius,
                        borderCornerRadius,
                        borderCornerRadius
                    }, null, null));
                    shapeDrawable3.Paint.SetStyle(Paint.Style.Stroke);
                    shapeDrawable3.Paint.Color = borderColor3.ToAndroid();
                    shapeDrawable3.Paint.StrokeWidth = EntryProperties.GetBorderWidth(e.NewElement);
                    Control.Background = new LayerDrawable(new ShapeDrawable[2]
                    {
                        backgroundShape3,
                        shapeDrawable3
                    });
                    _otherShape = new WeakReference<ShapeDrawable>(shapeDrawable3);
                    break;
            }
        }

        private Xamarin.Forms.Color GetBorderColor(Entry control)
        {
            Xamarin.Forms.Color color = EntryProperties.GetBorderColor(Element);
            if (Build.VERSION.SdkInt < BuildVersionCodes.Lollipop && color == Xamarin.Forms.Color.Default)
                color = Xamarin.Forms.Color.White;
            //color = Xamarin.Forms.Color.Accent ColorCache.AccentColor;
            return color;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (string.Equals(e.PropertyName, "BorderColor"))
            {
                BorderStyle borderStyle = EntryProperties.GetBorderStyle(Element);
                Xamarin.Forms.Color borderColor = GetBorderColor(Element);
                if (borderStyle == BorderStyle.Default)
                {
                    if (!Control.HasFocus || Control.Background == null)
                        return;
                    Control.Background.SetColorFilter(borderColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
                }
                else if (borderStyle == BorderStyle.BottomLine)
                {
                    ShapeDrawable shapeDrawable;
                    if (_buttomLineShape == null || !_buttomLineShape.TryGetTarget(out shapeDrawable))
                        return;
                    shapeDrawable.Paint.Color = borderColor.ToAndroid();
                }
                else
                {
                    ShapeDrawable shapeDrawable;
                    if (borderStyle != BorderStyle.Rect && borderStyle != BorderStyle.RoundRect || (_otherShape == null || !_otherShape.TryGetTarget(out shapeDrawable)))
                        return;
                    shapeDrawable.Paint.Color = borderColor.ToAndroid();
                }
            }
            else if (string.Equals(e.PropertyName, "BorderWidth"))
            {
                switch (EntryProperties.GetBorderStyle(Element))
                {
                    case BorderStyle.BottomLine:
                        ShapeDrawable shapeDrawable1;
                        if (_buttomLineShape == null || !_buttomLineShape.TryGetTarget(out shapeDrawable1))
                            break;
                        shapeDrawable1.Paint.StrokeWidth = EntryProperties.GetBorderWidth(Element);
                        break;
                    case BorderStyle.Rect:
                    case BorderStyle.RoundRect:
                        ShapeDrawable shapeDrawable2;
                        if (_otherShape == null || !_otherShape.TryGetTarget(out shapeDrawable2))
                            break;
                        shapeDrawable2.Paint.StrokeWidth = EntryProperties.GetBorderWidth(Element);
                        break;
                }
            }
            else if (string.Equals(e.PropertyName, "BorderCornerRadius"))
            {
                ShapeDrawable shapeDrawable3;
                if (EntryProperties.GetBorderStyle(Element) != BorderStyle.RoundRect || _otherShape == null || !_otherShape.TryGetTarget(out shapeDrawable3))
                    return;
                float borderCornerRadius = EntryProperties.GetBorderCornerRadius(Element);
                float[] outerRadii = new float[8]
                {
                    borderCornerRadius,
                    borderCornerRadius,
                    borderCornerRadius,
                    borderCornerRadius,
                    borderCornerRadius,
                    borderCornerRadius,
                    borderCornerRadius,
                    borderCornerRadius
                };
                shapeDrawable3.Shape = new RoundRectShape(outerRadii, null, null);
            }
            else
            {
                if (!string.Equals(e.PropertyName, "PlaceholderColor"))
                    return;
                UpdatePlaceholderColor(Element);
            }
        }

        private void UpdatePlaceholderColor(Entry element)
        {
            Xamarin.Forms.Color placeholderColor = EntryProperties.GetPlaceholderColor(element);
            if (!(placeholderColor != Xamarin.Forms.Color.Default))
                return;
            Control.SetHintTextColor(placeholderColor.ToAndroid());
        }

        private static ShapeDrawable CreateBackgroundShape(Android.Graphics.Color color)
        {
            ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
            shapeDrawable.Paint.Color = color;
            shapeDrawable.Paint.SetStyle(Paint.Style.Fill);
            return shapeDrawable;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !disposed)
            {
                disposed = true;
                if (Element != null)
                {
                    Element.Focused -= OnFocused;
                    Element.Unfocused -= OnLostFocus;
                }
            }
            base.Dispose(disposing);
        }

        private void OnFocused(object sender, FocusEventArgs e)
        {
            Entry element = Element;
            if (element == null || !e.IsFocused)
                return;
            FormsEditText control = Control;
            Xamarin.Forms.Color borderColor = GetBorderColor(element);
            if (control == null || control.Background == null || !(borderColor != Xamarin.Forms.Color.Default))
                return;
            control.Background.SetColorFilter(borderColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
        }

        private void OnLostFocus(object sender, FocusEventArgs e)
        {
            if (e.IsFocused)
                return;
            FormsEditText control = Control;
            if (control == null || control.Background == null)
                return;
            control.Background.ClearColorFilter();
        }
    }
}
