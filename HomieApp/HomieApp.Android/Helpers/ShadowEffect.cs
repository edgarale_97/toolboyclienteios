﻿using System.ComponentModel;
using Android.Graphics.Drawables;
using Android.OS;
using HomieApp.Droid;
using HomieApp.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportEffect(typeof(ShadowEffect), "ShadowEffect")]
namespace HomieApp.Droid
{
    public class ShadowEffect : BaseEffect
    {
        private float _originalElevation;
        private Drawable _originalBackground;

        protected override bool CanBeApplied()
        {
            if (!false)
                return (int)Build.VERSION.SdkInt >= 21;
            return false;
        }

        protected override void OnAttachedInternal()
        {
            _originalElevation = Container.Elevation;
            UpdateShadow();
        }

        protected override void OnDetachedInternal()
        {
            Container.Elevation = _originalElevation;
            if (_originalBackground == null || Control.Background != null)
                return;
            Control.SetBackground(_originalBackground);
        }

        protected virtual void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            OnElementPropertyChanged(args);
            if (!Attached || !(args.PropertyName == Effects.ShadowProperty.PropertyName) && !(args.PropertyName == Effects.ShadowSizeProperty.PropertyName))
                return;
            UpdateShadow();
        }

        private void UpdateShadow()
        {
            float shadowSize = (float)Effects.GetShadowSize(Element);
            if (shadowSize < 0.0)
            {
                if (Control == null || Control.Background == null)
                    return;
                _originalBackground = Container.Background;
                Control.Background = null;
            }
            else
            {
                if (_originalBackground != null && Control.Background == null)
                {
                    Control.SetBackground(_originalBackground);
                    _originalBackground = null;
                }
                Container.Elevation = (shadowSize * Container.Resources.DisplayMetrics.Density);
            }
        }
    }
}
