﻿using System.ComponentModel;
using Android.Graphics;
using Android.OS;
using Android.Views;
using HomieApp.Droid;
using HomieApp.Helpers;
using Xamarin.Forms;
using View = Android.Views.View;

[assembly: ExportEffect(typeof(CornerRadiusEffect), "CornerRadiusEffect")]
namespace HomieApp.Droid
{
    public class CornerRadiusEffect : BaseEffect
    {
        private ViewOutlineProvider _originalProvider;

        protected override bool CanBeApplied()
        {
            if (Container != null)
                return (int)Build.VERSION.SdkInt >= 21;
            return false;
        }

        protected override void OnAttachedInternal()
        {
            _originalProvider = Container.OutlineProvider;
            Container.OutlineProvider = new CornerRadiusOutlineProvider(Element);
            Container.ClipToOutline = (true);
        }

        protected override void OnDetachedInternal()
        {
            Container.OutlineProvider = (_originalProvider);
            Container.ClipToOutline = (false);
        }

        protected virtual void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            OnElementPropertyChanged(args);
            if (!Attached || !(args.PropertyName == Effects.CornerRadiusProperty.PropertyName))
                return;
            Container.Invalidate();
        }

        private class CornerRadiusOutlineProvider : ViewOutlineProvider
        {
            private Element _element;

            public CornerRadiusOutlineProvider(Element element)
            {
                _element = element;
            }

            public override void GetOutline(View view, Outline outline)
            {
                float num = (float)Effects.GetCornerRadius(_element) * view.Resources.DisplayMetrics.Density;
                outline.SetRoundRect(new Rect(0, 0, view.Width, view.Height), (int)num);
            }
        }
    }
}