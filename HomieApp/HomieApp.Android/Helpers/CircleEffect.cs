﻿using System;
using Android.Graphics;
using Android.OS;
using Android.Views;
using HomieApp.Droid;
using Xamarin.Forms;
using View = Android.Views.View;

[assembly: ExportEffect(typeof(CircleEffect), "CircleEffect")]
namespace HomieApp.Droid
{
    public class CircleEffect : BaseEffect
    {
        private ViewOutlineProvider _originalProvider;

        protected override bool CanBeApplied()
        {
            if (Container != null)
                return (int)Build.VERSION.SdkInt >= 21;
            return false;
        }

        protected override void OnAttachedInternal()
        {
            _originalProvider = Container.OutlineProvider;
            Container.OutlineProvider = new CirlceOutlineProvider();
            Container.ClipToOutline = true;
        }

        protected override void OnDetachedInternal()
        {
            Container.ClipToOutline = false;
            Container.OutlineProvider = (_originalProvider);
        }

        private class CirlceOutlineProvider : ViewOutlineProvider
        {
            public override void GetOutline(View view, Outline outline)
            {
                double width = view.Width;
                double height = view.Height;
                if (width <= 0.0 || height <= 0.0)
                    return;
                double num1 = Math.Min(width, height);
                float num2 = (float)(num1 / 2.0);
                double num3 = width <= num1 ? 0.0 : (width - num1) / 2.0;
                double num4 = height <= num1 ? 0.0 : (height - num1) / 2.0;
                outline.SetRoundRect(new Rect((int)num3, (int)num4, (int)(num3 + num1), (int)(num4 + num1)), num2);
            }
        }
    }
}