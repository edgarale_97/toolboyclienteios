﻿using Android.App;
using Android.Util;
using Firebase.Messaging;

using System.Linq;
using Android.Content;
using System.Collections.Generic;
using Android.Support.V4.App;
using HomieApp.Droid;
using WindowsAzure.Messaging;
using HomieApp.Helpers;
using System;
using Android.OS;

[Service]
[IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
public class MyFirebaseMessagingService : FirebaseMessagingService
{
    const string TAG = "MyFirebaseMsgService";


    public override void OnMessageReceived(RemoteMessage message)
    {
        Log.Debug(TAG, "From: " + message.From);
        Log.Debug(TAG, "Data: " + message.Data);
        Log.Debug(TAG, "ClassToString: " + message.Class.ToString());
        if (message.GetNotification() != null)
        {
            //These is how most messages will be received
            Log.Debug(TAG, "Notification Message Body: " + message.GetNotification().Body);
            SendNotification(message.GetNotification().Title,message.GetNotification().Body, message.Data);
        }
        else
        {
            //Only used for debugging payloads sent from the Azure portal
            SendNotification(message.Data["title"], message.Data["message"], message.Data);
            //SendNotification(message.Data.Values.First());
        }
    }

    void SendNotification(string title, string messageBody, IDictionary<string, string> data)
    {
        var intent = new Intent(this, typeof(MainActivity));
        intent.AddFlags(ActivityFlags.ClearTop);
        foreach (var key in data.Keys)
        {
            intent.PutExtra(key, data[key]);
        }
        var requestCode = new Random().Next();
        var pendingIntent = PendingIntent.GetActivity(this,
                                                     requestCode,
                                                      intent,
                                                      PendingIntentFlags.OneShot);

        var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                                  .SetSmallIcon(Resource.Drawable.ic_home)
                                  .SetContentTitle(title)
                                  .SetContentText(messageBody)
                                  .SetAutoCancel(true)
                                  .SetContentIntent(pendingIntent);

        var notificationManager = NotificationManagerCompat.From(this);
        notificationManager.Notify(MainActivity.NOTIFICATION_ID, notificationBuilder.Build());
    }

    /*void SendNotification(string messageBody)
    {
        Log.Debug(TAG, "Notification Message Body: " + messageBody);

        var intent = new Intent(this, typeof(MainActivity));
        intent.AddFlags(ActivityFlags.ClearTop);
        var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

        var notificationBuilder = new Notification.Builder(this)
                    .SetContentTitle("FCM Message")
                    .SetSmallIcon(Resource.Drawable.ic_launcher)
                    .SetContentText(messageBody)
                    .SetAutoCancel(true)
                    .SetContentIntent(pendingIntent);

        var notificationManager = NotificationManager.FromContext(this);

        notificationManager.Notify(0, notificationBuilder.Build());

    }*/


}

