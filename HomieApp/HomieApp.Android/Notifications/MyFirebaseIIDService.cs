﻿using System;
using Android.App;
using Firebase.Iid;
using Android.Util;
using WindowsAzure.Messaging;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.DeviceInfo;

namespace FCMClient
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        NotificationHub hub;

        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }
        void SendRegistrationToServer(string token)
        {
            Log.Debug(PackageName,token);
            // Add custom implementation, as needed.
            hub = new NotificationHub(HomieApp.Helpers.ApiKeys.NotificationHub,
                                       HomieApp.Helpers.ApiKeys.ConnectionString, this);

          
            var regID = "";
            var tags = new List<string>() { CrossDeviceInfo.Current.Id };
            regID = hub.Register(token, tags.ToArray()).RegistrationId;
            //var tag = hub.Register(token, "id" + regID).RegistrationId;

            Log.Debug(TAG, $"Successful registration of ID {regID}");
            HomieApp.Helpers.Settings.DeviceId = CrossDeviceInfo.Current.Id;
            HomieApp.Helpers.Settings.RegistrationId = regID;
        }
    }
}