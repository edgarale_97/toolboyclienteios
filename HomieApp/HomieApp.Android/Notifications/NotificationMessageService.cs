﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HomieApp.Helpers;
using HomieApp.Entities;
using Xamarin.Forms;

namespace HomieApp.Droid.Notifications
{
    [Service]
    class NotificationMessageService : Service
    {
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            var parameters = intent.GetStringExtra("parameters") ?? string.Empty;
            var pageToOpen = intent.GetStringExtra("pageToOpen") ?? string.Empty;
            var title = intent.GetStringExtra("title") ?? string.Empty;
            var message = intent.GetStringExtra("message") ?? string.Empty;

            try
            {
                // Enviamos un message para abrir las pantallas.
                MainActivity.instance.RunOnUiThread(() =>
                {
                    try
                    {
                        //if (Settings.NotificationPageToOpen == NotificacionPage.Chat)
                        //{
                        //}

                        // Enviamos un message para abrir las pantallas.
                        var notificacion = new HomieApp.Entities.Notificaciones();
                        notificacion.PageToOpen = pageToOpen;
                        notificacion.Parameters = parameters;
                        notificacion.Titulo = title;
                        notificacion.Descripcion = message;
                        MessagingCenter.Send<App, HomieApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "HubNotification", notificacion);

                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
            catch (Exception ex)
            {
                
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }
    }
}