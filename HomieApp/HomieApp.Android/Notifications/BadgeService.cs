﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HomieApp.Helpers;
using Plugin.Badge;

namespace HomieApp.Droid.Notifications
{
    [Service]
    class BadgeService : Service
    {
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            var pageToOpen = intent.GetStringExtra("pageToOpen") ?? string.Empty;

            try
            {
                // Enviamos un message para abrir las pantallas.
                MainActivity.instance.RunOnUiThread(() =>
                {
                    try
                    {
                        if (pageToOpen != HomieApp.Entities.NotificacionPage.Chat)
                        {
                            Settings.Notificaciones++;
                            CrossBadge.Current.SetBadge(Settings.Notificaciones);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
            catch (Exception ex)
            {

            }

            return base.OnStartCommand(intent, flags, startId);
        }

        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }
    }
}