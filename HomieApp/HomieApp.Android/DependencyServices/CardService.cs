﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Card.IO;
using HomieApp.DependencyServices;
using HomieApp.Droid.DependencyServices;
using HomieApp.Entities;
using Xamarin.Forms;
using CreditCard = Card.IO.CreditCard;

[assembly: Dependency(typeof(CardService))]

namespace HomieApp.Droid.DependencyServices
{
    public class CardService : ICardService
    {
        private Activity activity;

        public void StartCapture()
        {
            InitCardService();

            var intent = new Intent(activity, typeof(CardIOActivity));
            intent.PutExtra(CardIOActivity.ExtraRequireExpiry, true);
            intent.PutExtra(CardIOActivity.ExtraRequireCvv, true);
            intent.PutExtra(CardIOActivity.ExtraRequirePostalCode, false);
            intent.PutExtra(CardIOActivity.ExtraHideCardioLogo, true);

            activity.StartActivityForResult(intent, 101);
        }

        public Entities.CreditCard GetCreditCard()
        {
            if (InfoShareHelper.Instance.CardInfo != null)
            {
                var cardInfo = InfoShareHelper.Instance.CardInfo;
                CreditCardType type;

                if (cardInfo.CardType == CardType.Amex)
                {
                    type = CreditCardType.Amex;
                }
                else if (cardInfo.CardType == CardType.Visa)
                {
                    type = CreditCardType.Visa;
                }
                else if (cardInfo.CardType == CardType.Mastercard)
                {
                    type = CreditCardType.Mastercard;
                }
                else
                {
                    type = CreditCardType.Unrecognized;
                }

                return new Entities.CreditCard
                {
                    ExpiryYear = cardInfo.ExpiryYear,
                    ExpiryMonth = cardInfo.ExpiryMonth,
                    Cvv = cardInfo.Cvv,
                    CardholderName = cardInfo.CardholderName,
                    CardNumber = cardInfo.CardNumber,
                    RedactedCardNumber = cardInfo.RedactedCardNumber,
                    CardType = type
                };
            }

            return null;
        }

        public string GetCardNumber()
        {
            return (InfoShareHelper.Instance.CardInfo != null) ? InfoShareHelper.Instance.CardInfo.CardNumber : null;
        }

        public string GetCardholderName()
        {
            return (InfoShareHelper.Instance.CardInfo != null)
                ? InfoShareHelper.Instance.CardInfo.CardholderName
                : null;
        }

        private void InitCardService()
        {
            // Init current activity
            var context = Forms.Context;
            activity = context as Activity;
            InfoShareHelper.Instance.CardInfo = null;
        }

        public void CloseCardService()
        {
            activity = null;
            InfoShareHelper.Instance.CardInfo = null;
        }
    }

    public class InfoShareHelper
    {
        private static InfoShareHelper instance = null;
        private static readonly object padlock = new object();

        public CreditCard CardInfo { get; set; }

        public static InfoShareHelper Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new InfoShareHelper();
                    }
                    return instance;
                }
            }
        }
    }
}